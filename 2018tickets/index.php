<?php require_once('Connections/channel1media.php'); ?>
<?php
session_start();
$sessionId = session_id();
mysql_select_db($database_channel1media, $channel1media);
$ebrochure = $_dbname;

//$googleTracking = false;
$googleTracking = true;

$isTesting = false;
$sessionDbId = 0;
$presentationId = 0;
$dbId = 0;
$firstname = "";
$lastname = "";
$fullname = "";
$email = "";
$strUrl = "";
$hitsCount = true;
$genericVersion = true;
$arrFiles = array();
$myinfo = array();
$isSpecialAccount = false;

//$query_eb = "SELECT * FROM " . $_dbname . "_sections WHERE `active`=1 ORDER BY `parentId`, `id` ASC";
$query_eb = "SELECT * FROM " . $_dbname . "_sections ORDER BY `parentId`, `id` ASC";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$amountSection = mysql_num_rows($eb);
$arrSectionDisabled = array();
$arrSection0 = array();
$arrSectionParent = array();
$arrSectionName = array();
$arrSectionHasChild = array();
if ($amountSection > 0) {
	do {
		if ($row_eb['active'] == 1) {
			$arrSection0[] = $row_eb['id'];
			$arrSectionParent[$row_eb['id']] = $row_eb['parentId'];
			$arrSectionName[$row_eb['id']] = $row_eb['name'];
			$arrSectionHasChild[$row_eb['parentId']] = true;
		} else {
			$arrSectionDisabled[] = $row_eb['id'];
		}
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$arrSection = array();
$arrSectionOff = array();

$thetime = date("YmdHis");

$strUrl = $_SERVER["REQUEST_URI"];
$arrUrl = explode("/", $strUrl);
$strUrl = $arrUrl[count($arrUrl)-1];

if (substr($strUrl, 0, 9) == "?preview.") {
	$hitsCount = false;
	$strUrl = substr($strUrl, 9);
	$isTesting = true;
}
$genericPurl = false;
$isRealPurl = false;
if (isset($_arrEditable)) {
	$arrSummaryTitle = array();
	$arrSummaryCopy = array();
	$arrSummaryPhoto = array();
	$arrSummaryFile = array();
	$arrSummaryFileTitle = array();
	$arrSummaryExists = array();
	$arrSummaryOn = array();
	for ($i=0; $i<count($_arrEditable); $i++) {
		$sid = $_arrEditable[$i];
		$arrSummaryTitle[$sid] = "";
		$arrSummaryCopy[$sid] = "";
		$arrSummaryPhoto[$sid] = $_defaultPhoto;
		$arrSummaryFile[$sid] = "";
		$arrSummaryFileTitle[$sid] = "";
		$arrSummaryExists[$sid] = false;
		$arrSummaryOn[$sid] = false;
	}
} else {
	$summaryTitle = "";
	$summaryCopy = "";
	$summaryPhoto = $_defaultPhoto;
	$summaryFile = "";
	$hasSummary = false;
	$isSummaryOn = false;
}
$repId = 0;
$row_eb = array();
$row_eb['section'] = "";
$row_eb['sectionOff'] = "";

function isSectionOff($n) {
	global $arrSectionOff, $arrSectionDisabled, $arrSection;
	$r = false;
	if (count($arrSectionDisabled)>0) {
		for ($i=0; $i<count($arrSectionDisabled); $i++) {
			if ($n == $arrSectionDisabled[$i]) {
				$r = true;
				break;
			}
		}
	}
	if ((!$r) && count($arrSectionOff) > 0) {
		for ($i=0; $i<count($arrSectionOff); $i++) {
			if ($n == $arrSectionOff[$i]) {
				$r = true;
			}
		}
	}
	if (!$r) {
		for ($i=0; $i<count($arrSection); $i++) {
			if ($n == $arrSection[$i]) {
				break;
			}
		}
		if ($i == count($arrSection)) {
			$r = true;
		}
	}
	return $r;
}

//if ((strrpos($strUrl, ".js")===false) && (strrpos($strUrl, ".txt")===false) && (strrpos($strUrl, ".html")===false) && (strrpos($strUrl, ".png")===false) && (strrpos($strUrl, ".jpg")===false) && (strrpos($strUrl, ".jpeg")===false) && (strrpos($strUrl, ".gif")===false) && (strrpos($strUrl, ".php")===false) && (strrpos($strUrl, ".css")===false)) {
if (checkStrUrl($strUrl)) {
	if (strlen($strUrl)>5) {
		$arrUrl = explode("?", $strUrl);
		$strUrl = $arrUrl[0];
		$arrUrl = explode("#", $strUrl);
		$strUrl = $arrUrl[0];
		$arrUrl = explode("&", $strUrl);
		$strUrl = $arrUrl[0];
	}
	if (isset($_genericAccount)) {
		$apl = explode(".", $strUrl);
		if (strlen($apl[count($apl)-1]) == 5) {
			$genericPurl = true;
		}
	}

	if (isset($_SESSION['sessionId'])) {
		if ($sessionId == $_SESSION['sessionId']) {
			if (isset($_SESSION['purl'])) {
				if ($strUrl == $_SESSION['purl']) {
					$hitsCount = false;
				}
			}
		}
	}
	if ($strUrl == "") {
		$strUrl = $_defaultPurl;
		/*
		if ($_dbname == "tigers2014cms" || $_dbname == "whitesox2014cms") {
			$strUrl = "newsales";
		} else {
			$strUrl = "admin.0000";
		}
		*/
	}

	if ($hitsCount) {
		$updateSQL = "UPDATE page_hits SET hits=hits+1 WHERE ebrochure='" . $ebrochure . "'";
		$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
		if (!$genericPurl) {

			$updateSQL = "UPDATE " . $_dbname . "_presentations SET `Status`=4 WHERE `purl`='$strUrl' AND `hits`=0";
			$Result = mysql_query($updateSQL, $channel1media) or die(mysql_error());

			$updateSQL = "UPDATE " . $_dbname . "_presentations SET hits=hits+1 , `lasttime`='$thetime' WHERE `purl`='$strUrl'";
			$Result1 = mysql_query($updateSQL, $channel1media) or die(mysql_error());

			// $insertSQL = sprintf("INSERT INTO " . $_dbname . "_tracking (`time`, `from`, `url`) VALUES (%s, %s, %s)",
			// 					   GetSQLValueString($thetime, "text"),
			// 					   GetSQLValueString($_SERVER['HTTP_REFERER'], "text"),
			// 					   GetSQLValueString($strUrl, "text"));
			$insertSQL = sprintf("INSERT INTO " . $_dbname . "_tracking (`time`, `url`) VALUES (%s, %s)",
								   GetSQLValueString($thetime, "text"),
								   GetSQLValueString($strUrl, "text"));
			$Result1 = mysql_query($insertSQL, $channel1media) or die(mysql_error());
		}
	}
	if ($genericPurl) {
		$totalRows_eb = 0;
	} else {
		$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE purl='$strUrl' LIMIT 1";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
		$totalRows_eb = mysql_num_rows($eb);
	}
	if ($totalRows_eb > 0) {
		$isRealPurl = true;
		$dbId = $row_eb['id'];
		$repId = $row_eb['repId'];
		if ($dbId != 1) {
			$firstname = trim(rawurldecode($row_eb['firstname']));
			$lastname = trim(rawurldecode($row_eb['lastname']));
			$email = trim(rawurldecode($row_eb['email']));
		}
		$replicateFrom = $row_eb['replicateFrom'];
		$myinfo = $row_eb;

		if ($hitsCount) {
			$insertSQL = sprintf("INSERT INTO " . $_dbname . "_sessions (`presentationId`, `time`, `sessionId`, `system`) VALUES (%s, %s, %s, %s)",
						   GetSQLValueString($dbId, "text"),
						   GetSQLValueString($thetime, "text"),
						   GetSQLValueString($sessionId, "text"),
						   GetSQLValueString($_SERVER['HTTP_USER_AGENT'], "text"));
			$Result1 = mysql_query($insertSQL, $channel1media) or die(mysql_error());
			$sessionDbId = mysql_insert_id();
		} else {
			$query_eb2 = "SELECT `id` FROM " . $_dbname . "_sessions WHERE `presentationId`='$dbId' AND `sessionId`='$sessionId'";
			$eb2 = mysql_query($query_eb2, $channel1media) or die(mysql_error());
			$row_eb2 = mysql_fetch_assoc($eb2);
			$totalRows_eb2 = mysql_num_rows($eb2);
			$sessionDbId = $row_eb2['id'];
		}
		$presentationId = $dbId;

	} else {
		$aname = explode(".", $strUrl);
		$firstname = str_replace("_", " ", $aname[0]);
		if (count($aname)>1) {
			$lastname = str_replace("_", " ", $aname[1]);
		} else {
			$lastname = "";
		}
	}
	if (!$isRealPurl) {
		$dbId = 1;
		$repId = 1;
		if ($genericPurl) {	//athletics/uva special
			$arrAccount = array();
			if (strlen($strUrl)>2) {
				$query_eb = "SELECT * FROM " . $_dbname . "_accounts WHERE purl='$strUrl' LIMIT 1";
				$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
				$row_eb = mysql_fetch_assoc($eb);
				$totalRows_eb = mysql_num_rows($eb);
				if ($totalRows_eb > 0) {
					$myinfo = $row_eb;
					if (isset($row_eb['FirstName'])) {
						$firstname = trim(rawurldecode($row_eb['FirstName']));
					}
					if (isset($row_eb['LastName'])) {
						$lastname = trim(rawurldecode($row_eb['LastName']));
					}
					if (isset($row_eb['FullName'])) {
						$fulltname = trim(rawurldecode($row_eb['FullName']));
					}
					if (isset($row_eb['Email'])) {
						$email = trim(rawurldecode($row_eb['Email']));
					}
					$lastname = rawurldecode($row_eb['LastName']);
					$email = rawurldecode($row_eb['email']);
					if (isset($row_eb['repId'])) {
						$repId = $row_eb['repId'];
					}
					for ($kk=0; $kk<count($_genericAccount); $kk++) {
						$fieldItem = $_genericAccount[$kk];
						$arrAccount[$fieldItem] = $row_eb[$fieldItem];
					}
					if ($hitsCount) {
						$updateSQL = "UPDATE " . $_dbname . "_accounts SET hits=hits+1 , `lasttime`='$thetime' WHERE `purl`='$strUrl'";
						$Result1 = mysql_query($updateSQL, $channel1media) or die(mysql_error());
					}
					$isSpecialAccount = true;
				}
			}
		}
		/*
		if ($hitsCount && $dbId > 1) {
			$insertSQL = sprintf("INSERT INTO " . $_dbname . "_sessions (`presentationId`, `time`, `sessionId`, `system`) VALUES (%s, %s, %s, %s)",
						   GetSQLValueString($dbId, "text"),
						   GetSQLValueString($thetime, "text"),
						   GetSQLValueString($sessionId, "text"),
						   GetSQLValueString($_SERVER['HTTP_USER_AGENT'], "text"));
			$Result1 = mysql_query($insertSQL, $channel1media) or die(mysql_error());
			$sessionDbId = mysql_insert_id();
		}
		*/

		$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=1";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
		$replicateFrom = $row_eb['replicateFrom'];
	}
	if ($replicateFrom == 0) {
		$replicateFrom = 1;
	}
	if (isset($row_eb['section'])) {
	} else {
		$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$replicateFrom";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
	}
	if (strlen($row_eb['sectionOff'])>0) {
		$arrSectionOff = explode(",", $row_eb['sectionOff']);
	}
	if (strlen($row_eb['section'])>0) {
		$arrSection = explode(",", $row_eb['section']);
	} else {
		$arrSection = $arrSection0;
		$arrSectionOff = array();
	}
	$sections = "";
	for ($i=0; $i<count($arrSection); $i++) {
		if (!isSectionOff($arrSection[$i])) {
			if ($sections != "") {
				$sections .= ",";
			}
			$sections .= $arrSection[$i];
		}
	}
	if ($dbId > 0) {
		$multiSummary = false;
		if (isset($_arrEditable)) {
			if (count($_arrEditable) > 1) {
				$multiSummary = true;
			}
		}
		if ($multiSummary) {
			$query_eb = "SELECT * FROM " . $_dbname . "_summary WHERE `presentationId`=$dbId";
			$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
			$row_eb = mysql_fetch_assoc($eb);
			$totalRows_eb = mysql_num_rows($eb);
			if ($totalRows_eb > 0) {
				do {
					$sid = $row_eb['sectionId'];
					if (isset($row_eb['title'])) {
						$arrSummaryTitle[$sid] = $row_eb['title'];
						if (strlen($arrSummaryTitle[$sid]) > 2) {
							$isSummaryOn = true;
						}
					}
					if (isset($row_eb['copy'])) {
						$arrSummaryCopy[$sid] = $row_eb['copy'];
					}
					if (isset($row_eb['photo'])) {
						$arrSummaryPhoto[$sid] = $row_eb['photo'];
					}
					if (isset($row_eb['attachment'])) {
						$arrSummaryFile[$sid] = $row_eb['attachment'];
					}
					if (isset($row_eb['attachmentTitle'])) {
						$arrSummaryFileTitle[$sid] = $row_eb['attachmentTitle'];
					}

				} while ($row_eb = mysql_fetch_assoc($eb));
			}
		} else {
			$query_eb = "SELECT * FROM " . $_dbname . "_summary WHERE `presentationId`=$dbId";
			$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
			$row_eb = mysql_fetch_assoc($eb);
			$totalRows_eb = mysql_num_rows($eb);
			if ($totalRows_eb > 0) {
				if (isset($row_eb['title'])) {
					$summaryTitle = $row_eb['title'];
					if (strlen($summaryTitle) > 2) {
						$isSummaryOn = true;
					}
				}
				if (isset($row_eb['copy'])) {
					$summaryCopy = $row_eb['copy'];
				}
				if (strlen($row_eb['photo'])>3) {
					$summaryPhoto = $row_eb['photo'];
				}
				if (strlen($row_eb['attachment'])>3) {
					$summaryFile = $row_eb['attachment'];
				}
			}
		}
	}

	if (isset($_fileUpload)) {	//red wings multiple files upload
		if ($_fileUpload > 0) {
			$query_eb = "SELECT * FROM " . $_dbname . "_upload WHERE `presentationId`=$dbId AND `attachment` IS NOT NULL AND `attachment`<>''";
			$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
			$row_eb = mysql_fetch_assoc($eb);
			$totalRows_eb = mysql_num_rows($eb);
			if ($totalRows_eb > 0) {
				$arrFiles[] = $row_eb['attachment'];
			}
		}
	}

	if ($repId > 0) {
		$query_eb = "SELECT * FROM " . $_dbname . "_reps WHERE `id`=$repId";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
		if ($row_eb['rep'] != "Admin") {
			$rep = $row_eb['rep'];
			$repEmail = $row_eb['email'];
			$repPhone = $row_eb['phone'];
		}
	}
	$_SESSION['isSpecialAccount'] = $isSpecialAccount;
	$_SESSION['purl'] = $strUrl;
	$_SESSION['sessionId'] = $sessionId;
	$arrSection = explode(',', $sections);
	require_once('newbiz.php');
}
?>