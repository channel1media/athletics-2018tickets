<?php
# FileName="Connection_php_mysql.htm"
# Type="MYSQL"
# HTTP="true"
define("ENV", "production");

if(ENV == "development") {
    $hostname_channel1media = "localhost";
    $database_channel1media = "white_sox";
    $username_channel1media = "root";
    $password_channel1media = "webandsoftware";
} else {
    $hostname_channel1media = "localhost";
    $database_channel1media = "channel1media_com_-_mlb2017";
    $username_channel1media = "adminC1MS";
    $password_channel1media = "zQAFS4hS5DBSxUVs";
}

$channel1media = mysql_pconnect($hostname_channel1media, $username_channel1media, $password_channel1media) or trigger_error(mysql_error(),E_USER_ERROR); 

function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
function getUniqueFilename($r) {
	$r = str_replace("^", "", $r);
	$r = str_replace("+", "", $r);
	$r = str_replace("/", "", $r);
	$r = str_replace("*", "", $r);
	$r = str_replace("(", "", $r);
	$r = str_replace(")", "", $r);
	$r = str_replace("#", "", $r);
	$r = str_replace("?", "", $r);
	$r = str_replace("=", "", $r);
	$r = str_replace("&", "", $r);
	$r = str_replace("'", "", $r);
	$r = str_replace(".", "", $r);
	$r = str_replace(",", "", $r);
	$r = str_replace("\"", "", $r);
	$r = str_replace(" ", "", $r);
	$r = str_replace("%", "", rawurlencode($r));
	if (strlen($r)>50) {
		$r = substr($r, 0, 50);
	}
	return $r . "-" . date("U");
}
function updateVersion($app) {
	global $channel1media;
	$updateSQL = "UPDATE app_version SET `version`=" . date("U") . " WHERE `app`='" . $app . "'";
	$Result1 = mysql_query($updateSQL, $channel1media) or die(mysql_error());
}
function getTimeNow() {
	$et = mktime(date("H"), date("i"), date("s"), date("m"), date("d"), date("Y"));
	$s = date("Y") . date("m") . date("d") . date("H") . date("i") . date("s");
	return getTheTime($s);
}
function getTheTime($s) {
	if (strlen($s)>10) {
		$t = mktime(substr($s, 8, 2), substr($s, 10, 2), substr($s, 12, 2), substr($s, 4, 2), substr($s, 6, 2), substr($s, 0, 4));
		return (date("Y", $t) . "-" . date("m", $t) . "-" . date("d", $t) . ", " . date("H", $t) . ":" . date("i", $t) . ":" . date("s", $t));
	} else {
		return "";
	}
}
function getRandom() {
	$s = "";
	for ($i=0; $i<4; $i++) {
		$s .= rand(0, 9);
	}
	return $s;
}
setlocale(LC_MONETARY, 'en_US');
function getNumberValue($s) {
	$s = str_replace("(", "", $s);
	$s = str_replace(")", "", $s);
	$s = str_replace("$", "", $s);
	$s = str_replace(",", "", $s);
	$s = str_replace(" ", "", $s);
	if ($s == "" || $s == "-" || $s == "--") {
		return 0;
	} else {
		return (float)$s;
	}
}
function getIntString($n) {
	$s = "" . getNumberValue($n);
	$a = explode(".", $s);
	return $a[0];
}
function getMoneyString($n) {
	return "$" . number_format(getNumberValue($n), 2, ".", "," );
}
function getMoneyStringInt($n) {
	$s = "$" . number_format(getNumberValue($n), 0, ".", "," );
	return $s;
}
function getStringInt($n) {
	$s = number_format(getNumberValue($n), 0, ".", "," );
	return $s;
}
function getPurl($r) {
	$r = str_replace("Mr. & Mrs. ", "", $r);
	$r = str_replace("Mr. ", "", $r);
	$r = str_replace("Mrs. ", "", $r);
	$r = str_replace("Ms. ", "", $r);
	$r = str_replace(" and ", " ", $r);
	$r = str_replace(" & ", " ", $r);
	$r = str_replace("^", "", $r);
	$r = str_replace("+", "", $r);
	$r = str_replace("/", "", $r);
	$r = str_replace("*", "", $r);
	$r = str_replace("(", "", $r);
	$r = str_replace(")", "", $r);
	$r = str_replace("#", "", $r);
	$r = str_replace("?", "", $r);
	$r = str_replace("=", "", $r);
	$r = str_replace("&", "", $r);
	$r = str_replace("'", ".", $r);
	$r = str_replace("..", ".", $r);
	$r = str_replace("..", ".", $r);
	$r = str_replace(",", ".", $r);
	$r = str_replace("\"", "", $r);
	$r = str_replace(" ", ".", $r);
	if (strlen($r)>35) {
		$r = substr($r, 0, 35);
		$p = strrpos($r, '.', -1);
		$r = substr($r, 0, $p-1);
	}
	$r = $r . "." . getRandom();
	$r = str_replace("...", ".", $r);
	$r = str_replace("..", ".", $r);
	if (substr($r, 0, 1) == ".") {
		$r = substr($r, 1);
	}
	return strtolower($r);
}
function checkStrUrl($s) {
	$str = "ico,js,txt,html,png,jpg,jpeg,gif,php,css,wotf,css";
	$a = explode(",", $str);
	$r = true;
	for ($i=0; $i<count($a); $i++) {
		$es = "." . $a[$i];
		if (strlen($s)>strlen($es)) {
			if (substr($s, strlen($s)-strlen($es)) == $es) {
				$r = false;
				break;
			}
		}
	}
	return $r;
}
$strUrl = $_SERVER["SERVER_NAME"];
$arrUrl = explode(".", $strUrl);
$strUrl = $arrUrl[0];
$arrUrl = explode("//", $strUrl);
$strUrl = $arrUrl[count($arrUrl)-1];
$arrUrl = explode(".", $strUrl);

date_default_timezone_set('America/Denver');
$_team = "Oakland Athletics";
$_teamname = "athletics";
$_dbname = "athletics2018cms";
$_url = "http://athletics.brochure-mlb.com/2018tickets/";
$_defaultPurl = "admin.0000";
$_defaultPhoto = "custom-default.png";
$_deleteEnabled = true;
$_multipleMaster = false;
$_subLevel = true;
$_attachmentEnabled = true;
$_emailCopy = "to check it out";
$_showValue = true;
$_fileTitle = true;
$_arrEditable = array();
$_arrEditable[] = 100;
?>