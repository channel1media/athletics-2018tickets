<?php 
$targetUrl = "";

$arrUrl = array();

//if (isset($_GET["firstname"])) {
	//$params = "?firstname=" . $_GET["firstname"] . "&lastname=" . $_GET["lastname"] . "&email=" . $_GET["email"];
//}
$arrUrl["Buy"] = "http://purchase.tickets.com/buy/MLBEventInfo?orgid=25565&agency=TGEV_GROUP_DEP";
$arrUrl["GroupInfo"] = "http://m.mlb.com/tigers/tickets/group/";
$arrUrl["GroupTickets"] = "http://m.mlb.com/tigers/tickets/group/?partnerId=5153V6I1R-4I2";
$arrUrl["PartySuites"] = "http://m.mlb.com/tigers/tickets/suites/schedule-and-pricing?partnerId=515Z3AB6W1-12";
$arrUrl["PartyAreas"] = "http://m.mlb.com/tigers/tickets/info/party-areas?partnerId=1C9N1K5361-P4K4";

$arrUrl["Terms"] = "http://detroit.tigers.mlb.com/det/help/det_help_about_terms.jsp?partnerId=715361FDVY-38";
$arrUrl["Privacy"] = "http://detroit.tigers.mlb.com/det/help/det_help_about_privacy.jsp?partnerId=715361FDVY-38";
$arrUrl["DotCom"] = "http://detroit.tigers.mlb.com/index.jsp?c_id=det&partnerId=291IY536H1I-3G4";

$arrUrl["HomeGames"] = "http://detroit.tigers.mlb.com/schedule/index.jsp?c_id=det#y=2017&m=4&calendar=DEFAULT&partnerId=L1H115261-0GC";

$arrUrl["Program"] = "http://detroit.tigers.mlb.com/det/ticketing/ticketexchange.jsp?partnerId=X379W61-3C";
$arrUrl["AccountChanges"] = "http://m.mlb.com/tigers/tickets/season/info?partnerId=31B5221-1D0MP";
$arrUrl["Unused"] = "http://m.mlb.com/tigers/tickets/season/exchange?partnerId=2315R2Y41-P3S0W";
$arrUrl["Rainouts"] = "http://detroit.tigers.mlb.com/det/ticketing/rainout.jsp?partnerId=X379W61-3C";
$arrUrl["KidsClub"] = "http://tigers.com/kidsclub";

$targetUrl = $arrUrl[$_GET["go"]];

//echo $targetUrl;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-59768036-22', 'auto');
			ga('send', 'pageview');
		</script>
		<?php if ($targetUrl != "") { ?>
		<meta http-equiv="REFRESH" content="0;url=<?php echo $targetUrl; ?>" />
		<?php } ?>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	</head>
</html>