<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
} else {
	header('Location: index.php');
	exit;
}
$pid = 0;
if (isset($_GET['id'])) {
	$pid = $_GET['id'];
}
$repPassword = $_teamname;
if (isset($_repPassword)) {
	$repPassword = $_repPassword;
}
function getRepName($r) {
	$ar = explode(" ", $r);
	if (count($ar) == 1) {
		return $ar[0];
	} else {
		return $ar[0] . " " . substr($ar[1], 0, 1) . ".";
	}
}
function removeFirstSpace($s) {
	for ($i=0; $i<5; $i++) {
		if (substr($s, 0, 1) == " ") {
			$s = substr($s, 1);
		} else {
			break;
		}
	}
	return $s;
}
function getPercentString($n) {
	$r = getIntString($n * 100);
	if (strlen($r) == 1) {
		$r = "00" . $r;
	} elseif (strlen($r) == 2) {
		$r = "0" . $r;
	}
	$r = substr($r, 0, strlen($r)-2) . "." . substr($r, strlen($r)-2);
	return $r . "%";
}

$mykey = "rep";
$mydirection = 0;
$myorder = "ASC";
if (isset($_GET['s'])) {
	$mykey = $_GET['s'];
}
if (isset($_GET['d'])) {
	if ($_GET['d'] == 1) {
		$mydirection = 1;
		$myorder = "DESC";
	}
}
function getSortDirection($s) {
	global $mykey, $mydirection;
	$d = 0;
	if ($s == $mykey) {
		$d = 1-$mydirection;
	}
	return "s=" . $s . "&d=" . $d;
}
function getArrow($s) {
	global $mykey, $mydirection;
	$d = 0;
	$extra = "";
	if ($s == $mykey) {
		if ($mydirection == 1) {
			return "down-blue";
		} else {
			return "up-blue";
		}
	} else {
		return "up-grey";
	}
}

mysql_select_db($database_channel1media, $channel1media);

$query_eb = "SELECT `repId`, COUNT(*) AS cou FROM " . $_dbname . "_presentations WHERE `id`>10 AND `active`=1 GROUP BY `repId`";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrPres = array();
$i = 0;
if ($totalRows_eb > 0) {
	do {
		$arrPres[$row_eb['repId']] = $row_eb['cou'];
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$query_eb = "SELECT * FROM " . $_dbname . "_reps WHERE `id`>10 AND `active`=1 ORDER BY `$mykey` $myorder";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrReps = array();
$i = 0;
if ($totalRows_eb > 0) {
	do {
		$arrReps[$i] = $row_eb; 
		$i++;
	} while ($row_eb = mysql_fetch_assoc($eb));
}
function getPres($rid) {
	global $arrPres;
	if (isset($arrPres[$rid])) {
		return $arrPres[$rid];
	} else {
		return 0;
	}
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Reps</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'Casper'; font-size:16px; line-height:20px;}
#mainContainer .mainContent .content-block span {position:relative; display:inline-block; vertical-align:top; padding:17px 0;}
#mainContainer .mainContent .content-block .rep {width:190px; padding-left:10px; white-space:nowrap; overflow:hidden; margin-right:20px;}
#mainContainer .mainContent .content-block .phone {width:110px; }
#mainContainer .mainContent .content-block .email {width:265px; }
#mainContainer .mainContent .content-block .pres {width:70px; }
#mainContainer .mainContent .content-block img {vertical-align:middle;}
#mainContainer .mainContent .content-block .line-break-grey {margin:0px !important;}

#mainContainer .mainContent .title-block {font-family:'ProximaNova-Bold'; text-transform:uppercase; font-size:13px; color:#494949;}
#mainContainer .mainContent .title-block .icon {margin-left:5px;}
#mainContainer .mainContent .title-block a {color:#494949; text-decoration:none;}
#mainContainer .mainContent .title-block a img {display:inline-block; margin-left:0px; margin-bottom:3px;}
#mainContainer .mainContent .item-block {/*background-color:#f2f2f2;*/ /*overflow:hidden;*/ height:auto; border-bottom:1px solid #f2f2f2; border-radius:3px;}
#mainContainer .mainContent .item-block img {margin-right:10px;}

#mainContainer .mainContent .item-block a {position:relative; display:inline-block; transition:background-color 0.3s; vertical-align:top; width:auto; color:#fff; background:none; background-color:#575757; height:36px; line-height:36px; border:none; border-radius:5px; font-size:13px; text-align:center; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none; padding:0 15px; margin:3px;}
#mainContainer .mainContent .item-block a img {transition:filter 0.3s;}
#mainContainer .mainContent .item-block .btn-img-only img {margin-right:0;}
#mainContainer .mainContent .item-block .btn-img-only {padding:0 15px;}
#mainContainer .mainContent .item-block .btn-edit:hover {background-color:#77cdd7;}
#mainContainer .mainContent .item-block .btn-edit:hover img {-webkit-filter:brightness(200%); filter:brightness(200%);}

#mainContainer .mainContent .item-block .btn-reset:hover {background-color:#77cdd7;}
#mainContainer .mainContent .item-block .btn-delete:hover {background-color:#ff0000;}
#mainContainer .mainContent .item-block .btn-delete:hover img {-webkit-filter:brightness(200%); filter:brightness(200%);}

#mainContainer .mainContent .btn-add-rep {position:relative; display:block; transition:background-color 0.3s, color 0.3s; width:100%; height:45px; padding:0px; background-color:#f2f2f2; margin:20px 0px; text-align:center; font-family: 'ProximaNova-Bold'; font-size:13px; line-height:45px; color:#444444; text-transform:uppercase; text-decoration:none;}
#mainContainer .mainContent .btn-add-rep img {display:inline-block; position:relative; transition:filter 0.3s; vertical-align:middle;  margin-right:10px;}
#mainContainer .mainContent .btn-add-rep:hover {background-color:#444444; color:#fff;}
#mainContainer .mainContent .btn-add-rep:hover img {-webkit-filter:brightness(400%); filter:brightness(400%);}

</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
var rid;
var repId = 0;
var repName = "";
var repPhone = "";
var repEmail = "";
var repTitle = "";
var repPassword = "<?php echo $repPassword; ?>";
var arrReps = new Array();
<?php if (count($arrReps)>0) {
	for ($i=0; $i<count($arrReps); $i++) {?>
		rid = <?php echo $arrReps[$i]['id']; ?>;
		arrReps["r"+rid] = new Array();
		arrReps["r"+rid]['rep'] = "<?php echo rawurlencode($arrReps[$i]['rep']); ?>";
		arrReps["r"+rid]['phone'] = "<?php echo rawurlencode($arrReps[$i]['phone']); ?>";
		arrReps["r"+rid]['email'] = "<?php echo rawurlencode($arrReps[$i]['email']); ?>";
		arrReps["r"+rid]['title'] = "<?php echo rawurlencode($arrReps[$i]['title']); ?>";
<?php }} ?>
function getRepInfo(rid) {
	repId = rid;
	repName = unescape(arrReps['r'+repId]['rep']);
	repPhone = unescape(arrReps['r'+repId]['phone']);
	repEmail = unescape(arrReps['r'+repId]['email']);
	repTitle = unescape(arrReps['r'+repId]['title']);
}
function onMouseClickEdit(rid) {
	getRepInfo(rid);
	jQuery("#pop-action .pop-up-message").html("Modify Rep Info");
	openRepInfo();
	return false;
}
function onMouseClickAddRep() {
	repId = 0;
	repName = "";
	repPhone = "";
	repEmail = "";
	repTitle = "";
	jQuery("#pop-action .pop-up-message").html("Add a Rep");
	openRepInfo();
	return false;
}
function openRepInfo() {
	jQuery("#rep-name").val(repName);
	jQuery("#rep-phone").val(repPhone);
	jQuery("#rep-email").val(repEmail);
	jQuery("#rep-title").val(repTitle);
	openPopAction();
}
function onMouseClickResetPassword(rid) {
	getRepInfo(rid);
	confirmAction("Are you sure you want to reset password to " + repPassword + " for " + repName + "?", "reset-password");
	return false;
}
function onMouseClickDelete(rid) {
	getRepInfo(rid);
	confirmAction("Are you sure you want to remove rep " + repName + "?", "delete-rep");
	return false;
}
function onConfirmAction(s) {
	if (s == "reset-password") {
		var info = {};
		info.rid = repId;
		info.action = "reset-password";
		jQuery.post("submitRepInfo.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				flashMsg("Password has been reset to " + repPassword + " for " + repName + ".");
			} else {
				alertMsg("server-error");
			}
		});
	} else if (s == "delete-rep") {
		var info = {};
		info.rid = repId;
		info.action = "delete-rep";
		jQuery.post("submitRepInfo.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				location.reload();
			} else {
				alertMsg("server-error");
			}
		});
	}
}
function submitRepInfo() {
	var info = {};
	info.rid = repId;
	if (repId == 0) {
		info.action = "add-rep";
	} else {
		info.action = "edit-rep";
	}
	info.rep = jQuery("#rep-name").val();
	info.phone = jQuery("#rep-phone").val();
	info.email = jQuery("#rep-email").val();
	info.title = jQuery("#rep-title").val();
	if (info.rep != "" && info.email.indexOf("@")>1) {
		jQuery.post("submitRepInfo.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				location.reload();
			} else if (r == "email-exists") {
				alertMsg("This email address is already in the rep list. Please check and make sure you are not adding an duplicate.");
			} else {
				alertMsg("server-error");
			}
		});
	} else {
		alertMsg("Please make sure all info is valid.");
	}
}
function initIt() {
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body class="body-with-footer">
<div id="mainContainer" class="main-no-bottom">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"></div>
			<div class="top-center-right">
				<a href="#" onClick="return window.close();" class="btn btn-grey" id="btnSave">Exit</a>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Manage Reps</div>
		<a class="btn-add-rep" href="#" onClick="return onMouseClickAddRep();"><img src="images/icon-add-rep.png" /><span>Add a Rep</span></a>
		<?php if (count($arrReps) > 0) { ?>
		<div class="content-block title-block">
			<a href="reps.php?<?php echo getSortDirection('rep'); ?>"><span class="rep">Rep Name<img src="images/arrow-sort-<?php echo getArrow('rep'); ?>.png" /></span></a>
			<a href="reps.php?<?php echo getSortDirection('phone'); ?>"><span class="phone">Phone<img src="images/arrow-sort-<?php echo getArrow('phone'); ?>.png" /></span></a>
			<a href="reps.php?<?php echo getSortDirection('email'); ?>"><span class="email">Email<img src="images/arrow-sort-<?php echo getArrow('email'); ?>.png" /></span></a>
			<a ><span class="pres"># of Pres</span></a>
			<div class="line-break-grey"></div>
		</div>
		<?php for ($i=0; $i<count($arrReps); $i++) {
				$id = $arrReps[$i]['id'];
				?>
		<div class="content-block item-block" id="r<?php echo $id; ?>">
			<span class="rep"><?php echo rawurldecode($arrReps[$i]['rep']); ?></span>
			<span class="phone"><?php echo $arrReps[$i]['phone']; ?></span>
			<span class="email"><?php echo $arrReps[$i]['email']; ?></span>
			<span class="pres"><?php echo getPres($arrReps[$i]['id']); ?></span>
			<a href="#" onClick="return onMouseClickEdit(<?php echo $id; ?>);" class="btn-edit" ><img src="images/icon-edit-presentation.png" />Edit</a>
			<a href="#" onClick="return onMouseClickResetPassword(<?php echo $id; ?>);" class="btn-reset" ><img src="images/icon-lock.png" />Reset Password</a>
			<a href="#" onClick="return onMouseClickDelete(<?php echo $id; ?>);" class="btn-img-only btn-delete"><img src="images/icon-delete-presentation.png" /></a>
		</div>
		<?php }} ?>
	</div>
	<div class="footer">
		<p class="footer-copy">&copy; Copyright 2015 &bull; Channel 1 Media Solutions &bull; All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &bull; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.channel1media.com" target="_blank">www.channel1media.com</a></p>
		<p class="footer-logo"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c.png" /></a></p>
	</div>
	<div id="pop-action" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message">Rep Info:</span>
			<input name='rep-name' id='rep-name' class='myinput' type='text' placeholder='Rep Name' />
			<input name='rep-phone' id='rep-phone' class='myinput' type='text' placeholder='Phone Number' />
			<input name='rep-email' id='rep-email' class='myinput' type='text' placeholder='Email Address' />
			<input name='rep-title' id='rep-title' class='myinput' type='text' placeholder='Job Title' />
			<a class="btn-cancel" href="#" onClick="return closePopAction();">Cancel</a>
			<a id="btnOK" class="btn-ok" href="#" onClick="return submitRepInfo();">OK</a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>