<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
	if ($repId == 1) {
		//header('Location: tree.php?id=1');
		//exit;
	}
} else {
	header('Location: index.php');
	exit;
}
$timePeriod = "morning";
$thehour = date("H");
if ($thehour > 18) {
	$timePeriod = "evening";
} elseif ($thehour > 12) {
	$timePeriod = "afternoon";
}
$aRep = explode(" ", $_SESSION['rep']);
$strGreeting = $aRep[0];
if ($strGreeting == "" || $strGreeting == "Admin") {
	$strGreeting = "";
} else {
	$strGreeting = ", $strGreeting";
}
$multipleMaster = false;
if (isset($_multipleMaster)) {
	$multipleMaster = $_multipleMaster;
}

?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Dashboard</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">

#mainContainer .mainContent .dashboard-option {position:relative; display:block; max-width:980px;}
#mainContainer .mainContent .dashboard-option .icon-dashboard {position:relative; display:inline-block; vertical-align:middle; width:92px; height:67px; margin:15px 20px 20px 5px;}
#mainContainer .mainContent .dashboard-option .option {position:relative; display:inline-block; vertical-align:middle; width:calc(100% - 480px); max-width:500px; height:90px; margin:0 100px 0 20px;}
#mainContainer .mainContent .dashboard-option .option .option-title {font-size:21px; margin-bottom:10px;}
#mainContainer .mainContent .dashboard-option a {position:relative; display:inline-block; transition:background-color 0.3s; vertical-align:middle; width:232px; color:#fff; background:none; background-color:#77ced8; height:50px; line-height:50px; border:none; border-radius:5px; font-size:13px; text-align:center; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none;}
#mainContainer .mainContent .dashboard-option a:hover {background-color:#469fa9;}
</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
var repId = <?php echo $repId; ?>;
var password = "";
function submitPassword() {
	password = jQuery("#password").val();
	var password2 = jQuery("#password2").val();
	if (password.length < 5) {
		alertMsg("Password must be at least 5 characters.");
	} else if (password != password2) {
		alertMsg("Your new password and confirming password do not match.");
	} else {
		confirmAction("Are you sure you want to change your log in password?", "change-password");
	}
	return false;
}
function onConfirmAction(s) {
	var info = {};
	info.repId = repId;
	info.password = password;
	info.r = Math.random();
	jQuery.post("submitChangePassword.php", info, function(data) {
		var r = data + "";
		if (r == "success") {
			flashMsg("Your login password has been changed successfully.");
		} else {
			alertMsg("server-error");
		}
	});
	closePopAction();
}
function initIt(){
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body class="body-with-footer">
<div id="mainContainer" class="main-no-bottom">
	<div class="top">
		<div class="top-center">
			<img class="icon-epitch" src="images/epitch.png" />
			<div class="presentation-name"></div>
			<div class="top-center-right">
				<div class="mycontrol">
					<a href="#" class="myaccount" onClick="return onMouseClickMyControl();">
						<p class="team-logo"></p>
						<p class="myinfo"><?php echo $_SESSION['rep']; ?><span class="myteam"><?php echo $_team; ?></span></p>
						<img class="arrow-down" src="images/arrow-down.png" />
					</a>
					<a href="#" onClick="return openPopAction();" class="control-btn">Change Password</a>
					<a href="http://c1ms.com/ePitch/ePitchLiteManual2.0.pdf" target="_blank" class="control-btn">ePitch Manual</a>
					<?php if ($repId == 1) { ?>
					<a href="http://c1ms.com/ePitch/ePitchManualAdmin.pdf" target="_blank" class="control-btn">ePitch Manual - Admin</a>
					<?php } ?>
					<a href="index.php?a=logout" class="control-btn">Log Out</a>
				</div>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Dashboard</div>
		<p class="page-note">Good <?php echo $timePeriod; ?><?php echo $strGreeting; ?>. Select an option below:</p>
		<div class="line-break-grey"></div>
		<?php if ($repId == 1) { ?>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard6.png" />
			<div class="option">
				<p class="option-title">Edit the master presentation</p>
				<p>Edit the master presentation that all presentations are created from.</p>
			</div>
			<a href="tree.php?id=1" class="btn">Edit Master Presentation</a>
		</div>
		<div class="line-break-grey"></div>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard3.png" />
			<div class="option">
				<p class="option-title">View all created presentations</p>
				<p>See all the presentations reps have created and view analytics and stats on those sent presentations.</p>
			</div>
			<a href="all-presentations.php" target="_blank" class="btn">View Presentations</a>
		</div>
		<div class="line-break-grey"></div>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard5.png" />
			<div class="option">
				<p class="option-title">Manage Reps</p>
				<p>Add, Remove and edit your reps. Also reset your reps passwords.</p>
			</div>
			<a href="reps.php" target="_blank" class="btn">Manage Reps</a>
		</div>
		<div class="line-break-grey"></div>
		<?php } else { ?>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard1.png" />
			<div class="option">
				<p class="option-title">Create a new presentation</p>
				<p>Create a customized presentation! Select which pages are visible and add a unique summary page.</p>
			</div>
			<?php if ($multipleMaster) { ?>
			<a href="all-presentations.php" class="btn">Create Presentation</a>
			<?php } else { ?>
			<a href="presentation.php" class="btn">Create Presentation</a>
			<?php } ?>
		</div>
		<div class="line-break-grey"></div>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard2.png" />
			<div class="option">
				<p class="option-title">View + edit your created presentations</p>
				<p>See all the presentations you have created. Make edits to existing presentations and view analytics and stats on sent presentations.</p>
			</div>
			<a href="my-presentations.php" class="btn">View Presentations</a>
		</div>
		<div class="line-break-grey"></div>
		<div class="dashboard-option">
			<img class="icon-dashboard" src="images/icon-dashboard3.png" />
			<div class="option">
				<p class="option-title">Replicate a presentation</p>
				<p>Duplicate a created presentation, then edit it to personalize it for a new client.</p>
			</div>
			<a href="all-presentations.php" class="btn">Replicate a Presentation</a>
		</div>
		<div class="line-break-grey"></div>
		<?php } ?>
	</div>
	<div class="footer">
		<p class="footer-copy">&copy; Copyright 2015 &bull; Channel 1 Media Solutions &bull; All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &bull; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.channel1media.com" target="_blank">www.channel1media.com</a></p>
		<p class="footer-logo"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c.png" /></a></p>
	</div>
	<div id="pop-action" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"><img src="images/icon-lock2.png" /><br />Change your password:</span>
			<input name='myinput' id='password' class='myinput' type='text' placeholder='New Password' />
			<input name='myinput' id='password2' class='myinput' type='text' placeholder='Retype Password' />
			<a class="btn-cancel" href="#" onClick="return closePopAction();">Cancel</a>
			<a class="btn-ok" href="#" onClick="return submitPassword();">OK</a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>