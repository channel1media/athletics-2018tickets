var strActionConfirm = "";
var flashMsgInterval, flashMsgTimer;
jQuery(function(){
	jQuery(document).ready(function(){
		initIt();
		jQuery(".mycontrol").bind('mouseover', onMouseOverMyControl);
		jQuery(".mycontrol").bind('mouseleave', onMouseOutMyControl);
	})
})
function onMouseOverMyControl(event) {
	//alert("aa"+jQuery(".mycontrol").height());
	if (jQuery(".mycontrol").height()<62) {
		var nh = jQuery(".mycontrol").css({'height':'auto'}).height();
		jQuery(".mycontrol").css({'height':'60px'}).animate({'height':nh}, 300);
	}
	event.stopPropagation();
}
function onMouseOutMyControl(event) {
	//alert("aa"+jQuery(".mycontrol").height());
	//if (jQuery(".mycontrol").height()>60) {
		jQuery(".mycontrol").animate({'height':60}, 300);
	//}
	event.stopPropagation();
}
function onMouseClickMyControl(event) {
	if (jQuery(".mycontrol").height()<70) {
		var nh = jQuery(".mycontrol").css({'height':'auto'}).height();
		jQuery(".mycontrol").css({'height':'60px'}).animate({'height':nh}, 300);
	} else {
		jQuery(".mycontrol").animate({'height':60}, 300);
	}
	return false;
}
function alertMsg(s) {
	if (s == "server-error") {
		s = "An error occurred while submitting your edits.  Please try to log out then log in again.";
	}
	if (s != "") {
		jQuery("#pop-up .pop-up-message").html(s);
	}
	jQuery("#pop-up .btn-ok").css({'display':'inline-block'});
	jQuery("#pop-up .btn-cancel, #pop-up .btn-yes").css({'display':'none'});
	jQuery("#pop-action").css({'visibility':'hidden'});
	jQuery("#pop-up").css({'display':'block'});
}
function flashMsg(s) {
	clearInterval(flashMsgInterval);
	jQuery("#flash-msg").css({'opacity':0, 'display':'block'}).animate({'opacity':1}, 500);
	jQuery("#flash-msg .pop-up-message").html(s);
	flashMsgTimer = -1;
	flashMsgInterval = setInterval(onFlashMsg, 500);
}
function onFlashMsg(event) {
	flashMsgTimer++;
	if (flashMsgTimer == 5) {
		jQuery("#flash-msg").animate({'opacity':0}, 300);
	} else if (flashMsgTimer == 6) {
		clearInterval(flashMsgInterval);
		jQuery("#flash-msg").css({'display':'none'});
	}
}
function confirmAction(s, a) {
	strActionConfirm = a;
	jQuery("#pop-up .pop-up-message").html(s);
	jQuery("#pop-up .btn-ok").css({'display':'none'});
	jQuery("#pop-up .btn-cancel, #pop-up .btn-yes").css({'display':'inline-block'});
	jQuery("#pop-action").css({'visibility':'hidden'});
	jQuery("#pop-up").css({'display':'block'});
	return false;
}
function closeAlert() {
	jQuery("#pop-up").css({'display':'none'});
	jQuery("#pop-action").css({'visibility':'visible'});
	return false;
}
function cancelConfirm() {
	jQuery("#pop-up").css({'display':'none'});
	jQuery("#pop-action").css({'visibility':'visible'});
	return false;
}
function yesConfirm() {
	jQuery("#pop-up").css({'display':'none'});
	jQuery("#pop-action").css({'visibility':'visible'});
	onConfirmAction(strActionConfirm);
	return false;
}
function openPopAction() {
	jQuery("#pop-action").css({'display':'block'});
	jQuery("#pop-action").find(".myinput").first().focus();
	return false;
}
function closePopAction() {
	jQuery("#pop-action").css({'display':'none'});
	return false;
}
function openPopExit() {
	jQuery("#pop-exit").css({'display':'block'});
	return false;
}
function closePopExit() {
	jQuery("#pop-exit").css({'display':'none'});
	return false;
}