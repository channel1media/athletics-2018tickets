<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId']) && isset($_GET['id'])) {
	$repId = $_SESSION['repId'];
	$presentationId = $_GET['id'];
} else {
	header('Location: index.php');
	exit;
}
mysql_select_db($database_channel1media, $channel1media);

$levels = 2;
if (isset($_treeLevels)) {
	$levels = $_treeLevels;
}
$addSub1 = false;
if (isset($_addSub1)) {
	$addSub1 = $_addSub1;
}
$allSectionOffDefault = true;
if (isset($_allSectionOffDefault)) {
	$allSectionOffDefault = $_allSectionOffDefault;
}

$query_eb = "SELECT * FROM " . $_dbname . "_sections WHERE `active`=1 ORDER BY `parentId`, `id` ASC";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$amountSection = mysql_num_rows($eb);
$arrSection0 = array();
$arrSectionParent = array();
$arrSectionName = array();
if ($amountSection > 0) {
	do {
		$arrSection0[] = $row_eb['id'];
		$arrSectionParent[$row_eb['id']] = $row_eb['parentId'];
		$arrSectionName[$row_eb['id']] = $row_eb['name'];
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$arrSection = array();
$arrSectionOff = array();

$version = 1;
$purl = "";
$presentationTitle = "";
$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$presentationId";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
if ($row_eb['repId'] != $repId) {
	header('Location: index.php');
	exit;
} else {
	$purl = $row_eb['purl'];
	//if not customized yet, then take the master version (instead of having all as previous)
	if (strlen($row_eb['section']) == 0) {
		$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$version";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
	}
	if (strlen($row_eb['sectionOff'])>0) {
		$arrSectionOff = explode(",", $row_eb['sectionOff']);
	}
	if (strlen($row_eb['section'])>0) {
		$arrSection = explode(",", $row_eb['section']);
	} else {
		$arrSection = $arrSection0;
		$arrSectionOff = array();
		if ($allSectionOffDefault) {
			for ($i=0; $i<count($arrSection); $i++) {
				$arrSectionOff[] = $arrSection[$i];
			}
		}
	}
	$presentationTitle = "Client: " . trim(rawurldecode($row_eb['firstname'] . " " . $row_eb['lastname']));
}
function isRoot($n, $n0, $n1) {
	global $arrSectionParent, $levels;
	$r = false;
	if ($n>=$n0 && $n<=$n1) {
		$r = true;
	} else {
		for ($i=0; $i<$levels; $i++) {
			$n = $arrSectionParent[$n];
			if ($n>=$n0 && $n<=$n1) {
				$r = true;
				break;
			} elseif ($n == 0) {
				break;
			}
		}
	}
	return $r;
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">
#mainContainer .mainContent .dashboard-title {font-family: 'CasperBold'; color:#77cdd7; font-size:29px; line-height:30px; margin:20px 20px 20px 10px;}
#mainContainer .mainContent .dashboard-title img {position:relative; display:inline-block; margin:0 10px 4px 0; vertical-align:middle;}
#mainContainer .mainContent .line-break-grey {position:relative; display:block; width:100%; height:1px; background-color:#f2f2f2; margin:0; }

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'Casper'; font-size:16px; line-height:20px;}


#mainContainer .mainContent .content-block .row {position:relative; display:block; width:auto; height:44px; line-height:42px; border-bottom:1px solid #f2f2f2;}
#mainContainer .mainContent .content-block .row-notselect {border-bottom:1px solid #f2f2f2; color:#565656; background-color:none; border-radius:0; }
#mainContainer .mainContent .content-block .row-select {border:none; color:#fff; background-color:#77cdd7; border-radius:5px;}

#mainContainer .mainContent .content-block .row .btnUp {position:absolute; display:block; width:10px; height:10px; left:10px; top:8px; z-index:1000;}
#mainContainer .mainContent .content-block .row .btnDown {position:absolute; display:block; width:10px; height:10px; left:10px; bottom:8px; z-index:1000;}

#mainContainer .mainContent .content-block img {position:relative; display:inline-block; vertical-align:middle; margin-right:10px;}
#mainContainer .mainContent .content-block .tree {position:relative; display:block; width:auto; }
#mainContainer .mainContent .content-block .row-left {position:relative; display:inline-block; vertical-align:middle; width:calc(100% - 350px); height:42px; margin-right:10px; overflow: hidden;}
#mainContainer .mainContent .content-block .row-left .tree-text {position:relative; display:inline-block; vertical-align:middle; height:42px; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;}
#mainContainer .mainContent .content-block .s0 .row-left .tree-text {width:calc(100% - 140px);}
#mainContainer .mainContent .content-block .s1 .row-left .tree-text {width:calc(100% - 170px);}
#mainContainer .mainContent .content-block .page .row-left .tree-text {width:calc(100% - 210px);}
#mainContainer .mainContent .content-block .row-left .btnOpen {visibility:hidden;}

#mainContainer .mainContent .content-block .row-right {position:relative; display:inline-block; vertical-align:middle; width:340px; height:42px; text-align:right;}
#mainContainer .mainContent .content-block .icon-tree {width:8px; height:27px; margin:0 16px 0 30px;}
#mainContainer .mainContent .content-block .s1 .row-left .icon-arrow {margin-left:30px;}
#mainContainer .mainContent .content-block .s2 .row-left .icon-arrow {margin-left:60px;}
/*#mainContainer .mainContent .content-block .s3 .row-left .icon-arrow {margin-left:90px;}
#mainContainer .mainContent .content-block .s4 .row-left .icon-arrow {margin-left:120px;}*/

#mainContainer .mainContent .content-block .page .row-left .icon-arrow {margin-left:90px; visibility:hidden;}
#mainContainer .mainContent .content-block .addnew a {color:#77cdd7; text-decoration:none;}
#mainContainer .mainContent .content-block .addnew .icon-tree {visibility:hidden;}
#mainContainer .mainContent .content-block .row-right .btnOn {position:relative; display:inline-block; float:right; width:58px; height:25px; margin:10px 10px 0 0; background-image:url(images/btn-on.png); background-repeat:no-repeat; text-align:center; line-height:25px; text-transform:uppercase; font-family:'ProximaNova-Bold'; font-size:13px; color:#fff; text-decoration:none;}
#mainContainer .mainContent .content-block .row-right .btnOn-on {background-image:url(images/btn-on.png) !important;}
#mainContainer .mainContent .content-block .row-right .btnOn-off {background-image:url(images/btn-off.png);}
#mainContainer .mainContent .content-block .row-right .btnEdit {position:relative; display:inline-block; float:right; height:25px; padding:0; text-align:center; line-height:25px; text-transform:uppercase; font-family:'ProximaNova-Bold'; font-size:13px; color:#cfcfcf; text-decoration:none; margin:10px 10px 0 0;}

#mainContainer .mainContent .content-block .row-video {position:relative; display:block; width:auto; height:44px; line-height:42px; border-bottom:1px solid #f2f2f2;}
#mainContainer .mainContent #tree0 .row-left {width:calc(100% - 270px);}
#mainContainer .mainContent #tree0 .row-left .icon-section {margin-left:26px; margin-right:4px;}
#mainContainer .mainContent #tree0 .row-right {width:220px !important;}

#mainContainer .mainContent #temp-row {display:block; position:absolute; left:0; top:0; width:100%; visibility:hidden; z-index:2000;}
#mainContainer .mainContent #temp-hotspot {display:block; position:absolute; left:0; top:0; width:300px; height:44px; /*background-color:#ff00ff;*/ visibility:hidden; z-index:3000;}

<?php if (!$addSub1) { ?>
	.btn-add-sub1 {visibility:hidden !important;}
<?php } ?>

.btn-hidden {visibility:hidden !important;}

</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
var presentationId = <?php echo $presentationId; ?>;
var arrSection = new Array();
var arrSectionParent = new Array();
var arrSectionName = new Array();
var arrSectionHasChild = new Array();
var arrSectionChild = new Array();

var arrEditable = new Array();
<?php if (isset($_arrEditable)) {
	for ($i=0; $i<count($_arrEditable); $i++) { ?>
		arrEditable[<?php echo $_arrEditable[$i]; ?>] = true;
<?php }} ?>

var arrSectionOff = new Array();

var snEditId, snAddSubId, snAddPageId, snDelSubId, snDelPageId;
var changeType = "";

var isAdmin = false;
var editTarget = " target='_blank'";
<?php if ($presentationId <= 10) { ?>
	isAdmin = true;
	editTarget = " target='_blank'";
<?php } ?>

<?php for ($i=0; $i<$amountSection; $i++) {
	if (isset($arrSection[$i])) {
	if ($arrSection[$i] != "") {
	if (rawurldecode($arrSectionName[$arrSection[$i]]) != 'Blank Sub Section') { ?>
	arrSection.push(<?php echo $arrSection[$i]; ?>);
	arrSectionParent[<?php echo $arrSection[$i]; ?>] = <?php echo $arrSectionParent[$arrSection[$i]]; ?>;
	arrSectionName[<?php echo $arrSection[$i]; ?>] = "<?php echo $arrSectionName[$arrSection[$i]]; ?>";
<?php } } } }?>
<?php if (count($arrSectionOff)>0) {
	for ($i=0; $i<count($arrSectionOff); $i++) { ?>
		arrSectionOff[<?php echo $arrSectionOff[$i]; ?>] = true;
<?php }} ?>
var amountSection = arrSection.length;

var levels = <?php echo $levels; ?>;
var subLimit = 20;
function updateList() {
	for (i=0; i<amountSection; i++) {
		arrSectionHasChild[arrSection[i]] = false;
	}
	var s = "";
	var pid0, pid1, pid2, sid, i0, i1, i2, i, sn2, sn1;
	pid0 = 0;
	for (i0=0; i0<amountSection; i0++) {
		sid = arrSection[i0];
		if (arrSectionParent[sid] == pid0) {
			s += "<div id='s" + sid + "' class='row row-notselect section s0'><img src='images/arrow-up-white.png' class='btnUp' id='sbtnUp"+sid+"'><img src='images/arrow-down-white.png' class='btnDown' id='sbtnDn"+sid+"'>";
			s += "<div class='row-left'><img class='icon-tree' src='images/icon-tree.png' /><img id='btnOpen" + sid + "' class='icon-arrow btnOpen' src='images/arrow-right2.png' /><img class='icon-section' src='images/icon-tree-page.png' /><span class='tree-text'>" + unescape(arrSectionName[sid]) + "</span></div><div class='row-right'>";
			s += "<a href='#' id='sbtnOn" + sid + "' class='btnOn'>On</a><img src='images/icon-tree-blank.png' />";
					
			if (arrEditable[sid]) {
				if (presentationId != 1) {
					s += "<a class='btnEdit' id='sbtnEdit"+sid+"' target='_blank' href='page.php?pid="+presentationId+"&sid="+sid+"'><img src='images/icon-tree-edit.png' class='btnEditPage'>Edit</a>";
				}
			} else {
				if (presentationId == 1) {
					s += "<a href='#' id='btnAddSub" + sid + "' class='btnEdit btnAddSub btnFirst btn-add-sub1'><img class='btnAddSub' src='images/icon-tree-add-sub.png' />Add Sub Section</a>";
				}
			}	
			s += "</div></div>";

			pid1 = sid;
			for (i1=0; i1<amountSection; i1++) {
				sid = arrSection[i1];
				if (arrSectionParent[sid] == pid1) {
					//
					if (!arrSectionHasChild[pid1]) {
						arrSectionHasChild[pid1] = true;
						s += "<div id='sc" + pid1 +"' >";
					}
					//
					s += "<div id='s" + sid + "' class='row row-notselect section s1'><img src='images/arrow-up-white.png' class='btnUp' id='sbtnUp"+sid+"'><img src='images/arrow-down-white.png' class='btnDown' id='sbtnDn"+sid+"'>";
					s += "<div class='row-left'><img class='icon-tree' src='images/icon-tree.png' /><img id='btnOpen" + sid + "' class='icon-arrow btnOpen' src='images/arrow-right2.png' /><img class='icon-section' src='images/icon-tree-page.png' /><span class='tree-text'>" + unescape(arrSectionName[sid]) + "</span></div><div class='row-right'>";
					s += "<a href='#' id='sbtnOn" + sid + "' class='btnOn'>On</a></div></div>";
					//
					if (levels == 3) {
						pid2 = sid;
						for (i2=0; i2<amountSection; i2++) {
							sid = arrSection[i2];
							if (arrSectionParent[sid] == pid2) {
								//
								if (!arrSectionHasChild[pid2]) {
									arrSectionHasChild[pid2] = true;
									s += "<div id='sc" + pid2 +"' >";
								}
								//
								s += "<div id='s" + sid + "' class='row row-notselect section s2'><img src='images/arrow-up-white.png' class='btnUp' id='sbtnUp"+sid+"'><img src='images/arrow-down-white.png' class='btnDown' id='sbtnDn"+sid+"'>";
								s += "<div class='row-left'><img class='icon-tree' src='images/icon-tree.png' /><img id='btnOpen" + sid + "' class='icon-arrow btnOpen' src='images/arrow-right2.png' /><img class='icon-section' src='images/icon-tree-page.png' /><span class='tree-text'>" + unescape(arrSectionName[sid]) + "</span></div><div class='row-right'>";
								s += "<a href='#' id='sbtnOn" + sid + "' class='btnOn'>On</a></div></div>";
								//
							}
						}
					}
				}
			}
			if (arrSectionHasChild[pid1]) {
				s += "</div>";
			}
			//
		}
	}
	s += "<div id='temp-row' class='row row-select'></div><div id='temp-hotspot'></div>";
	jQuery("#tree").html(s);
	for (i=0; i<amountSection; i++) {
		id = arrSection[i];
		if (arrSectionOff[id]) {
			jQuery("#sbtnOn"+id).removeClass("btnOn-on").addClass("btnOn-off");
			jQuery("#sbtnOn"+id).html("Off");
		}
	}
	if (rowId != "") {
		highlightRow(rowId, true);
	}
	enableButtons();
}
function notStaticSection($n) {
	var r = true;
	if (arrStaticSections.length > 0) {
		for (var i=0; i<arrStaticSections.length; i++) {
			if ($n == arrStaticSections[i]) {
				r = false;
				break;
			}
		}
	}
	return r;
}
function getSectionPath(sn) {
	var r = arrSectionName[sn];
	for (var i=0; i<3; i++) {
		if (arrSectionParent[sn] != 0) {
			sn = arrSectionParent[sn];
			r = arrSectionName[sn] + " > "+r;
		}
	}
	return unescape(r);
}
function enableButtons() {
	jQuery("#tree .btnOn").bind('click', onMouseClickOn);
	jQuery(".btnUp").bind('click', onMouseClickUp);
	jQuery(".btnDown").bind('click', onMouseClickDown);

	jQuery(".btnEditSectionName").bind('click', onMouseClickSectionEdit);
	jQuery(".btnAddSub").bind('click', onMouseClickAddSub);
	
	jQuery("#btnSave").bind('click', onMouseClickSave);
	
	jQuery(".row").bind('mouseover', onMouseOverRow);
	//jQuery(".row .icon-tree, .row .tree-text").bind('click', onMouseClickRow);
	//jQuery(".row .tree-text").bind('mousedown', onMouseDownRow);

	jQuery(".row, #temp-row, #temp-hotspot").bind('mouseenter', onMouseEnterRow);
	jQuery(".row, #temp-row, #temp-hotspot").bind('mouseleave', onMouseLeaveRow);
}
var changeType;
function onMouseClickSave() {
	changeType = "";
	saveChanges();
}
function saveChanges() {
	var i;
	var info = {};
	info.id = presentationId;
	info.section = arrSection.join(',');
	info.sectionOff = "";
	for (i=0; i<arrSection.length; i++) {
		if (arrSectionOff[arrSection[i]]) {
			if (info.sectionOff != "") {
				info.sectionOff += ",";
			}
			info.sectionOff += arrSection[i];
		}
	}
	jQuery.post("submitTree.php", info, function(data) {
		var r = data + "";
		if (r == "success") {
			if (changeType == "") {
				//flashMsg("Your customization has been saved successfully.");
			} else if (changeType.substr(0, 3) == "Del") {
				onDelete();
			} else if (changeType.substr(0, 3) == "Add") {
				onAddNew();
			}
			jQuery("#btnGenerate").css('opacity', 1);
		} else {
			alertMsg("server-error");
		}
	});
}
function onMouseClickAddSub() {
	snAddSubId = parseInt(jQuery(this).attr('id').substr(9));
	jQuery("#action-message").html("Name the new sub section you are adding <br />(under: " + getSectionPath(snAddSubId) + ")");
	jQuery("#myinput").val("");
	jQuery("#btnOK").attr("href", "javascript:submitAddSub();");
	openPopAction();
	return false;
}
function onMouseClickSectionEdit() {
	snEditId = parseInt(jQuery(this).attr('id').substr(8));
	jQuery("#action-message").html("Change Section Name:");
	jQuery("#myinput").val(unescape(arrSectionName[snEditId]));
	jQuery("#btnOK").attr("href", "javascript:submitEdit();");
	openPopAction();
}
function submitAddSub() {
	changeType = "AddSub";
	saveChanges();
}
function onAddNew() {
	var info = {};
	info.pid = presentationId;
	info.addNewType = changeType.substr(3);
	if (changeType == "AddSub") {
		info.title = jQuery("#myinput").val();
		info.sid = snAddSubId;
	} else if (changeType == "AddPage") {
		info.title = jQuery("#myinput").val();
		info.sid = snAddPageId;
	}
	if (info.title.length > 0) {
		jQuery.post("submitAddNew.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				window.location = "tree.php?id="+presentationId;
			} else {
				alertMsg("server-error");
			}
		});
	}
}
function submitEdit() {
	var info = {};
	info.pid = presentationId;
	info.sid = snEditId;
	info.sectionName = jQuery("#myinput").val();
	if (info.sectionName.length > 0) {
		jQuery.post("submitSectionEdit.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				flashMsg("The section name has been changed and saved successfully.");

				arrSectionName[snEditId] = jQuery("#myinput").val();
				jQuery("#s"+snEditId+" span").html(arrSectionName[snEditId]);
				closePopAction();
			} else {
				alertMsg("server-error");
			}
		});
	}
}
function onMouseClickOn() {
	var btnId = jQuery(this).attr('id');
	var id = parseInt(btnId.substr(6));
	var arr = arrSectionOff;
	if (arr[id]) {
		switchOn(arr, btnId);
	} else {
		switchOff(arr, btnId);
	}
	changeType = "";
	saveChanges();
	return false;
}
function onFinishCustomization() {
	changeType = "";
	saveChanges();
	return true;
}
function switchOn(arr, btnId) {
	var id = parseInt(btnId.substr(6));
	turnItOn(arr, btnId);
	var i, cid;
	if (arr == arrSectionOff) {
		for (i=0; i<arrSection.length; i++) {
			cid = arrSection[i];
			if (arrSectionParent[cid] == id) {
				switchOn(arrSectionOff, "sbtnOn"+cid);
			}
		}
	}	
}
function turnOnParent(id) {
	turnItOn(arrSectionOff, "sbtnOn"+id);
	if (arrSectionParent[id] != 0) {
		turnOnParent(arrSectionParent[id]);
	}
}
function switchOff(arr, btnId) {
	var id = parseInt(btnId.substr(6));
	turnItOff(arr, btnId);
	var i, cid;
	if (arr == arrSectionOff) {
		for (i=0; i<arrSection.length; i++) {
			cid = arrSection[i];
			if (arrSectionParent[cid] == id) {
				switchOff(arrSectionOff, "sbtnOn"+cid);
			}
		}
	}
}
function turnOffParent(id, childIsContent) {
	var arr = arrSection;
	var arrOff = arrSectionOff;
	var arrParent = arrSectionParent;
	var allChildOff = true;
	if (arr.length > 0) {
		for (var i=0; i<arr.length; i++) {
			var nid = arr[i];
			var spid = arrParent[nid];
			if (spid == id) {
				//alert(nid+":"+spid+"/"+arrOff.length+"/"+arrOff[nid]);
				if (arrOff[nid]) {

				} else {
					allChildOff = false;
					break;
				}
			}
		}
	}
	//alert(id+"/"+allChildOff);
	if (allChildOff) {		
		turnItOff(arrSectionOff, "sbtnOn"+id);
		if (arrSectionParent[id] != 0) {
			turnOffParent(arrSectionParent[id], false);
		}
	}
}
function turnItOn(arr, btnId) {
	var id = parseInt(btnId.substr(6));
	arr[id] = false;
	jQuery("#"+btnId).removeClass("btnOn-off").addClass("btnOn-on");
	jQuery("#"+btnId).html("On");
}
function turnItOff(arr, btnId) {
	var id = parseInt(btnId.substr(6));
	arr[id] = true;
	//alert("turnIfOff:"+id+arr[id]);
	jQuery("#"+btnId).removeClass("btnOn-on").addClass("btnOn-off");
	jQuery("#"+btnId).html("Off");
	/* close children when a section is turned off
	if (arr == arrSectionOff) {
		arrSectionOpen[id] = false;
		jQuery("#btnOpen"+id).attr('src', 'images/arrow-right.png');
		jQuery("#sc"+id).css({'display':'none'});
	}
	*/
}
function onMouseClickUp() {
	var btnId = jQuery(this).attr('id');
	var id = parseInt(btnId.substr(6));
	var arr = arrSection;
	var arrParent = arrSectionParent;
	var parentId = arrParent[id];
	var n;
	for (i=0; i<arr.length; i++) {
		if (arr[i] == id) {
			n = i;
			break;
		}
	}
	if (n > 0) {
		for (i=n-1; i>=0; i--) {
			if (arrParent[arr[i]] == parentId) {
				arr[n] = arr[i];
				arr[i] = id;
				updateList();
				break;
			}
		}
	}
	changeType = "";
	saveChanges();
}
function onMouseClickDown() {
	var btnId = jQuery(this).attr('id');
	var id = parseInt(btnId.substr(6));
	var arr = arrSection;
	var arrParent = arrSectionParent;
	var parentId = arrParent[id];
	var n;
	for (i=0; i<arr.length; i++) {
		if (arr[i] == id) {
			n = i;
			break;
		}
	}
	if (n < arr.length-1) {
		for (i=n+1; i<arr.length; i++) {
			if (arrParent[arr[i]] == parentId) {
				arr[n] = arr[i];
				arr[i] = id;
				updateList();
				break;
			}
		}
	}
	changeType = "";
	saveChanges();
}
function ResizeWindow() {
	var width = jQuery(window).width();
	var height = jQuery(window).height();
	var nx = (width-400)/2;
	var ny = (height-160)/2;
	if (nx < 10) {
		nx = 10;
	}
	if (ny < 10) {
		ny = 10;
	}
	jQuery(".editBox").css({'left':nx, 'top':ny});

	jQuery(".pop-library").css({'width':width, 'height':height});
	jQuery(".pop-library .library").css({'left':(jQuery(window).width()-970)/2});
}
function onMouseClickGenerate() {
	if(jQuery("#btnGenerate").css('opacity') == 1) {
		jQuery.post('../../print-pdf.php?id=' + presentationId);
		jQuery("#btnGenerate").css('opacity', 0.3);
	}
	return false;
}
var rowId = "";
var lastRowId = "";
function onMouseClickRow(event) {
	if (rowId != "") {
		highlightRow(rowId, false);
	}
	if (jQuery(this).parents('.row').attr('id')) {
		rowId = jQuery(this).parents('.row').attr('id');
		highlightRow(rowId, true);
	} else {
		rowId = "";
	}
}
var mx0, my0, hotspotOffsetX, hotspotWidth;
function onMouseOverRow(event) {
	var a = new Array("section", "page", "s0", "s1", "s2", "s3", "s4");
	//if (jQuery(this).parents('.row').attr('id')) {
		rowId = jQuery(this).attr('id');
		if (rowId != lastRowId) {
			if (lastRowId != "") {
				highlightRow(lastRowId, false);
			}
			highlightRow(rowId, true);
			jQuery("#temp-row").html(jQuery("#"+rowId).html());
			for (var i=0; i<a.length; i++) {
				if (jQuery("#"+rowId).hasClass(a[i])) {
					jQuery("#temp-row").addClass(a[i]);
				} else {
					jQuery("#temp-row").removeClass(a[i]);
				}
			}
			jQuery("#temp-row").css({'left':jQuery("#"+rowId).position().left, 'top':jQuery("#"+rowId).position().top, 'visibility':'hidden'});
			hotspotOffsetX = jQuery("#temp-row .tree-text").position().left;
			var btnFirstX = 0;
			if (jQuery("#temp-row .btnFirst").position()) {
				btnFirstX = jQuery("#temp-row .btnFirst").position().left;
			}
			hotspotWidth = jQuery("#temp-row .row-right").position().left + btnFirstX - hotspotOffsetX;
			//alert(jQuery("#temp-row .btnFirst").position().left);
			jQuery("#temp-hotspot").css({'left':jQuery("#temp-row").position().left+hotspotOffsetX, 'top':jQuery("#"+rowId).position().top, 'width':hotspotWidth, 'visibility':'visible', 'cursor':'grab', 'cursor':'-moz-grab', 'cursor':'-webkit-grab'});
			//jQuery("#temp-hotspot").css({'left':jQuery("#temp-row").position(), 'top':jQuery("#"+rowId).position().top, 'width':800, 'visibility':'visible', 'cursor':'grab', 'cursor':'-moz-grab', 'cursor':'-webkit-grab'});
			mx0 = event.pageX - jQuery("#"+rowId).position().left;
			my0 = event.pageY - jQuery("#"+rowId).position().top;
			
			jQuery("#temp-hotspot").unbind('mousedown', onMouseDownRow);
			jQuery("#temp-hotspot").unbind('mousemove', onMouseMoveRow);
			jQuery("#temp-hotspot").unbind('mouseup', onMouseUpRow);
			jQuery("#temp-hotspot").bind('mousedown', onMouseDownRow);
			jQuery("#temp-hotspot").bind('mouseup', onMouseUpRow);
			lastRowId = rowId;
		}
	//}
}
function onMouseDownRow(event) {
	if (rowId != "") {
		jQuery("#temp-row").css({'visibility':'visible'});
		mx0 = event.pageX - jQuery("#"+rowId).position().left;
		my0 = event.pageY - jQuery("#"+rowId).position().top;
		jQuery("#"+rowId).css({'opacity':0});
		jQuery("#temp-hotspot").css({'cursor':'grabbing'});
		jQuery("#temp-hotspot").bind('mousemove', onMouseMoveRow);
	}
}
function onMouseMoveRow(event) {
	//alert("onMouseMove");
	var nx = event.pageX-mx0;
	var ny = event.pageY-my0;
	jQuery("#temp-row").css({'left':nx, 'top':ny});
	jQuery("#temp-hotspot").css({'left':nx+hotspotOffsetX, 'top':ny});
}
function onMouseUpRow(event) {
	jQuery("#temp-hotspot").unbind('mousedown', onMouseDownRow);
	jQuery("#temp-hotspot").unbind('mousemove', onMouseMoveRow);
	jQuery("#temp-hotspot").unbind('mouseup', onMouseUpRow);
	jQuery("#temp-hotspot").css({'cursor':'grab'});
	if (jQuery("#temp-row").position().top > jQuery("#"+rowId).position().top) {
		onDropRowDown();
	} else {
		onDropRowUp();
	}
	updateList();
	changeType = "";
	saveChanges();
}
var rowInterval;
function onMouseEnterRow(event) {
	if (jQuery(this).hasClass('addnew')) {
	} else {
		clearInterval(rowInterval);
	}
}
function onMouseLeaveRow(event) {
	clearInterval(rowInterval);
	rowInterval = setInterval(clearTempRow, 300);
}
function clearTempRow(event) {
	jQuery("#temp-row").css({'left':0, 'top':0, 'visibility':'hidden'});
	jQuery("#temp-hotspot").css({'left':0, 'top':0, 'visibility':'hidden'});
	if (lastRowId != "") {
		jQuery("#"+lastRowId).css({'opacity':1});
		highlightRow(lastRowId, false);
		lastRowId = "";
	}
}
function onDropRowDown() {
	var ny = jQuery("#temp-row").position().top;
	var id = parseInt(rowId.substr(1));
	var st = rowId.substr(0, 1);
	var arr = arrSection;
	var arrParent = arrSectionParent;
	var parentId = arrParent[id];
	var arrChild = new Array();
	var arrChildPos = new Array();
	var isStart = false;
	var i, n;
	for (i=0; i<arr.length; i++) {
		if (arr[i] == id) {
			n = i;
			isStart = true;
		} else {
			if (isStart) {
				if (arrParent[arr[i]] == parentId) {
					if (ny > jQuery("#"+st+arr[i]).position().top) {
						arrChild.push(arr[i]);
						arrChildPos.push(i);
					} else {
						break;
					}
				}
			}
		}
	}
	//alert(arrChild);
	if (arrChild.length > 0) {
		for (i=0; i<arrChild.length; i++) {
			if (i == 0) {
				arr[n] = arrChild[i];
			} else {
				arr[arrChildPos[i-1]] = arrChild[i];
			}
		}
		arr[arrChildPos[i-1]] = id;
	}
}
function onDropRowUp() {
	var ny = jQuery("#temp-row").position().top;
	var id = parseInt(rowId.substr(1));
	var st = rowId.substr(0, 1);
	var arr = arrSection;
	var arrParent = arrSectionParent;
	var parentId = arrParent[id];
	var arrChild = new Array();
	var arrChildPos = new Array();
	var isStart = false;
	var i, n;
	for (i=arr.length-1; i>=0; i--) {
		if (arr[i] == id) {
			n = i;
			isStart = true;
		} else {
			if (isStart) {
				if (arrParent[arr[i]] == parentId) {
					if (ny < jQuery("#"+st+arr[i]).position().top) {
						arrChild.push(arr[i]);
						arrChildPos.push(i);
					} else {
						break;
					}
				}
			}
		}
	}
	//alert(arrChild);
	if (arrChild.length > 0) {
		for (i=0; i<arrChild.length; i++) {
			if (i == 0) {
				arr[n] = arrChild[i];
			} else {
				arr[arrChildPos[i-1]] = arrChild[i];
			}
		}
		arr[arrChildPos[i-1]] = id;
	}
}
function highlightRow(did, h) {
	if (!h) {
		//alert(1);
	}
	var t = did.substr(0, 1);
	var id = parseInt(did.substr(1));
	if (h) {
		jQuery("#"+did).removeClass('row-notselect').addClass('row-select');
		jQuery("#"+did+" .btnEdit").css({'color':'#fff'});
		jQuery("#"+did+" .btnAddSub img").attr('src', 'images/icon-tree-add-sub-white.png');
		jQuery("#"+did+" .btnDelete img").attr('src', 'images/icon-tree-delete-white.png');
		jQuery("#"+did+" .btnRename, "+"#"+did+" .btnEditPage").attr('src', 'images/icon-tree-edit-white.png');
		//if (t == "s") {
			//jQuery("#s"+id+" .icon-section").attr('src', 'images/icon-tree-section-select.png');
		//}		
	} else {
		jQuery("#"+did).removeClass('row-select').addClass('row-notselect');
		jQuery("#"+did+" .btnEdit").css({'color':'#cfcfcf'});
		jQuery("#"+did+" .btnAddSub img").attr('src', 'images/icon-tree-add-sub.png');
		jQuery("#"+did+" .btnDelete img").attr('src', 'images/icon-tree-delete.png');
		jQuery("#"+did+" .btnRename, "+"#"+did+" .btnEditPage").attr('src', 'images/icon-tree-edit.png');
		if (t == "s") {
			jQuery("#s"+id+" .icon-section").attr('src', 'images/icon-tree-page.png');
		}
	}
}
function initIt() {
	jQuery(window).resize(function(){ ResizeWindow();});
	ResizeWindow();
	updateList();
	jQuery("#btnGenerate").bind('click', onMouseClickGenerate);
};
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body>
<div id="mainContainer">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"><?php echo $presentationTitle; ?></div>
			<div class="top-center-right">
				<div class="mycontrol">
					<a href="#" class="myaccount" onClick="return onMouseClickMyControl();">
						<p class="team-logo"></p>
						<p class="myinfo"><?php echo $_SESSION['rep']; ?><span class="myteam"><?php echo $_team; ?></span></p>
						<img class="arrow-down" src="images/arrow-down.png" />
					</a>
					<a href="dashboard.php" class="control-btn">Dashboard</a>
					<?php if ($repId > 1) { ?>
					<a href="my-presentations.php?id=<?php echo $presentationId; ?>" class="control-btn">My Custom Presentations</a>
					<?php } ?>
					<a href="http://c1ms.com/ePitch/ePitchLiteManual2.0.pdf" target="_blank" class="control-btn">ePitch Manual</a>
					<?php if ($repId == 1) { ?>
					<a href="http://c1ms.com/ePitch/ePitchManualAdmin.pdf" target="_blank" class="control-btn">ePitch Manual - Admin</a>
					<?php } ?>
					<a href="index.php?a=logout" class="control-btn">Log Out</a>
				</div>
				<?php if ($purl != "") { ?>
				<a href="../../?preview.<?php echo $purl; ?>" target="_new" class="btn btn-grey" id="btnPreview">Preview</a>
				<?php } ?>
				<!--<a href="#" class="btn btn-blue" id="btnSave">Save</a>-->
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="dashboard-title"><img src="images/icon-dashboard.png" />Customize your presentation</div>
		<div class="content-block" id="tree" style="-moz-user-select: none; -webkit-user-select: none; -ms-user-select:none; user-select:none;-o-user-select:none;" unselectable="on" onselectstart="return false;" onmousedown="return false;">
			<div class="row section s0">
				<div class="row-left"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-arrow" src="images/arrow-right.png" /><img src="images/icon-tree-page.png" /><span class="tree-text">Section Level 1</span><div class="hotspot"></div></div><div class="row-right"><a href="#" class="btnEdit"><img src="images/icon-tree-add-sub.png" />Add Sub Section</a><a href="#" class="btnOn">On</a><a href="#" class="btnDelete"><img src="images/icon-tree-delete.png" /></a></div>
			</div>
			<div class="sub-group">
				<div class="row section s1">
					<div class="row-left"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-arrow" src="images/arrow-right.png" /><img src="images/icon-tree-page.png" /><span class="tree-text">Section Level 2</span></div><div class="row-right"><a href="#" class="btnEdit"><img src="images/icon-tree-edit.png" />Rename</a><a href="#" class="btnEdit"><img src="images/icon-tree-add-sub.png" />Add Sub Section</a><a href="#" class="btnOn">On</a><a href="#" class="btnDelete"><img src="images/icon-tree-delete.png" /></a></div>
				</div>
				<div class="row section s2">
					<div class="row-left"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-arrow" src="images/arrow-right.png" /><img src="images/icon-tree-page.png" /><span class="tree-text">Section Level 3</span></div><div class="row-right"><a href="#" class="btnEdit"><img src="images/icon-tree-edit.png" />Rename</a><a href="#" class="btnEdit"><img src="images/icon-tree-add-sub.png" />Add Sub Section</a><a href="#" class="btnOn">On</a><a href="#" class="btnDelete"><img src="images/icon-tree-delete.png" /></a></div>
				</div>
				<div class="row page">
					<div class="row-left"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-arrow" src="images/arrow-right.png" /><img class='icon-page' src="images/icon-tree-page.png" /><span class="tree-text">Page</span></div><div class="row-right"><a href="#" class="btnEdit"><img src="images/icon-tree-edit.png" />Edit</a><a href="#" class="btnOn">On</a><a href="#" class="btnDelete"><img src="images/icon-tree-delete.png" /></a></div>
				</div>
				<div class="row page addnew">
					<div class="row-left"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-arrow" src="images/arrow-right.png" /><img src="images/icon-tree-add-page.png" /><span class="tree-text">Add a page</span></div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="bottom-center">	
			<?php if ($repId > 1) { ?>
			<a href="presentation.php?id=<?php echo $presentationId; ?>" class="bottom-btn<?php if ($presentationId < 10) { ?> btn-hidden<?php } ?>" id="btnBack"><img src="images/arrow-left-white.png" />Back</a>
			
			<div class="step-indicator">
				<img src="images/icon-step.png" />
				<img src="images/icon-step.png" />
				<img src="images/icon-step.png" />
				<img class="my-step" src="images/icon-step.png" />
			</div>
			<a href="my-presentations.php?id=<?php echo $presentationId; ?>" onClick="return onFinishCustomization();" class="bottom-btn" id="btnNext">Next<img src="images/arrow-right-white.png" /></a>
			<?php } ?>
		</div>
	</div>
	<div id="pop-action" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message" id="action-message">Are you sure you want to add a new sub section under:</span>
			<input name='myinput' id='myinput' class='myinput' type='text' value="" />
			<a class="btn-cancel" href="#" onClick="return closePopAction();">Cancel</a>
			<a class="btn-ok" id="btnOK" href="javascript:submitAddSub();">OK</a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
<div id="pop-intro" class="pop-library">
	<div id="library" class="library">
		<div class="library-top">
			<span class="library-title"><img src="images/icon-dashboard.png">Video Library</span>
			<a href="#" onClick="return closeLibrary();" class="btnClose"><img src="images/btn-close.png" /></a>
		</div>
		<div class="library-btns new-video-btns">
			<div class="input-field"><input id="input-video" class="inputslot library-input" type="text" placeholder="Vimeo Video Id" autocomplete="on" /></div>
			<a href="#" id="btnAddVideo" class="btn-library btn-on btn-submit-input">Add</a>
		</div>
		<div class="library-btns confirm-video-btns">
			Are you sure you want to add this new video: 
			<div class="input-field"><input id="input-video-title" class="inputslot library-input" type="text" autocomplete="on" /></div>
			<span class="new-video"></span>
			<a href="#" id="btnYesVideo" class="btn-library btn-on btn-submit-input">Yes</a>
			<a href="#" id="btnCancelVideo" class="btn-library btn-on btn-submit-input">Cancel</a>
		</div>
		<div class="library-thumbs">
			<?php for ($i=0; $i<8; $i++) { ?>
			<div class="file" id="v<?php echo $i; ?>">
				<div class="thumb">
					<div class="file-thumb" data-dbid="" data-vid=""></div>
				</div>
				<p><span class="file-title"></span></p>
			</div>
			<?php } ?>
		</div>
		<a href="javascript:loadVideoLibrary(-1);" class="btnPrev"><img src="images/arrow-left.png" /> Prev</a>
		<a href="javascript:loadVideoLibrary(1);" class="btnMore">More <img src="images/arrow-right.png" /></a>
	</div>
</div>
</body>