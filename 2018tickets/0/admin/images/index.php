<?php
if (isset($_GET['a'])) {
	if ($_GET['a'] == 'logout') {
		if (isset($_SESSION)) {
			session_destroy();
			unset($_SESSION);
		}
	}
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Login</title>
<link href="airport.ico" rel="shortcut icon">
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">
#mainContainer .login {position:relative; display:block; overflow:hidden; width:auto; height:500px; font-size:13px; }
#mainContainer .login .login-back {position:absolute; display:block; }
#mainContainer .login .login-center {position:relative; top:166px; margin:0 auto; width:330px; height:168px; background-image:url(images/grey.png); border-radius:5px;}

#mainContainer .login .login-center p {position:relative;}
#mainContainer .login .login-center .login-title {color:#fff; padding:18px 0 15px 0;}
#mainContainer .login .login-center .login-title .icon-lock {margin-right:5px;}
#mainContainer .login .login-center .inputslot {position:relative; display:inline-block; width:278px; color:#585858; background:none; background-color:#f0f0f0; height:32px; line-height:32px; border:none; border-radius:5px; padding:0 12px; margin-bottom:5px; text-align:left;}
#mainContainer .login .login-center .login-btn {position:relative; display:inline-block; width:278px; color:#fff; background:none; background-color:#77ced8; height:32px; line-height:32px; border:none; border-radius:5px; padding:0 12px; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none;}

#mainContainer .bottom-copy {position:relative; margin-top:20px; }
#mainContainer .logo-c1ms {position:relative; margin-top:35px; }
</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
function ResizeWindow() {
	height = jQuery(window).height();
	width = jQuery(window).width();

	jQuery(".login-top").css({'width':width});
	if (width <= 1600) {
		jQuery(".login-back").css({'width':1600, 'height':500, 'left':(width-1600)/2});
	} else {
		scale = width/1600;
		jQuery(".login-back").css({'width':width, 'height':500*scale, 'left':0});
	}
}
function submitLogin() {
	var info = {};
	info.email = jQuery("#input-email").val();
	info.password = jQuery("#input-password").val();
	jQuery.post("submitLogin.php", info, function(data) {
		var r = data + "";
		if (r == "failed") {
			alert("Wrong email address or password!");
		} else if (r == "success") {
			window.location = 'dashboard.php';
		}
	});
	return false;
}
function onKeyPress(e) {
	if (e.keyCode == 13) {
		submitLogin();
		return false;
	}
}
jQuery(function(){
	jQuery(document).ready(function(){
		ResizeWindow();
		jQuery(window).resize(function(){ ResizeWindow();});
	});
})
</script>
</head>
<body>
<div id="mainContainer">
	<div class="top">
		<div class="top-center">
			<img class="icon-epitch" src="images/epitch.png" />
		</div>
	</div>
	<div class="login">
		<img class="login-back" src="images/login-back.jpg" />
		<div class="login-center">
			<p class="login-title"><img class="icon-lock" src="images/icon-lock.png" />User Login</p>
			<form>
				<p><input id="input-email" class="inputslot" type="text" placeholder="email address" autocomplete="on" /></p>
				<p><input id="input-password" class="inputslot" type="password" placeholder="password" onkeypress="return onKeyPress(event)" autocomplete="on" /></p>
				<a href="#" onclick="javascript:submitLogin();" class="login-btn" id="btnLogin">Login</a>
			</form>
		</div>
	</div>
	<p class="bottom-copy">ePitch <span class="text-thin">The most powerful, easy to use, customizable sales presentation tool developed specifically for sports!</span></p>
	<p class="logo-c1ms"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c1ms.png" /></a></p>
</div>
</body>