<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
mysql_select_db($database_channel1media, $channel1media);
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
	if (isset($_POST['editType'])) {
		if ($_POST['editType']== 'uploadImage') {
			$pid = $_POST['pid'];			
			$sid = $_POST['sid'];	
			$imgFile = 'uploadedImage';
			if ((strpos(strtolower($_FILES[$imgFile]['name']), ".jpg")) || (strpos(strtolower($_FILES[$imgFile]['name']), ".jpeg")) || (strpos(strtolower($_FILES[$imgFile]['name']), ".png"))) {
				$filename = getUniqueFilename(str_replace('.jpeg', '', (str_replace('.jpg', '', strtolower($_FILES[$imgFile]['name']))))) . '.jpg';
				$target_path = "../../upload/" . $filename;
				if(move_uploaded_file($_FILES[$imgFile]['tmp_name'], $target_path)) {					
					$updateSQL = "UPDATE " . $_dbname . "_summary SET `photo`='$filename' WHERE `presentationId`=$pid AND `sectionId`=$sid";
					$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
				} else{
					echo "There was an error uploading the file, please try again!";
				}
			}
			header("Location: page.php?pid=$pid&sid=$sid");
			exit;
		} elseif ($_POST['editType'] == 'uploadAttachment') {
			$pid = $_POST['pid'];		
			$sid = $_POST['sid'];		
			$theFile = 'uploadedFile';
			$filename = $_FILES[$theFile]['name'];
			$af = explode(".", $filename);
			$extname = strtolower($af[count($af)-1]);
			$filename = substr($filename, 0, strlen($filename)-strlen($extname)-1);
			$filename = getUniqueFilename($filename) . ".$extname";
			$target_path = "../../upload/" . $filename;
			if(move_uploaded_file($_FILES[$theFile]['tmp_name'], $target_path)) {					
				$updateSQL = "UPDATE " . $_dbname . "_summary SET `attachment`='$filename' WHERE `presentationId`=$pid AND `sectionId`=$sid";
				$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
			} else{
				echo "There was an error uploading the file, please try again!";
			}
			header("Location: page.php?pid=$pid&sid=$sid");
			exit;
		}
	} elseif (isset($_GET['pid'])) {
		$pid = $_GET['pid'];
		$sid = $_GET['sid'];
	} else {
		header('Location: index.php');
		exit;
	}
} else {
	header('Location: index.php');
	exit;
}

$fileTitle = false;
if (isset($_fileTitle)) {
	$fileTitle = $_fileTitle;
}
$presentationTitle = "";
$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$pid";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
if ($totalRows_eb > 0) {
	$summaryPId = $row_eb['id'];
	$presentationTitle = "Client: " . trim(rawurldecode($row_eb['firstname'] . " " . $row_eb['lastname']));
}

$title = "";
$copy = "";
$photo = "";
$attachment = "";
$attachmentTitle = "No Title";

$query_eb = "SELECT * FROM " . $_dbname . "_summary WHERE `presentationId`=$pid AND `sectionId`=$sid";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
if ($totalRows_eb > 0) {
	if (isset($row_eb['title'])) {
		$title = $row_eb['title'];
	}
	if (isset($row_eb['copy'])) {
		$copy = $row_eb['copy'];
	}
	if (isset($row_eb['photo'])) {
		$photo = $row_eb['photo'];
	}
	if (isset($row_eb['attachment'])) {
		$attachment = $row_eb['attachment'];
	}
	if (isset($row_eb['attachmentTitle'])) {
		if ($row_eb['attachmentTitle'] != "") {
			$attachmentTitle = $row_eb['attachmentTitle'];
		}
	}
	$summaryPId = $row_eb['presentationId'];
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">
#mainContainer .top .top-center .presentation-name {width:200px;}
#mainContainer .top .top-center .top-center-right {width:calc(100% - 360px);}

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'CasperBold'; line-height:30px;}
#mainContainer .mainContent .content-block .name {position:relative; display:block; margin:0 0 10px 20px;}
#mainContainer .mainContent .content-block .note {position:relative; display:block; margin:0 0 10px 20px; font-size:13px; }
#mainContainer .mainContent .content-block .input-required {display:block; position:absolute; top:5px; right:20px; color:#cfcfcf; font-size:12px;}
#mainContainer .mainContent .content-block .input-required img {position:relative; margin-right:8px;}

#mainContainer .mainContent .content-block .input-field {position:relative; width:auto; height:40px;}
#mainContainer .mainContent .content-block .input-field .inputslot {display:block; width:calc(100% - 40px);; height:38px; line-height:38xp; color:#77cdd7; background:none; border:1px solid #77cdd7; padding:0 20px;}

#mainContainer .mainContent .content-block .option {font-family:'Casper'; color:#565656; font-size:13px; line-height:25px;}
#mainContainer .mainContent .content-block .option .text-bold {font-family:'CasperBold'; text-transform:uppercase;}
#mainContainer .mainContent .content-block .option img {position:relative; display:inline-block; vertical-align:middle; margin:0 5px 0 20px;}

#mainContainer .mainContent .content-block .half-block {position:relative; display:inline-block; vertical-align:top; width:470px; height:auto; margin-top:10px;}
#mainContainer .mainContent .content-block .left-block a {position:relative; display:block; width:440px; height:100px;  padding-top:35px; margin-bottom:5px; text-align:center; font-family: 'ProximaNova-Bold'; font-size:13px; line-height:14px; text-transform:uppercase; text-decoration:none;}

#mainContainer .mainContent .content-block .left-block a span {position:relative; display:block; width:200px; margin:0 auto;}
#mainContainer .mainContent .content-block .right-block {margin-left:20px;}
#mainContainer .mainContent .content-block .background-image {position:relative; display:inline-block; visibility:hidden; vertical-align:top; width:484px; height:270px; padding:0px; text-align:center; background-position:center; background-size:cover;}
/*#mainContainer .mainContent .content-block .background-image img {display:inline-block; position:relative; margin:0; vertical-align:middle; max-width:484px; height:270px;}*/

#mainContainer .mainContent .content-block .background-image .media-title {display:block; position:absolute; width:444px; height:30px; bottom:0px; background-color: rgba(0, 0, 0, 0.5); text-align:left; padding:15px 20px; color:#fff;}
#mainContainer .mainContent .content-block .background-image .media-title span {display:inline-block; position:relative; width:400px; margin-right:10px; white-space:nowrap; text-overflow:ellipsis; overflow:hidden;}
#mainContainer .mainContent .content-block .background-image .media-title a {position:relative; display:inline-block; vertical-align:top; width:auto; color:#fff; background:none; background-color:#fff; height:28px; line-height:26px; border:none; border-radius:5px; text-align:center; padding:0 12px;}
.btn-large {height:170px !important; padding-top:100px !important;}
.left-block a img {margin-bottom:10px;}

.media-btn-normal {background-color:#f2f2f2; color:#444444;}
.media-btn-over {background-color:#77ced8; color:#fff;}

#mainContainer .mainContent .content-row {height:45px;}
#mainContainer .mainContent .content-row span {position:relative; display:inline-block; height:50px; vertical-align:top; line-height:45px;}
#mainContainer .mainContent .content-row .title {width:calc(100% - 470px); padding-left:10px; white-space:nowrap; text-overflow:ellipsis; overflow:hidden; margin-right:45px;}

#mainContainer .mainContent .file p {white-space:nowrap; text-overflow:ellipsis; overflow:hidden;}
#mainContainer .mainContent .content-block .filename {width:345px; }
#mainContainer .mainContent .content-block .type {width:80px; margin-left:20px; }
#mainContainer .mainContent .content-block .delete {width:50px; }
#mainContainer .mainContent .content-block img {vertical-align:middle;}

#mainContainer .mainContent #myfile .title {line-height:45px;}
#mainContainer .mainContent #myfile .title #filetitle {position:relative; width:400px; height:30px; height:28px; line-height:38xp; color:#77cdd7; background:none; border:1px solid #77cdd7; padding:0 20px;}

#mainContainer .mainContent .title-block .title {padding-left:75px; width:calc(100% - 535px);}
#mainContainer .mainContent .title-block {font-family:'ProximaNova-Bold'; text-transform:uppercase; font-size:13px; color:#494949; border-bottom:1px solid #f2f2f2; padding-left:10px;}
#mainContainer .mainContent .title-block .icon {margin-left:5px;}
#mainContainer .mainContent .item-block {overflow:hidden; border-bottom:1px solid #f2f2f2; padding-left:20px; font-family: 'Casper'; font-size:13px;}
#mainContainer .mainContent .item-block img {margin-right:15px;}
#mainContainer .mainContent .item-block .line-break-grey {margin-top:20px;}
#mainContainer .mainContent .item-block .buttons {margin-left:25px;}
#mainContainer .mainContent .item-block .buttons a {position:relative; display:inline-block; vertical-align:top; width:auto; color:#fff; background:none; background-color:#575757; height:36px; line-height:36px; border:none; border-radius:5px; font-size:13px; text-align:center; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none; padding:0 26px; margin:3px;}
#mainContainer .mainContent .item-block .buttons .btn-img-only img {margin-right:0;}
#mainContainer .mainContent .item-block .buttons .btn-img-only {padding:0 15px;}

#mainContainer .mainContent .btn-add {position:relative; display:block; width:440px; height:100px;  padding-top:35px;margin-bottom:5px; text-align:center; font-family: 'ProximaNova-Bold'; font-size:13px; line-height:14px; text-transform:uppercase; text-decoration:none;}
#mainContainer .mainContent .btn-add img {position:relative; display:inline-block; vertical-align:middle; margin-bottom:10px;}
#mainContainer .mainContent .btn-add span {display:block;}

#mainContainer .mainContent .btnUploadImage {position:absolute;display:block; left:0px; top:0px; width:470px; height:135px; background:rgba(0, 0, 0, 0) url("images/blank.png") no-repeat scroll 0 0; cursor:pointer;}
#mainContainer .mainContent .btnUploadImage .uploadImage {position: absolute;opacity:0;width:470px; height:135px;left: 0;top: 0;}
#mainContainer .mainContent .add-file {position:relative; display:block;}
#mainContainer .mainContent .btnUploadFile {position:absolute;display:block; left:0px; top:0px; width:100%; height:135px; background:rgba(0, 0, 0, 0) url("images/blank.png") no-repeat scroll 0 0; cursor:pointer;}
#mainContainer .mainContent .btnUploadFile .uploadFile {position:absolute;opacity:0;width:100%; height:135px;left: 0;top: 0;}

.file p {display:inline-block; vertical-align:middle;}
.file-show {display:block;}
.file-hidden {display:none !important;}

.mce-menu-item .mce-text {font-family:"Helvetica Neue",Helvetica,Arial,sans-serif !important;}
</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
<script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
var pid = <?php echo $pid; ?>;
var sid = <?php echo $sid; ?>;
var title, copy, attachmentTitle;
var title0 = "<?php echo $title; ?>";
var copy0 = "<?php echo $copy; ?>";
var photo0 = "<?php echo $photo; ?>";
var attachment0 = "<?php echo $attachment; ?>";
var attachmentTitle0 = "<?php echo $attachmentTitle; ?>";
var summaryPId = "<?php echo $summaryPId; ?>";
var actionPostSave = "";
var editType = "copy";
function savePage(sf) {
	actionPostSave = sf;
	editType = "copy";
	saveSummary();
}
function getAttachmentTitle(s) {
	var r = "";
	if (s) {
		if (s != "undefined" && s != "No Title") {
			r = unescape(s);
		}
	}
	return r;
}
function exitPage() {
	title = jQuery('#input-title').val();
	attachmentTitle = jQuery('#filetitle').val();
    jQuery("#thecopy").val(tinyMCE.activeEditor.getContent());
	copy = jQuery('#thecopy').val();

	//alert(unescape(title) +"/"+ unescape(title0) +"/"+ getAttachmentTitle(attachmentTitle) +"/"+ getAttachmentTitle(attachmentTitle0) +"/"+ unescape(copy) +"/"+ unescape(copy0));
	if (unescape(title) == unescape(title0) && getAttachmentTitle(attachmentTitle) == getAttachmentTitle(attachmentTitle0) && unescape(copy) == unescape(copy0)) {
		window.close();
	} else {
		openPopExit();
	}
	return false;
}
function onMouseClickSave() {
	editType = "copy";
	actionPostSave = "";
	saveSummary();
}
function onMouseClickClose() {
	window.close();
}
function saveSummary() {
	//alert("saveSummary()");
	title = jQuery('#input-title').val();
	attachmentTitle = jQuery('#filetitle').val();
    jQuery("#thecopy").val(tinyMCE.activeEditor.getContent());
	copy = jQuery('#thecopy').val();
	if (title.length < 2 || copy.length < 2) {
		alertMsg("Please fill in the page title and copy first.");
	} else {
		//if (confirm('Are you sure to update the copy?')) {
			//jQuery("#form-page").submit();
		//}
		if (unescape(title) != unescape(title0) || getAttachmentTitle(attachmentTitle) != getAttachmentTitle(attachmentTitle0) || unescape(copy) != unescape(copy0)) {
			var info = {};
			info.pid = pid;
			info.sid = sid;
			info.title = title;
			info.copy = copy;
			info.photo = photo0;
			info.attachment = attachment0;
			info.attachmentTitle = attachmentTitle;
			jQuery.post("submitPage.php", info, function(data) {
				var r = data + "";
				if (r == "success") {
					onSummarySaved();
				}
			});
		} else {
			onSummarySaved();
		}
	}
}
function onMouseClickDeletePhoto() {
	confirmAction("Are you sure you want to remove this photo from your customized page?", "delete-my-photo");
	return false;
}
function onMouseClickDeleteFile() {
	confirmAction("Are you sure you want to remove this attached file from your customized page?", "delete-my-file");
	return false;
}
function onConfirmAction(s) {
	if (s == "delete-my-file") {
		var info = {};
		info.pid = pid;
		info.sid = sid;
		info.photo = "removeAttachment";
		jQuery.post("submitPage.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				attachment0 = attachmentTitle0 = "";
				jQuery("#myfile, #myfile-title").html("");
				jQuery("#title-attachment").html("Upload Attachment");
			}
		});
	} else if (s == "delete-my-photo") {
		var info = {};
		info.pid = pid;
		info.sid = sid;
		info.photo = "removePhoto";
		jQuery.post("submitPage.php", info, function(data) {
			var r = data + "";
			if (r == "success") {
				photo0 = "";
				jQuery("#background-image").css({'display':'none'});
				jQuery("#title-image").html("Upload Image");
			}
		});
	}
	return false;
}
function onSummarySaved() {
	if (editType == "uploadImage" || editType == "uploadAttachment") {						
		jQuery('#editType').val(editType);
		//alert(jQuery('#form-page').serialize());
		//jQuery('#form-page').serialize();
		jQuery('#form-page').submit();
	} else {
		if (actionPostSave == "exit") {
			window.close();
		} else {
			title0 = escape(unescape(title));
			copy0 = escape(unescape(copy));
			attachmentTitle0 = escape(unescape(attachmentTitle));
			flashMsg("Your page information has been saved.");
		};
	}
}
function initIt() {
	jQuery("#btnNext").bind('click', onMouseClickSave);
	jQuery("#btnClose").bind('click', onMouseClickClose);
	jQuery("#background-image .btn-delete").bind('click', onMouseClickDeletePhoto);
	jQuery("#myfile .btnDelete").bind('click', onMouseClickDeleteFile);
	
	if (photo0 != "") {
		jQuery("#background-image").css({'background-image':"url('../../upload/"+photo0+"')"});
		jQuery("#background-image").css({'visibility':'visible'});
		jQuery("#title-image").html("Replace Image");
	}
	if (attachment0 != "") {
		jQuery("#title-attachment").html("Replace Attachment");
	}

	$("input:file").css({'opacity':0});
	$("#uploadedImage").change(function (){
		var filename = jQuery(this).val().toLowerCase();
		if (filename.indexOf('.jpg')>=1 || filename.indexOf('.jpeg')>=1 || filename.indexOf('.png')>=1) {
			if (this.files[0].size > 1000000) {
				alert("File size has to be under 1M");
			} else {
				editType = "uploadImage";
				saveSummary();
			}
		} else {
			alertMsg("Only .jpg and .png file can be used.");
		}
     });
	$("#uploadedFile").change(function (){
		var filename = jQuery(this).val().toLowerCase();
		if (filename.indexOf('.zip')>=1 || filename.indexOf('.pdf')>=1 || filename.indexOf('.doc')>=1 || filename.indexOf('.docx')>=1 || filename.indexOf('.docx')>=1 || filename.indexOf('.xls')>=1 || filename.indexOf('.xlsx')>=1 || filename.indexOf('.csv')>=1) {
			if (this.files[0].size > 5000000) {
				alertMsg("File size has to be under 5M");
			} else {
				editType = "uploadAttachment";
				saveSummary();
			}
		} else {
			alertMsg("Only .zip, .pdf, .doc/docx, xls/xlsx/csv file can be uploaded.");
		}
     });
};
</script>
<script>
    /** TEXT EDITOR */
    function strip_tags (str, allowed_tags)
    {
        var key = '', allowed = false;
        var matches = [];    var allowed_array = [];
        var allowed_tag = '';
        var i = 0;
        var k = '';
        var html = '';
        var replacer = function (search, replace, str) {
            return str.split(search).join(replace);
        };
        // Build allowes tags associative array
        if (allowed_tags) {
            allowed_array = allowed_tags.match(/([a-zA-Z0-9]+)/gi);
        }
        str += '';

        // Match tags
        matches = str.match(/(<\/?[\S][^>]*>)/gi);
        // Go through all HTML tags
        for (key in matches) {
            if (isNaN(key)) {
                // IE7 Hack
                continue;
            }

            // Save HTML tag
            html = matches[key].toString();
            // Is tag not in allowed list? Remove from str!
            allowed = false;

            // Go through all allowed tags
            for (k in allowed_array) {            // Init
                allowed_tag = allowed_array[k];
                i = -1;

                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+'>');}
                if (i != 0) { i = html.toLowerCase().indexOf('<'+allowed_tag+' ');}
                if (i != 0) { i = html.toLowerCase().indexOf('</'+allowed_tag);}

                // Determine
                if (i == 0) {                allowed = true;
                    break;
                }
            }
            if (!allowed) {
                str = replacer(html, "", str); // Custom replace. No regexing
            }
        }
        return str;
    }

    tinymce.init({
        selector:'textarea',
        height: 300,
        toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
        menubar: false,
        plugins : ["paste code link lists advlist"],
        paste_text_sticky : true,
        paste_auto_cleanup_on_paste : true,
        paste_preprocess : function(pl, o) {
            o.content = strip_tags( o.content,'' );
        }
    });
</script>
</head>
<body>
<div id="mainContainer">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"><?php echo $presentationTitle; ?></div>
			<div class="top-center-right">
				<a href="#" onClick="return savePage('');" class="btn btn-blue" id="btnSave">Save</a>
				<a href="#" onClick="return exitPage();" class="btn btn-grey" id="btnExit">Exit</a>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Page Content</div>
		<div class="line-break-grey"></div>
		<div class="content-block">
			<span class="name">Page Title</span>
			<div class="input-required"><img src="images/icon-required.png" />Required</div>
			<div class="input-field">
				<input id="input-title" class="inputslot" type="text" placeholder="Page Title" autocomplete="on" <?php if ($title != "") {?>value="<?php echo rawurldecode($title); ?>"<?php } ?> />
			</div>
		</div>
		<div class="line-break-grey"></div>
		<div class="content-block">
			<span class="name">Page Copy</span>
			<div class="editor-tf"><textarea name='thecopy' id='thecopy' class='inputcopy'><?php echo rawurldecode($copy); ?></textarea></div>
		</div>
		<div class="line-break-grey"></div>
		<form enctype="multipart/form-data" id="form-page" name="form-page" method="post" action="">
			<input type="hidden" name="pid" id="pid" value="<?php echo $pid; ?>">
			<input type="hidden" name="sid" id="sid" value="<?php echo $sid; ?>">
			<input type="hidden" name="editType" id="editType" value="">
			<div class="content-block">
				<span class="name" id="title-image">Upload Image</span>
				<div class="half-block left-block">
					<a class="upload-file media-btn media-btn-normal" href="#"><img class="media-btn-icon" src="images/icon-upload-image.png" /><span>Upload from <br />Your Computer</span></a>
					<div class="btnUploadImage btn-upload upload-normal"><input type="file" id="uploadedImage" class="uploadImage" name="uploadedImage"></div>
				</div>
				<?php if ($photo != "") { ?>
				<div id="background-image" class="half-block right-block background-image">
					<div class="media-title">
						<span></span><a href="#" class="btn-img-only btn-delete"><img src="images/icon-delete-presentation.png" /></a>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="line-break-grey"></div>
			<div class="content-block">
				<span class="name" id="title-attachment">Upload Attachment</span>
			</div>
			<?php if ($attachment != "") { ?>
			<div class="content-block content-row title-block" id="myfile-title">
				<span class="title">Title</span>
				<span class="filename">File Name</span>
				<span class="delete"></span>
			</div>
			<div class="content-block content-row item-block file" id="myfile" >
				<p class="title"><img class="icon-tree" src="images/icon-tree.png" /><img class="icon-link" src="images/icon-link.png" /><span><input id="filetitle" class="inputslot" type="text" placeholder="Attachment File Title" autocomplete="on" <?php if ($attachmentTitle != "" && $attachmentTitle != "No Title") {?>value="<?php echo rawurldecode($attachmentTitle); ?>"<?php } ?> /></span></p>
				<p class="filename"><?php echo $attachment; ?></p>
				<p class="delete"><a href="#" class="btnDelete"><img src="images/icon-tree-delete.png" /></a></p>
			</div>
			<?php } ?>
			<div class="content-block docs-control">
				<div class="half-block left-block">
					<div class="btn-add upload-file media-btn media-btn-normal"><img class="media-btn-icon" src="images/icon-add-file.png" /><span>Upload an Attachment<br />Word, Excel, PDF or Zip</span></div>
					<div class="btnUploadFile btn-upload"><input type="file" id="uploadedFile" class="uploadFile" name="uploadedFile"></div>
				</div>
			</div>
		</form>
	</div>
	<div id="pop-exit" class="pop-up">
		<div class="pop-up-box">
			<p><span class="pop-up-message">Are you sure you want to exit without saving?</p>
			<a href="#" class="btn-cancel"  onClick="return window.close();">Exit without saving</a>
			<a href="#" class="btn-ok" onClick="return savePage('exit');">Save & Exit</a>
			<a class="btn-cancel" href="#" onClick="return closePopExit();">Cancel</a>
		</div>
	</div>
	<div id="pop-action" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message">Title of the file you are uploading:</span>
			<input name='thetitle' id='thetitle' class='myinput' type='text' value="" />
			<a class="btn-cancel" href="#" onClick="return closePopAction();">Cancel</a>
			<a class="btn-ok" href="#" onClick="return submitUpload();">OK</a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>