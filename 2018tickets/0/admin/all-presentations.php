<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
} else {
	header('Location: index.php');
	exit;
}
$isAdmin = false;
if (isset($_SESSION['isAdmin'])) {
	$isAdmin = $_SESSION['isAdmin'];
}
function getListTime($s) {
	if (strlen($s)>10) {
		$t = mktime(substr($s, 8, 2), substr($s, 10, 2), substr($s, 12, 2), substr($s, 4, 2), substr($s, 6, 2), substr($s, 0, 4));
		return date("M d, Y H:i", $t);
	} else {
		return "";
	}
}
function getListTimeShort($s) {
	if (strlen($s)>10) {
		$t = mktime(substr($s, 8, 2), substr($s, 10, 2), substr($s, 12, 2), substr($s, 4, 2), substr($s, 6, 2), substr($s, 0, 4));
		return date("M d, Y", $t);
	} else {
		return "";
	}
}
function getRepName($r) {
	$ar = explode(" ", $r);
	if (count($ar) == 1) {
		return $ar[0];
	} else {
		return $ar[0] . " " . substr($ar[1], 0, 1) . ".";
	}
}
function removeFirstSpace($s) {
	for ($i=0; $i<5; $i++) {
		if (substr($s, 0, 1) == " ") {
			$s = substr($s, 1);
		} else {
			break;
		}
	}
	return $s;
}
function getPercentString($n) {
	$r = getIntString($n * 100);
	if (strlen($r) == 1) {
		$r = "00" . $r;
	} elseif (strlen($r) == 2) {
		$r = "0" . $r;
	}
	$r = substr($r, 0, strlen($r)-2) . "." . substr($r, strlen($r)-2);
	return $r . "%";
}


$multipleMaster = false;
if (isset($_multipleMaster)) {
	$multipleMaster = $_multipleMaster;
}
$listNoValue = false;
if (isset($_listNoValue)) {
	$listNoValue = $_listNoValue;
}
$listNoStatus = false;
if (isset($_listNoStatus)) {
	$listNoStatus = $_listNoStatus;
}
$listNoClient = false;
if (isset($_listNoClient)) {
	$listNoClient = $_listNoClient;
}
$listLastSaved = false;
if (isset($_listLastSaved)) {
	$listLastSaved = $_listLastSaved;
}
if (isset($_arrType)) {
	$_arrGroup = $_arrType;
}
$mykey = "lastSavedTime";
$mydirection = 1;
$myorder = "DESC";
if (isset($_GET['s'])) {
	$mykey = $_GET['s'];
}
if (isset($_GET['d'])) {
	if ($_GET['d'] == 0) {
		$mydirection = 0;
		$myorder = "ASC";
	}
}
function getSortDirection($s) {
	global $mykey, $mydirection;
	$d = 0;
	if ($s == $mykey) {
		$d = 1-$mydirection;
	}
	return "s=" . $s . "&d=" . $d;
}
function getArrow($s) {
	global $mykey, $mydirection;
	$d = 0;
	$extra = "";
	if ($s == $mykey) {
		if ($mydirection == 1) {
			return "down-blue";
		} else {
			return "up-blue";
		}
	} else {
		return "up-grey";
	}
}
mysql_select_db($database_channel1media, $channel1media);

$query_eb = "SELECT * FROM " . $_dbname . "_reps";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrReps = array();
if ($totalRows_eb > 0) {
	do {
		$arrReps[$row_eb['id']] = getRepName($row_eb['rep']); 
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$myValue = array(0, 0, 0, 0, 0);
$myClosed = "";
$otherValue = array(0, 0, 0, 0, 0);
$otherClosed = "";
$myPendings = $otherPendings = 0;

if ($mykey == 'rep') {
	$mykey2 = $_dbname . "_reps.`$mykey`";
} elseif ($mykey == "client") {
	$mykey2 = "`firstname` $myorder, `lastname`";
} else {
	$mykey2 = $_dbname . "_presentations.`$mykey`";
}

if ($repId == 1 || $repId > 10) {
	$query_eb = "SELECT *, " . $_dbname . "_presentations.`id` AS myid FROM " . $_dbname . "_presentations, " . $_dbname . "_reps WHERE `repId`=" . $_dbname . "_reps.`id` AND (`repId`<>$repId AND `repId`>10) AND " . $_dbname . "_presentations.`active`=1 ORDER BY $mykey2 $myorder";
} else {
	$query_eb = "SELECT *, " . $_dbname . "_presentations.`id` AS myid FROM " . $_dbname . "_presentations, " . $_dbname . "_reps WHERE `repId`=" . $_dbname . "_reps.`id` AND (`repId`<>$repId AND `repId`>1) AND " . $_dbname . "_presentations.`active`=1 ORDER BY $mykey2 $myorder";
}
//echo $query_eb;
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrOthers = array();
$i = 0;
if ($totalRows_eb > 0) {
	do {
		$arrOthers[$i] = $row_eb;
		$arrOthers[$i]['client'] = trim(rawurldecode($arrOthers[$i]['firstname'])) . " " . trim(rawurldecode($arrOthers[$i]['lastname']));
		$i++;
	} while ($row_eb = mysql_fetch_assoc($eb));
}

if ($multipleMaster && $_SESSION['rep'] != "Admin") {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `repId`=1 AND `active`=1 ORDER BY `id` ASC";	
	$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
	$row_eb = mysql_fetch_assoc($eb);
	$totalRows_eb = mysql_num_rows($eb);
	$arrAdmin = array();
	$i = 0;
	if ($totalRows_eb > 0) {
		do {
			$arrAdmin[$i] = $row_eb;
			$i++;
		} while ($row_eb = mysql_fetch_assoc($eb));
	}
}
$mykey2 = $mykey;
$myorder2 = $myorder;
if ($mykey == "rep") {
	$mykey2 = "lastSavedTime";
	$myorder2 = "DESC";
} elseif ($mykey == "client") {
	$mykey2 = "`firstname` $myorder2, `lastname`";
}
if ($_SESSION['rep'] == "Admin") {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `repId`=$repId AND `active`=1 ORDER BY `$mykey2` $myorder2";
} else {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `repId`=$repId AND `active`=1 ORDER BY `$mykey2` $myorder2";
}
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrMine = array();
$i = 0;
if ($totalRows_eb > 0) {
	do {
		$arrMine[$i] = $row_eb;
		$arrMine[$i]['client'] = trim(rawurldecode($arrMine[$i]['firstname'])) . " " . trim(rawurldecode($arrMine[$i]['lastname']));
		$arrMine[$i]['rep'] = $arrReps[$arrMine[$i]['repId']];
		if (isset($_arrGroup)) {
			$arrMine[$i]['type'] = $_arrGroup[$arrMine[$i]['version']];
		}
		$i++;
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$arrStatus = array();
$arrStatus[0] = "In Progress";
$arrStatus[1] = "Closed";
$arrStatus[2] = "Dormant";
$arrStatus[3] = "Sent";
$arrStatus[4] = "Viewed!";
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'Casper'; font-size:16px; line-height:20px;}
#mainContainer .mainContent .content-block span {position:relative; display:inline-block; vertical-align:middle; }
#mainContainer .mainContent .content-block .name {width:calc(100% - 585px); padding-left:10px; white-space:nowrap; overflow:hidden; margin-right:20px;}
#mainContainer .mainContent .content-block .name-short {width:calc(100% - 750px); padding-left:10px; white-space:nowrap; overflow:hidden; margin-right:20px;}
#mainContainer .mainContent .content-block .rep {width:100px; }
#mainContainer .mainContent .content-block .rep-short {width:85px; white-space:nowrap; overflow:hidden; }
#mainContainer .mainContent .content-block .last-saved {width:150px; }
#mainContainer .mainContent .content-block .last-saved-short {width:100px; }
#mainContainer .mainContent .content-block .last-hit {width:150px; }
#mainContainer .mainContent .content-block .last-hit-short {width:100px; }
#mainContainer .mainContent .content-block .hits {width:46px; }
#mainContainer .mainContent .content-block .value {width:90px; }
#mainContainer .mainContent .content-block .status {width:120px; margin-right:10px;}
#mainContainer .mainContent .content-block .status img {margin-right:5px;}
#mainContainer .mainContent .content-block img {vertical-align:middle;}
#mainContainer .mainContent .content-block .line-break-grey {margin:10px 0px 0px 0px !important;}

#mainContainer .mainContent .title-block {font-family:'ProximaNova-Bold'; text-transform:uppercase; font-size:13px; color:#494949;}
#mainContainer .mainContent .title-block .icon {margin-left:5px;}
#mainContainer .mainContent .title-block a {color:#494949; text-decoration:none;}
#mainContainer .mainContent .title-block a img {display:inline-block; margin-left:0px; margin-bottom:3px;}
#mainContainer .mainContent .item-block {/*background-color:#f2f2f2;*/ overflow:hidden; height:44px; line-height:44px; border-bottom:1px solid #f2f2f2;}
#mainContainer .mainContent .item-block a {position:relative; display:inline-block; transition:background-color 0.3s; vertical-align:middle; width:auto; color:#fff; background:none; background-color:#575757; height:36px; line-height:36px; border:none; border-radius:5px; font-size:13px; text-align:center; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none; padding:0 24px; margin:3px;}
#mainContainer .mainContent .item-block a img {margin-right:10px; transition:filter 0.3s;}
#mainContainer .mainContent .item-block a:hover {background-color:#77cdd7;}
#mainContainer .mainContent .item-block a:hover img {-webkit-filter:brightness(200%); filter:brightness(200%);}
#mainContainer .mainContent .item-block .btn-img-only img {margin-right:0;}
#mainContainer .mainContent .item-block .btn-img-only {padding:0 15px;}

<?php if ($isAdmin) { ?>
	#mainContainer .mainContent .content-block {font-size:13px;}
	#mainContainer .mainContent .item-block a {font-size:12px; padding:0 16px;}
	#mainContainer .mainContent .item-block a img {margin-right:5px; transition:filter 0.3s;}
	
	#mainContainer .mainContent .content-block .name-short {width:calc(100% - 800px); margin-right:10px;}
	#mainContainer .mainContent .content-block .rep-short {width:75px; white-space:nowrap; overflow:hidden; }
	#mainContainer .mainContent .content-block .last-saved-short {width:90px; }
	#mainContainer .mainContent .content-block .last-hit-short {width:90px; }
	#mainContainer .mainContent .content-block .value {width:80px; }
	#mainContainer .mainContent .content-block .status {width:90px; margin-right:10px;}
<?php } ?>
</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
function initIt(){
	//jQuery(".item-block").bind('click', onMouseClickItem);
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body class="body-with-footer">
<div id="mainContainer" class="main-no-bottom">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"></div>
			<div class="top-center-right">
				<?php if ($repId > 1) { ?>
				<div class="mycontrol">
					<a href="#" class="myaccount" onClick="return onMouseClickMyControl();">
						<p class="team-logo"></p>
						<p class="myinfo"><?php echo $_SESSION['rep']; ?><span class="myteam"><?php echo $_team; ?></span></p>
						<img class="arrow-down" src="images/arrow-down.png" />
					</a>
					<a href="dashboard.php" class="control-btn">Dashboard</a>
					<a href="my-presentations.php" class="control-btn">My Custom Presentations</a>
					<a href="http://c1ms.com/ePitch/ePitchLiteManual2.0.pdf" target="_blank" class="control-btn">ePitch Manual</a>
					<?php if ($repId == 1) { ?>
					<a href="http://c1ms.com/ePitch/ePitchManualAdmin.pdf" target="_blank" class="control-btn">ePitch Manual - Admin</a>
					<?php } ?>
					<a href="index.php?a=logout" class="control-btn">Log Out</a>
				</div>
				<?php } else { ?>
				<a href="#" onClick="return window.close();" class="btn btn-grey" id="btnSave">Exit</a>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title text-black"><img src="images/icon-dashboard.png" />
		<?php if ($repId == 1) { ?>
		List of presentations
		<?php } else { ?>
		Select a presentation to replicate
		<?php } ?>
		</div>
		<?php if ($multipleMaster && $repId > 1) { ?>
		<p class="page-note text-blue">Replicate a master presentation</p>
		<div class="content-block title-block">
			<a><span class="name">Presentation</span></a>
			<span class="replicate"></span>
			<div class="line-break-grey"></div>
		</div>
		<?php for ($i=0; $i<count($arrAdmin); $i++) { ?>
		<div class="content-block item-block" id="p<?php echo $arrAdmin[$i]['myid']; ?>">
			<span class="name"><?php echo $arrAdmin[$i]['purl']; ?></span>
			<span class="preview"><a href="../../?preview.<?php echo $arrAdmin[$i]['purl']; ?>" target="_new" ><img src="images/icon-preview.png" />Preview</a></span>
			<span class="replicate"><a href="presentation.php?rid=<?php echo $arrAdmin[$i]['id']; ?>" ><img src="images/icon-replicate.png" />Replicate</a></span>
		</div>
		<?php } ?>		
		<br /><br />
		<?php } ?>
		<p class="page-note text-blue">
		<?php if ($repId == 1) { ?>
		Admin presentation
		<?php } else { ?>
		Replicate one of your own presentations
		<?php } ?>
		</p>
		<?php if (count($arrMine)>0) { ?>
		<div class="content-block title-block">
			<?php if ($repId == 1) { ?>
			<a ><span class="name">Presentation</span></a>
			<?php } else { ?>
			<a href="all-presentations.php?<?php echo getSortDirection('client'); ?>"><span class="name">Client<img src="images/arrow-sort-<?php echo getArrow('client'); ?>.png" /></span></a>
			<?php } ?>
			<span class="rep"></span>
			<a href="all-presentations.php?<?php echo getSortDirection('lastSavedTime'); ?>"><span class="last-saved">Last Saved<img src="images/arrow-sort-<?php echo getArrow('lastSavedTime'); ?>.png" /></span></a>
			<span class="replicate"></span>
			<div class="line-break-grey"></div>
		</div>
		<?php if (count($arrMine) > 0) {
			for ($i=0; $i<count($arrMine); $i++) { ?>
		<div class="content-block item-block" id="p<?php echo $arrMine[$i]['id']; ?>">
			<?php if ($repId == 1) { ?>
			<span class="name"><?php echo $arrMine[$i]['purl']; ?></span>
			<?php } else { ?>
			<span class="name"><?php echo $arrMine[$i]['client']; ?></span>
			<?php } ?>
			<span class="rep"></span>
			<span class="last-saved"><?php echo getListTime($arrMine[$i]['lastSavedTime']); ?></span>
			<span class="preview"><a href="../../?preview.<?php echo $arrMine[$i]['purl']; ?>" target="_new" ><img src="images/icon-preview.png" />Preview</a></span>
			<?php if ($repId != 1) { ?>
			<span class="replicate"><a href="presentation.php?rid=<?php echo $arrMine[$i]['id']; ?>" ><img src="images/icon-replicate.png" />Replicate</a></span>
			<?php } ?>
		</div>
		<?php }} ?>
		<br /><br />
		<?php } ?>
		<?php if (count($arrOthers) > 0) {
			if ($isAdmin) { ?>
		<p class="page-note text-blue">All reps presentations</p>
		<div class="content-block title-block">
			<a href="all-presentations.php?<?php echo getSortDirection('client'); ?>"><span class="name-short">Client<img src="images/arrow-sort-<?php echo getArrow('client'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('rep'); ?>"><span class="rep-short">Rep<img src="images/arrow-sort-<?php echo getArrow('rep'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('hits'); ?>"><span class="hits">Hits<img src="images/arrow-sort-<?php echo getArrow('hits'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('lasttime'); ?>"><span class="last-hit-short">Last Hit<img src="images/arrow-sort-<?php echo getArrow('lasttime'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('Value'); ?>"><span class="value">Value<img src="images/arrow-sort-<?php echo getArrow('Value'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('Status'); ?>"><span class="status">Status<img src="images/arrow-sort-<?php echo getArrow('Status'); ?>.png" /></span></a>
			<div class="line-break-grey"></div>
		</div>
		<?php for ($i=0; $i<count($arrOthers); $i++) {
				$mystatus = $arrStatus[$arrOthers[$i]['Status']];
				?>
		<div class="content-block item-block" id="p<?php echo $arrOthers[$i]['myid']; ?>">
			<span class="name-short"><?php echo $arrOthers[$i]['client']; ?></span>
			<span class="rep-short"><?php echo getRepName($arrOthers[$i]['rep']); ?></span>
			<span class="hits"><?php echo $arrOthers[$i]['hits']; ?></span>
			<span class="last-hit-short"><?php echo getListTimeShort($arrOthers[$i]['lasttime']); ?></span>
			<span class="value"><?php echo getMoneyStringInt($arrOthers[$i]['Value']); ?></span>
			<span class="status"><img class="status-indicator" src="images/status-<?php echo str_replace(" ", "-", str_replace("!", "", strtolower($mystatus))); ?>.png" /><?php echo $mystatus; ?></span>
			<span class="preview"><a href="../../?preview.<?php echo $arrOthers[$i]['purl']; ?>" target="_new" ><img src="images/icon-preview.png" />Preview</a></span>
			<span class="analytics"><a target="_blank" href="analytics.php?id=<?php echo $arrOthers[$i]['myid']; ?>"><img src="images/icon-view-analytics.png">Analytics</a></span>
			<?php if ($repId != 1) { ?>
			<span class="replicate"><a href="presentation.php?rid=<?php echo $arrOthers[$i]['myid']; ?>" ><img src="images/icon-replicate.png" />Replicate</a></span>
			<?php } ?>
		</div>
		<?php } ?>
		<?php } else { ?>
		<p class="page-note text-blue">Replicate another rep's presentation</p>
		<div class="content-block title-block">
			<a href="all-presentations.php?<?php echo getSortDirection('client'); ?>"><span class="name">Client<img src="images/arrow-sort-<?php echo getArrow('client'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('rep'); ?>"><span class="rep">Rep<img src="images/arrow-sort-<?php echo getArrow('rep'); ?>.png" /></span></a>
			<a href="all-presentations.php?<?php echo getSortDirection('lastSavedTime'); ?>"><span class="last-saved">Last Saved<img src="images/arrow-sort-<?php echo getArrow('lastSavedTime'); ?>.png" /></span></a>
			<span class="replicate"></span>
			<div class="line-break-grey"></div>
		</div>
		<?php for ($i=0; $i<count($arrOthers); $i++) { ?>
		<div class="content-block item-block" id="p<?php echo $arrOthers[$i]['myid']; ?>">
			<span class="name"><?php echo $arrOthers[$i]['client']; ?></span>
			<span class="rep"><?php echo getRepName($arrOthers[$i]['rep']); ?></span>
			<span class="last-saved"><?php echo getListTime($arrOthers[$i]['lastSavedTime']); ?></span>
			<span class="preview"><a href="../../?preview.<?php echo $arrOthers[$i]['purl']; ?>" target="_new" ><img src="images/icon-preview.png" />Preview</a></span>
			<span class="replicate"><a href="presentation.php?rid=<?php echo $arrOthers[$i]['myid']; ?>" ><img src="images/icon-replicate.png" />Replicate</a></span>
		</div>
		<?php }}} ?>
	</div>

	<div class="footer">
		<p class="footer-copy">&copy; Copyright 2015 &bull; Channel 1 Media Solutions &bull; All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &bull; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.channel1media.com" target="_blank">www.channel1media.com</a></p>
		<p class="footer-logo"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c.png" /></a></p>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>