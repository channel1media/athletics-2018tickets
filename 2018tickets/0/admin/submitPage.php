<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();

mysql_select_db($database_channel1media, $channel1media);
$result = "failed";
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
	$pid = $_POST['pid'];
	$sid = $_POST['sid'];
	$photo = rawurldecode($_POST['photo']);
	if ($photo == "removePhoto") {
		$updateSQL = "UPDATE " . $_dbname . "_summary SET `photo`=NULL WHERE `presentationId`=$pid AND `sectionId`=$sid";
		$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
	} elseif ($photo == "removeAttachment") {
		$updateSQL = "UPDATE " . $_dbname . "_summary SET `attachment`=NULL WHERE `presentationId`=$pid AND `sectionId`=$sid";
		$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
	} else {
		$title = rawurlencode(rawurldecode($_POST['title']));
		$copy = rawurlencode(rawurldecode($_POST['copy']));
		$attachment = rawurldecode($_POST['attachment']);
		$attachmentTitle = "";
		if (isset($_POST['attachmentTitle'])) {
			$attachmentTitle = rawurlencode(rawurldecode($_POST['attachmentTitle']));
		}
		$query_eb = "SELECT * FROM " . $_dbname . "_summary WHERE `presentationId`=$pid AND `sectionId`=$sid LIMIT 1";
		$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
		$row_eb = mysql_fetch_assoc($eb);
		$totalRows_eb = mysql_num_rows($eb);
		if ($totalRows_eb == 0) {
			$insertSQL = sprintf("INSERT INTO " . $_dbname . "_summary (`presentationId`, `sectionId`, `repId`, `title`, `copy`, `photo`, `attachment`, `attachmentTitle`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
				   GetSQLValueString($pid, "int"),
				   GetSQLValueString($sid, "int"),
				   GetSQLValueString($repId, "int"),
				   GetSQLValueString($title, "text"),
				   GetSQLValueString($copy, "text"),
				   GetSQLValueString($photo, "text"),
				   GetSQLValueString($attachment, "text"),
				   GetSQLValueString($attachmentTitle, "text"));
			$insertHits = mysql_query($insertSQL, $channel1media) or die(mysql_error());
		} else {
			$updateSQL = "UPDATE " . $_dbname . "_summary SET `title`='$title', `attachmentTitle`='$attachmentTitle', `copy`='$copy' WHERE `presentationId`=$pid AND `sectionId`=$sid";
			$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
		}
	}
	$result = "success";
}
echo "$result";
?>