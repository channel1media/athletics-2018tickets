<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
} else {
	header('Location: index.php');
	exit;
}
if (isset($_disablePrivate)) {
} else {
	$_disablePrivate = false;
}
mysql_select_db($database_channel1media, $channel1media);

$purl = "";
$id = 0;
$presentationTitle = "";
$backUrl = "dashboard.php";
if (isset($_GET['id'])) {
	$id = $_GET['id'];	
	$backUrl = "my-presentations.php";
}
$rid = 0;
if (isset($_GET['rid'])) {
	$rid = $_GET['rid'];	
	$backUrl = "all-presentations.php";
}
if ($id == 0) {
	$row_eb = array();
	$presentationValue = "";
	$row_eb['firstname'] = "";
	$row_eb['lastname'] = "";
	$row_eb['email'] = "";
} else {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$id";
	$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
	$row_eb = mysql_fetch_assoc($eb);
	if ($row_eb['repId'] != $repId) {
		header('Location: index.php');
		exit;
	} else {
		//$isPublic = $row_eb['visibility'];
		$presentationTitle = "Client: " . trim(rawurldecode($row_eb['firstname'] . " " . $row_eb['lastname']));
		//if (isset($row_eb['title'])) {
			$presentationValue = $row_eb['Value'];
		//}
		$purl = $row_eb['purl'];	
	}
	$presentationValue = getMoneyStringInt($presentationValue);
}
if ($presentationValue == "$0") {
	$presentationValue = "";
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'CasperBold'; line-height:30px; border-top:1px solid #f2f2f2; margin-top:15px; margin-bottom:20px; padding-top:5px;}
#mainContainer .mainContent .content-block .name {position:relative; display:block; margin:0 0 10px 20px; }
#mainContainer .mainContent .content-block .input-required {display:block; position:absolute; top:5px; right:20px; color:#cfcfcf; font-size:12px;}
#mainContainer .mainContent .content-block .input-required img {position:relative; margin-right:8px;}

#mainContainer .mainContent .content-block .input-field {position:relative; width:auto; height:40px;}
#mainContainer .mainContent .content-block .input-field .inputslot {display:block; width:calc(100% - 40px);; height:38px; line-height:38xp; color:#77cdd7; background:none; border:1px solid #77cdd7; padding:0 20px;}

#mainContainer .mainContent .content-block .option {font-family:'Casper'; color:#565656; font-size:13px; line-height:25px;}
#mainContainer .mainContent .content-block .option .text-bold {font-family:'CasperBold'; text-transform:uppercase;}
#mainContainer .mainContent .content-block .option img {position:relative; display:inline-block; vertical-align:middle; margin:0 5px 0 20px;}

</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
var rid = <?php echo $rid; ?>;
var id = <?php echo $id; ?>;
var presentationValue = "<?php echo $presentationValue; ?>";

var strDigits = "01234567890.";
var arrDigits = strDigits.split("");
function isDigit(n) {
	var r = false;
	for (var i=0; i<arrDigits.length; i++) {
		if (n == arrDigits[i]) {
			r = true;
			break;
		}
	}
	return r;
}
function getNumberValue(s) {
	var r = "";
	if (s.length > 0) {
		for (var i=0; i<s.length; i++) {
			var c = s.substr(i, 1);
			if (isDigit(c)) {
				r += c;
			}
		}
	}
	return r;
}
function onMouseClickNext(event) {
	var info = {};
	info.firstname = jQuery("#input-firstname").val();
	info.lastname = jQuery("#input-lastname").val();
	info.email = jQuery("#input-email").val();
	info.presentationValue = getNumberValue(jQuery("#input-value").val());
	info.id = id;
	info.rid = rid;
	jQuery.post("submitPresentation.php", info, function(data) {
		var r = data + "";
		if (r.substr(0, 7) == "success") {
			id = r.substr(8);
			window.location = "tree.php?id="+id;
		} else {
			alertMsg("server-error");
		}
	});
	return false;
}
function initIt() {
	if (presentationValue != "") {
		jQuery("#input-value").val(unescape(presentationValue));
	}
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body>
<div id="mainContainer">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"><?php echo $presentationTitle; ?></div>
			<div class="top-center-right">
				<div class="mycontrol">
					<a href="#" class="myaccount" onClick="return onMouseClickMyControl();">
						<p class="team-logo"></p>
						<p class="myinfo"><?php echo $_SESSION['rep']; ?><span class="myteam"><?php echo $_team; ?></span></p>
						<img class="arrow-down" src="images/arrow-down.png" />
					</a>
					<a href="dashboard.php" class="control-btn">Dashboard</a>
					<a href="my-presentations.php?id=<?php echo $id; ?>" class="control-btn">My Custom Presentations</a>
					<a href="http://c1ms.com/ePitch/ePitchLiteManual2.0.pdf" target="_blank" class="control-btn">ePitch Manual</a>
					<?php if ($repId == 1) { ?>
					<a href="http://c1ms.com/ePitch/ePitchManualAdmin.pdf" target="_blank" class="control-btn">ePitch Manual - Admin</a>
					<?php } ?>
					<a href="index.php?a=logout" class="control-btn">Log Out</a>
				</div>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Presentation Info</div>
		<div class="content-block half-block left-block">
			<span class="name">First Name</span>
			<div class="input-field">
				<input id="input-firstname" class="inputslot" type="text" placeholder="First Name" <?php if ($row_eb['firstname']!="") { ?>value="<?php echo rawurldecode($row_eb['firstname']); ?>"<?php } ?>autocomplete="on" />
			</div>
		</div>
		<div class="content-block half-block">
			<span class="name">Last Name</span>
			<div class="input-field">
				<input id="input-lastname" class="inputslot" type="text" placeholder="Last Name" <?php if ($row_eb['lastname']!="") { ?>value="<?php echo rawurldecode($row_eb['lastname']); ?>"<?php } ?>autocomplete="on" />
			</div>
		</div>
		<div class="content-block">
			<span class="name">Email Address</span>
			<div class="input-field">
				<input id="input-email" class="inputslot" type="text" placeholder="Email Address" <?php if ($row_eb['email']!="") { ?>value="<?php echo rawurldecode($row_eb['email']); ?>"<?php } ?>autocomplete="on" />
			</div>
		</div>
		<div class="content-block">
			<span class="name">Value of Proposal</span>
			<div class="input-field">
				<input id="input-value" class="inputslot" type="text" placeholder="$0" autocomplete="on" />
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="bottom-center">
			<a href="my-presentations.php" class="bottom-btn" id="btnBack"><img src="images/arrow-left-white.png" />Back</a>
			<div class="step-indicator">
				<img class="my-step" src="images/icon-step.png" />
				<img src="images/icon-step.png" />
				<img src="images/icon-step.png" />
				<img src="images/icon-step.png" />
			</div>
			<a href="#" onClick="javascript:onMouseClickNext();" class="bottom-btn" id="btnNext">Next<img src="images/arrow-right-white.png" /></a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>