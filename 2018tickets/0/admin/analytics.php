<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId']) && isset($_GET['id'])) {
	$repId = $_SESSION['repId'];
	$presentationId = $_GET['id'];
} else {
	header('Location: index.php');
	exit;
}
$levels = 2;
if (isset($_treeLevels)) {
	$levels = $_treeLevels;
}
function getTimeSpent($st, $et) {
	$t = $et - $st;
	if ($t<0 || $t>60) {
		$t = 15;
	}
	return $t;
}
function getFormatedTime($t) {
	$s = $t % 60;
	$m = ($t - $s) / 60;
	return dig2($m) . "m:" . dig2($s) . "s";
}
function getFormatedTime2($t) {
	$s = $t % 60;
	$m = ($t - $s) / 60;
	if ($m > 0) {
		if ($m == 0) {
			return $m . "m";
		} else {
			return $m . "m" . $s . "s";
		}
	} else {
		return $s . "s";
	}
}
function dig2($n) {
	if ($n < 10) {
		return "0" . $n;
	} else {
		return "" . $n;
	}
}
function getSystem($s) {
	$r = "Other (including Seach Engines)";
	$s = strtolower($s);
	if (strpos($s,'ipad')) {
		$r = "iPad";
	} elseif (strpos($s,'iphone')) {
		$r = "iPhone";
	} elseif (strpos($s,'blackberry')) {
		$r = "Blackberry";
	} elseif (strpos($s,'android')) {
		$r = "Android";
	} elseif (strpos($s,'windows phone')) {
		$r = "Windows Phone";
	} elseif (strpos($s,'linux')) {
		$r = "Linux";
	} elseif (strpos($s,'macintosh')) {
		$r = "Mac OS";
	} elseif (strpos($s,'windows')) {
		$r = "Windows";
		if (strpos($s,'msie 5')) {
			$r .= " + IE 5";
		} elseif (strpos($s,'msie 6')) {
			$r .= " + IE 6";
		} elseif (strpos($s,'msie 7')) {
			$r .= " + IE 7";
		} elseif (strpos($s,'msie 8')) {
			$r .= " + IE 8";
		} elseif (strpos($s,'msie 9')) {
			$r .= " + IE 9";
		} elseif (strpos($s,'msie 10')) {
			$r .= " + IE 10";
		} elseif (strpos($s,'msie 11')) {
			$r .= " + IE 11";
		} elseif (strpos($s,'chrome')) {
			$r .= " + Chrome";
		} elseif (strpos($s,'safari')) {
			$r .= " + Safari";
		} elseif (strpos($s,'mozilla')) {
			$r .= " + Firefox";
		}
	}
	return $r;
}

$na = true;

mysql_select_db($database_channel1media, $channel1media);

$query_eb = "SELECT * FROM " . $_dbname . "_sections WHERE `active`=1 ORDER BY `parentId` ASC";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$amountSection = mysql_num_rows($eb);
$arrSection = array();
$arrSectionParent = array();
$arrSectionChildren = array();
$arrSectionChildren[0] = array();
$arrSectionName = array();
if ($amountSection > 0) {
	do {
		$arrSection[] = $row_eb['id'];
		$arrSectionParent[$row_eb['id']] = $row_eb['parentId'];
		$arrSectionSectionChildren[$row_eb['id']] = false;
		$arrSectionChildren[$row_eb['id']] = array();
		$arrSectionName[$row_eb['id']] = ucfirst(strtolower(rawurldecode($row_eb['name'])));
	} while ($row_eb = mysql_fetch_assoc($eb));
}
function isRoot($n, $n0, $n1) {
	global $arrSectionParent;
	$r = false;
	if ($n>=$n0 && $n<=$n1) {
		$r = true;
	} else {
		for ($i=0; $i<4; $i++) {
			$n = $arrSectionParent[$n];
			if ($n>=$n0 && $n<=$n1) {
				$r = true;
				break;
			} elseif ($n == 0) {
				break;
			}
		}
	}
	return $r;
}
////////////////////////////
$arrStatus = array();
$arrStatus[0] = "In Progress";
$arrStatus[1] = "Closed";
$arrStatus[2] = "Dormant";
$arrStatus[3] = "Sent";
$arrStatus[4] = "Viewed!";

$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$presentationId";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$presentationTitle = "Client: " . trim(rawurldecode($row_eb['firstname'] . " " . $row_eb['lastname']));
$purl = $row_eb['purl'];
$presentationId = $row_eb['id'];
if (isset($row_eb['Value'])) {
	$proposal = getMoneyStringInt($row_eb['Value']);
} else {
	$proposal = "";
}
if (isset($row_eb['Status'])) {
	$mystatus = $arrStatus[$row_eb['Status']];
} else {
	$mystatus = "In Progress";
}
if (isset($row_eb['section'])) {
	$arrSection = explode(",", $row_eb['section']);
}
for ($i=0; $i<count($arrSection); $i++) {
	$sid = $arrSection[$i];
	$pid = $arrSectionParent[$sid];
	$arrSectionChildren[$pid][] = $sid;
}
$query_eb = "SELECT * FROM " . $_dbname . "_sessions WHERE `presentationId`=$presentationId ORDER BY `id` DESC";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalViews = $totalRows_eb = mysql_num_rows($eb);
$arr = array();
if ($totalRows_eb > 0) {
	do {
		$arr[] = $row_eb;
		} while ($row_eb = mysql_fetch_assoc($eb));
}
$arrTimeSpentSession = array();
$arrTimeSpentSection = array();
$totalTimeSpent = 0;
for ($i=0; $i<count($arr); $i++) {
	$a[$i] = array();
	$aSections[$i] = array();
	$aSection[$i] = array();
	$query_eb = "SELECT * FROM " . $_dbname . "_pages WHERE `presentationId`=$presentationId AND `sessionId`=" . $arr[$i]['id'] . " ORDER BY `id`";
	$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
	$row_eb = mysql_fetch_assoc($eb);
	$totalRows_eb = mysql_num_rows($eb);
	if ($totalRows_eb > 0) {
		$na = false;
		do {
			$sec = $row_eb['section'];
			if (substr($sec, 0, 1)=='n') {
				$sid = substr($sec, 1);
				$aSection[$i][$sid] = true;
				if (isset($arrSectionParent[$sid])) {
					$sid = $arrSectionParent[$sid];
				} else {
					//echo $sid;
					$sid = 1;
				}
				for ($j=0; $j<=4; $j++) {
					$aSection[$i][$sid] = true;
					if ($sid == 0) {
						break;
					} else {
						$sid = $arrSectionParent[$sid];
					}
				}
				$timeSpent = getTimeSpent($row_eb['startTime'], $row_eb['endTime']);
				if (isset($a[$i][$sec])) {
					$a[$i][$sec] += $timeSpent;
				} else {
					$a[$i][$sec] = $timeSpent;
				}
				if (isset($arrTimeSpentSession[$i])) {
					$arrTimeSpentSession[$i] += $timeSpent;
				} else {
					$arrTimeSpentSession[$i] = $timeSpent;
				}
				if (isset($arrTimeSpentSection[$sec])) {
					$arrTimeSpentSection[$sec] += $timeSpent;
				} else {
					$arrTimeSpentSection[$sec] = $timeSpent;
					//echo "\"$sec\"";
				}
				if ($row_eb['sessionId'] == 167) {
					//echo $sec . ":" . $a[$i][$sec] .  ", ";
				}
				$totalTimeSpent += $timeSpent;
			}
		} while ($row_eb = mysql_fetch_assoc($eb));
	}
}
$averageTimeSpent = round($totalTimeSpent/count($arr));

$arrSectionMostViewed = array();
$arrSectionMostViewedTime = array();
arsort($arrTimeSpentSection);
foreach ($arrTimeSpentSection as $key => $val) {
	$arrSectionMostViewed[] = $arrSectionName[substr($key, 1)];
	$arrSectionMostViewedTime[] = $val;
}
$mostViewed = 5;
if (count($arrSectionMostViewed)<$mostViewed) {
	$mostViewed = count($arrSectionMostViewed);
}
function getTotalTime($n) {
	if ($n > 0) {
		$n = round($n/60);
		if ($n == 0) {
			$n = 1;
		}
	}
	if ($n > 1) {
		return $n . " minutes";
	} else {
		return $n . " minute";
	}
}
function getMyTime($s) {
	if (strlen($s)>10) {
		$t = mktime(substr($s, 8, 2), substr($s, 10, 2), substr($s, 12, 2), substr($s, 4, 2), substr($s, 6, 2), substr($s, 0, 4));
		return (date("M j,Y", $t) . " at " . date("g:ia", $t));
	} else {
		return "";
	}
}
function getSessionTimeSpent($n) {
	global $arrTimeSpentSession;
	if (isset($arrTimeSpentSession[$n])) {
		return " viewed for " . getFormatedTime2($arrTimeSpentSession[$n]);
	} else {
		return "";
	}
}
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />

<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />

<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>

<script type="text/javascript">
var pid = <?php echo $presentationId; ?>;
var mostViewed = <?php echo $mostViewed; ?>;
var maxViewTime = 60;
var viewInterval;
var topViewNo;
var arrSectionMostViewedTime = new Array();
<?php if ($mostViewed > 0) {
	for ($i=0; $i<count($arrSectionMostViewedTime); $i++) { ?>
		arrSectionMostViewedTime.push(<?php echo $arrSectionMostViewedTime[$i]; ?>);
<?php }} ?>

function ResizeWindow() {	
	var height = jQuery(window).height();
	var width = jQuery(window).width();
	jQuery(".pop-library").css({'width':width, 'height':height});
	jQuery(".pop-library .library").css({'left':(jQuery(window).width()-970)/2});
}
jQuery(function(){
	ResizeWindow();
	jQuery(window).resize(function(){ ResizeWindow();});
})
function showTopView(event) {
	jQuery("#view-slider"+topViewNo).animate({'width':Math.round(arrSectionMostViewedTime[topViewNo]*100/arrSectionMostViewedTime[0])+'%'}, 1000);
	topViewNo++;
	if (topViewNo == mostViewed) {
		clearInterval(viewInterval);
	}
}
function onToggleDetail(event) {
	var id = parseInt(jQuery(this).attr('id').substr(9));
	if (jQuery("#visit-detail"+id).hasClass('visit-detail-hidden')) {
		jQuery("#visit-detail"+id).removeClass('visit-detail-hidden').addClass('visit-detail-show');
		jQuery(this).attr('src', 'images/arrow-up.png');
	} else {
		jQuery("#visit-detail"+id).removeClass('visit-detail-show').addClass('visit-detail-hidden');
		jQuery(this).attr('src', 'images/arrow-down.png');
	}
}
function initIt(){
	jQuery(".toggle-detail").css({'cursor':'pointer'});
	jQuery(".toggle-detail").bind('click', onToggleDetail);
	topViewNo = 0;
	viewInterval = setInterval(showTopView, 400);
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>

<style>
    /** DEFAULT and COMMON PAGE STYLES **/
    .clearfix:after {
        visibility: hidden;
        display: block;
        font-size: 0;
        content: " ";
        clear: both;
        height: 0;
    }
    * html .clearfix             { zoom: 1; } /* IE6 */
    *:first-child+html .clearfix { zoom: 1; } /* IE7 */


    .page-title {color: #575757 !important;}

    /** OVERVIEW STYLES **/
    .overview-block {display: block;width: 33.333333333333%;float: left;}
    .overview-block > div {padding: 0 7px;}
    .overview-block:first-child > div {padding-left: 0 !important;}
    .overview-block:last-child > div {padding-right: 0 !important;}
    .overview-block .overview-title {
        display: block;
        padding: 7px 0 5px 24px;
        background: #575757;
        color: white;
        font-size: 16px;
        font-family: CasperBold, Arial, Helvetica, sans-serif;
    }
    .overview-block .overview-row {font-weight: bold;}
    .overview-block .overview-row.overview-row-thick {padding: 22px 25px 21px 25px;}
    .overview-block .overview-row .block-title {display: block;color: #575757;}
    .overview-block .overview-row .block-value {display: block;color: #59b615;font-size: 24px;}
    .overview-block .overview-row .arrow-down {float: right;margin-top: 10px;}
    .overview-block .block-top-pages ul li {padding: 10px 22px;font-size: 14px;}
    .overview-block .block-top-pages ul li.row-odd{background: #f3f3f3;}
    .overview-block .block-top-pages ul li.row-even{background: #e7e7e7;}
    .overview-block .block-top-pages ul li .block-title-inline {display: inline-block;font-weight: normal;}
    .overview-block .block-top-pages ul li .block-value-inline {display: inline-block;float: right;}
    .value-slider {
        position: relative;
        display: block;
        width: 60px;
        height: 10px;
        background: #aed393;
        margin-top: 3px;
    }
    .value-slider-ind {
        position: absolute;
        top: 0;
        left: 0;
        height: 10px;
        background: #59b615;
    }
    .value-green {color: #6ebe33;}
    .block-value-big {font-size: 33px;}
    .block-value-big.value-green {font-size: 32px !important;}
    .block-value-big span {font-size: 14px;}
    .row-gray {background: #f3f3f3;}

    /** VISITS STYLES **/
    .visits-analytics {clear: all;margin-top: 30px}
    .visits-analytics > ul > li {padding: 20px 0;border-top: 1px solid #f2f2f2;}
    .visits-analytics > ul > li.ion-iphone {margin-left: 5px; !important;}
    .visits-analytics > ul > li .icon-ion {margin-right: 10px;font-size: 20px;vertical-align: middle}
    .block-edited {color: #77cdd7}
    .device-color {color: #77cdd7}
    .visit-block-row .arrow-down,
    .visit-block-row .arrow-up {
        float: right;
        margin-right: 20px;
        margin-top: 10px;
    }
    .visit-block-row .table-view-wrapper {
        display: table;
        width: 100%;
        margin-top: 30px;
        color: #575757;
        font-weight: bold;
    }
    .table-view-wrapper .table-view-th-blue {
        display: table;
        background: #77cdd7 !important;
        color: white;
        width: 100%;
        padding: 3px 0;
    }
    .table-view-wrapper .table-view-th-light-blue {
        display: table;
        background: #c9ebef !important;
        width: 100%;
        padding: 3px 0;
    }
    .table-view-wrapper .table-view-th-light-grey {
        display: table;
        background: #dddddd !important;
        width: 100%;
        padding: 3px 0;
    }
    .table-view-wrapper .table-view-tr {display: table;width: 100%;padding:3px 0 }
    .table-view-wrapper .table-view-tr.row-gray {background: #f2f2f2;}

    /** INDENT STYLES **/
    .view-indent-1 {text-indent: 30px;}
    .view-indent-2 {text-indent: 65px;}
    .view-indent-3 {text-indent: 100px;}
    .view-indent-4 {text-indent: 135px;}

    /** GRID STYLES **/
    .table-view-half {display: table-row;width: 50%;float: left;}
    .table-view-2 {display: table-row;width: 16.6666666667%;float: right;}
    .table-view-3 {display: table-row;width: 25%;float: left;}
    .table-view-9 {display: table-row;width: 75%;float: left;}
    .table-view-10 {display: table-row;width: 83.3333333333%;float: left;}

.visit-detail-hidden {display:none;}
.visit-detail-show {display:block;}

.overview-block .overview-row .block-title {display: block;color: #575757;}
.overview-block .overview-row .inputslot {display:block; width:280px; height:20px; line-height:38xp; color:#59b615; background:none; border:0px solid #59b615; padding:2px 5px; margin-top:3px;}
</style>
</head>
<body class="body-with-footer">
<div id="mainContainer" class="main-no-bottom">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"><?php echo rawurldecode($presentationTitle); ?></div>
			<div class="top-center-right">
				<a href="#" onClick="return window.close();" class="btn btn-blue" id="btnSave">Exit</a>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Presentation (<?php echo rawurldecode($presentationTitle); ?>) Analytics</div>
		<div class="line-break-grey"></div>

        <div class="overview-analytics clearfix">
            <div class="overview-block">
                <div class="block-info">
                    <span class="overview-title">Presentation Info</span>
                    <div class="overview-row overview-row-thick">
                        <span class="block-title">Value of Proposal</span>
                        <span class="block-value"><?php echo $proposal; ?></span>
                    </div>
                    <div class="overview-row overview-row-thick row-gray">
                        <span class="block-title">Status</span>
                        <span class="block-value"><span><?php echo $mystatus; ?></span><!--<img class="arrow-down" src="images/arrow-down.png">--></span>
                    </div>
                </div>
            </div>
            <div class="overview-block">
                <div class="block-top-pages">
                    <span class="overview-title">Top <?php echo $mostViewed; ?> Pages Viewed</span>
                    <ul>
						<?php for ($i=0; $i<$mostViewed; $i++) { ?>
                        <li class="overview-row row-odd">
                            <span class="block-title-inline"><?php echo $i+1; ?>. <?php echo $arrSectionMostViewed[$i]; ?></span>
                            <span class="block-value-inline">
                                <span class="value-slider">
                                    <span class="value-slider-ind" id="view-slider<?php echo $i; ?>" style="width:0%"></span>
                                </span>
                            </span>
                        </li>
						<?php } ?>
                    </ul>
                </div>
            </div>
            <div class="overview-block block-stats">
                <div class="block-stats">
                    <span class="overview-title">Presentation Stats</span>
                    <div class="overview-row overview-row-thick">
                        <span class="block-value-big"><i class="ion-checkmark-circled value-green"></i> <?php echo $totalViews; ?> VIEW<?php if ($totalViews>1) { ?>S<?php } ?></span>
                    </div>
                    <div class="overview-row overview-row-thick row-gray">
                        <span class="block-value-big value-green"><?php echo getTotalTime($totalTimeSpent); ?></span>
                        <span class="block-title">Total Viewing Time</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="visits-analytics">
            <ul>
				<?php for ($i=0; $i<$totalViews; $i++) {
					$visitNo = $totalViews - $i;
					?>
                <li class="visit-block">
                    <div class="visit-block-row toggle-detail" id="btnToggle<?php echo $visitNo; ?>">
                        <span><i class="ion-monitor icon-ion"></i> Visit <?php echo $visitNo; ?> - <?php echo getMyTime($arr[$i]['time']); ?><?php echo getSessionTimeSpent($i); ?> <span class="device-color">(<?php echo "System: " . getSystem($arr[$i]['system']); ?>)</span></span>
                        <img class="arrow-up btn-toggle" src="images/arrow-down.png">
                    </div>
                    <div class="visit-block-row visit-detail-hidden" id="visit-detail<?php echo $visitNo; ?>">
                        <ul class="table-view-wrapper">
                            <!-- MAIN TITLE -->
                            <li class="table-view-th">
                                <span class="table-view-10 view-indent-1">Page Title</span>
                                <span class="table-view-2">Time on Page</span>
                            </li>
							
                            <!-- SUBSECTION AND SUBTITLE -->
                            <?php
							$sid0 = 0;
							for ($k0=0; $k0<count($arrSectionChildren[$sid0]); $k0++) {
								$sid1 = $arrSectionChildren[$sid0][$k0];
								if (isset($aSection[$i][$sid1])) {
									echo "<li class='table-view-th-blue'><span class='table-view-half view-indent-1'>" . $arrSectionName[$sid1] . "</span>";
									if (isset($a[$i]["n" . $sid1])) {
										echo "<span class='table-view-2'>" . getFormatedTime($a[$i]["n" . $sid1]) . "</span>";
									}
									echo "</li>";
									for ($k1=0; $k1<count($arrSectionChildren[$sid1]); $k1++) {
										$sid2 = $arrSectionChildren[$sid1][$k1];
										if (isset($aSection[$i][$sid2])) {
											echo "<li class='table-view-th-light-blue'><span class='table-view-half view-indent-2'>" . $arrSectionName[$sid2] . "</span>";
											if (isset($a[$i]["n" . $sid2])) {
												echo "<span class='table-view-2'>" . getFormatedTime($a[$i]["n" . $sid2]) . "</span>";
											}
											echo "</li>";
											if ($levels > 2) {
												for ($k2=0; $k2<count($arrSectionChildren[$sid2]); $k2++) {
													$sid3 = $arrSectionChildren[$sid2][$k2];
													if (isset($aSection[$i][$sid3])) {
														echo "<li class='table-view-th-light-grey'><span class='table-view-half view-indent-3'>" . $arrSectionName[$sid3] . "</span>";
														if (isset($a[$i]["n" . $sid3])) {
															echo "<span class='table-view-2'>" . getFormatedTime($a[$i]["n" . $sid3]) . "</span>";
														}
														echo "</li>";
													}
												}
											}
										}
									}
								}
							}
							?>
                        </ul>
                        <div class="line-break-grey"></div>
                    </div>
                </li>
				<?php } ?>
            </ul>
        </div>
	</div>
	<div class="footer">
		<p class="footer-copy">&copy; Copyright 2015 &bull; Channel 1 Media Solutions &bull; All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &bull; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.channel1media.com" target="_blank">www.channel1media.com</a></p>
		<p class="footer-logo"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c.png" /></a></p>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>