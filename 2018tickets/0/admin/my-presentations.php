<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
} else {
	header('Location: index.php');
	exit;
}
$pid = 0;
if (isset($_GET['id'])) {
	$pid = $_GET['id'];
}
function getListTime($s) {
	if (strlen($s)>10) {
		$t = mktime(substr($s, 8, 2), substr($s, 10, 2), substr($s, 12, 2), substr($s, 4, 2), substr($s, 6, 2), substr($s, 0, 4));
		return date("M d, Y H:i", $t);
	} else {
		return "";
	}
}
function getRepName($r) {
	$ar = explode(" ", $r);
	if (count($ar) == 1) {
		return $ar[0];
	} else {
		return $ar[0] . " " . substr($ar[1], 0, 1) . ".";
	}
}
function removeFirstSpace($s) {
	for ($i=0; $i<5; $i++) {
		if (substr($s, 0, 1) == " ") {
			$s = substr($s, 1);
		} else {
			break;
		}
	}
	return $s;
}
function getPercentString($n) {
	$r = getIntString($n * 100);
	if (strlen($r) == 1) {
		$r = "00" . $r;
	} elseif (strlen($r) == 2) {
		$r = "0" . $r;
	}
	$r = substr($r, 0, strlen($r)-2) . "." . substr($r, strlen($r)-2);
	return $r . "%";
}

$multipleMaster = false;
if (isset($_multipleMaster)) {
	$multipleMaster = $_multipleMaster;
}
$listNoValue = false;
if (isset($_listNoValue)) {
	$listNoValue = $_listNoValue;
}
$listNoStatus = false;
if (isset($_listNoStatus)) {
	$listNoStatus = $_listNoStatus;
}
$listNoClient = false;
if (isset($_listNoClient)) {
	$listNoClient = $_listNoClient;
}
$listLastSaved = false;
if (isset($_listLastSaved)) {
	$listLastSaved = $_listLastSaved;
}
if (isset($_arrType)) {
	$_arrGroup = $_arrType;
}

$mykey = "lastSavedTime";
$mydirection = 1;
$myorder = "DESC";
if (isset($_GET['s'])) {
	$mykey = $_GET['s'];
}
if (isset($_GET['d'])) {
	if ($_GET['d'] == 0) {
		$mydirection = 0;
		$myorder = "ASC";
	}
}
function getSortDirection($s) {
	global $mykey, $mydirection;
	$d = 0;
	if ($s == $mykey) {
		$d = 1-$mydirection;
	}
	return "s=" . $s . "&d=" . $d;
}
function getArrow($s) {
	global $mykey, $mydirection;
	$d = 0;
	$extra = "";
	if ($s == $mykey) {
		if ($mydirection == 1) {
			return "down-blue";
		} else {
			return "up-blue";
		}
	} else {
		return "up-grey";
	}
}

mysql_select_db($database_channel1media, $channel1media);

$query_eb = "SELECT * FROM " . $_dbname . "_reps";
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);
$arrReps = array();
if ($totalRows_eb > 0) {
	do {
		$arrReps[$row_eb['id']] = getRepName($row_eb['rep']); 
	} while ($row_eb = mysql_fetch_assoc($eb));
}

if ($mykey == "client") {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `repId`=$repId AND `active`=1 ORDER BY `firstname` $myorder, `lastname` $myorder";
} else {
	$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `repId`=$repId AND `active`=1 ORDER BY `$mykey` $myorder";
}
$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
$row_eb = mysql_fetch_assoc($eb);
$totalRows_eb = mysql_num_rows($eb);

$arrMine = array();
$i = 0;
if ($totalRows_eb > 0) {
	do {
		$arrMine[$i] = $row_eb;
		$arrMine[$i]['client'] = trim(rawurldecode($arrMine[$i]['firstname'])) . " " . trim(rawurldecode($arrMine[$i]['lastname']));
		$arrMine[$i]['rep'] = $arrReps[$arrMine[$i]['repId']];
		$i++;
	} while ($row_eb = mysql_fetch_assoc($eb));
}

$arrStatus = array();
$arrStatus[0] = "In Progress";
$arrStatus[1] = "Closed";
$arrStatus[2] = "Dormant";
$arrStatus[3] = "Sent";
$arrStatus[4] = "Viewed!";
?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
<meta name="format-detection" content="telephone=no"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=1100, user-scalable=no" name="viewport">
<title>ePitch Admin - Presentation</title>
<link href="favicon.ico" rel="shortcut icon">
<link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/epitch.css" />
<style type="text/css">

#mainContainer .mainContent .content-block {position:relative; display:block; width:auto; font-family: 'Casper'; font-size:16px; line-height:20px;}
#mainContainer .mainContent .content-block span {position:relative; display:inline-block; vertical-align:top; padding:17px 0;}
#mainContainer .mainContent .content-block .name {width:calc(100% - 620px); padding-left:10px; white-space:nowrap; overflow:hidden; margin-right:20px;}
#mainContainer .mainContent .content-block .hits {width:50px; }
#mainContainer .mainContent .content-block .last-hit {width:140px; }
#mainContainer .mainContent .content-block .last-saved {width:140px; }
#mainContainer .mainContent .content-block .value {width:100px; }
#mainContainer .mainContent .content-block .status {width:120px; }
#mainContainer .mainContent .content-block img {vertical-align:middle;}
#mainContainer .mainContent .content-block .line-break-grey {margin:0px !important;}

#mainContainer .mainContent .content-block .dropdown {z-index:100; display:block; position:absolute; overflow:hidden; width:120px; height:20px; right:0px; top:5px; border-radius:3px; padding:10px; }
#mainContainer .mainContent .content-block .dropdown a {display:block; width:100%; height:30px; text-decoration:none; color:#494949; font-family: 'ProximaNova-bold'; font-weight:lighter; font-size:13px; }
#mainContainer .mainContent .content-block .dropdown .arrow-down {display:none; position:absolute; right:5px; top:15px;}
#mainContainer .mainContent .content-block .dropdown a span {padding:0px;}

#mainContainer .mainContent .title-block {font-family:'ProximaNova-Bold'; text-transform:uppercase; font-size:13px; color:#494949;}
#mainContainer .mainContent .title-block .icon {margin-left:5px;}
#mainContainer .mainContent .title-block a {color:#494949; text-decoration:none;}
#mainContainer .mainContent .title-block a img {display:inline-block; margin-left:0px; margin-bottom:3px;}
#mainContainer .mainContent .item-block {/*background-color:#f2f2f2;*/ /*overflow:hidden;*/ height:auto; border-bottom:1px solid #f2f2f2; border-radius:3px;}
#mainContainer .mainContent .item-block img {margin-right:10px;}
#mainContainer .mainContent .item-block .buttons {display:block; margin-left:25px; height:0px; overflow:hidden;}
#mainContainer .mainContent .item-block .buttons a {position:relative; display:inline-block; transition:background-color 0.3s; vertical-align:top; width:auto; color:#fff; background:none; background-color:#575757; height:36px; line-height:36px; border:none; border-radius:5px; font-size:13px; text-align:center; text-transform:uppercase; font-family: 'ProximaNova-Bold'; text-decoration:none; padding:0 24px; margin:3px;}
#mainContainer .mainContent .item-block .buttons a img {transition:filter 0.3s;}
#mainContainer .mainContent .item-block .buttons a:hover {background-color:#77cdd7;}
#mainContainer .mainContent .item-block .buttons a:hover img {-webkit-filter:brightness(200%); filter:brightness(200%);}
#mainContainer .mainContent .item-block .buttons .btn-delete:hover {background-color:#ff0000;}


/*#mainContainer .mainContent .item-block .buttons .btn-delete {margin-left:150px;}*/
#mainContainer .mainContent .item-block .buttons .btn-img-only img {margin-right:0;}
#mainContainer .mainContent .item-block .buttons .btn-img-only {padding:0 15px;}

#mainContainer .mainContent .item-block .buttons .analytics-disabled {background-color:#d3d3d3; pointer-events:none; cursor:default;}

#mainContainer .mainContent .block-normal {background-color:none;}
#mainContainer .mainContent .block-over {background-color:#f2f2f2;}

#mainContainer .mainContent .close-block {background-color:none; /*height:50px;*/}
#mainContainer .mainContent .open-block {background-color:#e7e7e7; /*height:110px;*/}


</style>
<script language="javascript" type="text/javascript" src="js/jQuery.js"></script>
<script type="text/javascript">
var pno = <?php echo $pid; ?>;
var deletePresentationId = 0;
var sendPresentationId = 0;
var lastNo = -1;
var _url = "<?php echo $_url; ?>";
function onMouseClickItem(event) {
	if (event.pageY - jQuery(this).offset().top < 50) { 
		var pno = parseInt(jQuery(this).attr('id').substr(1));
		if (lastNo > 0) {
			jQuery("#p"+lastNo).removeClass("open-block block-over").addClass("close-block");
			jQuery("#p"+lastNo+" .buttons").animate({'height':0}, 300);
			jQuery("#p"+lastNo+" .name img").attr('src', 'images/arrow-down.png');
		}
		if (lastNo != pno) {
			jQuery("#p"+pno).removeClass("close-block").addClass("open-block");
			jQuery("#p"+pno+" .buttons").animate({'height':60}, 300);
			jQuery("#p"+pno+" .name img").attr('src', 'images/arrow-up.png');
			lastNo = pno;
		} else {
			lastNo = -1;
		}
	}
}
function onMouseClickSend(pid, purl, email) {
	var strSubject = "For your review";
	sendPresentationId = pid;
	window.location.href = "mailto:" + email + "?subject=" + strSubject + "&body=" + escape("Please click the link below:\n\n" + _url + purl);
	setTimeout(openPopAction, 1000);
}
function onMouseClickSendBookmark(pid, purl, email) {
	var strSubject = "For your review";
	sendPresentationId = pid;
	window.location.href = "mailto:" + email + "?subject=" + strSubject + "&body=" + escape("Please click the link below:\n\n" + _url + "views/" + purl);
	setTimeout(openPopAction, 1000);
}
function onMouseClickDelete(n) {
	deletePresentationId = n;
	confirmAction("Are you sure you want to delete this presentation permanently?", "delete-presentation");
	return false;
}
function onConfirmAction(s) {
	var info = {};
	info.id = deletePresentationId;
	info.action = "delete";
	jQuery.post("submitPresentationDelete.php", info, function(data) {
		var r = data + "";
		if (r == "failed") {
			alertMsg("Sorry, we cannot complete your request at this moment, please try again later.");
		} else {
			window.location = 'my-presentations.php';
		}
	});
}
var dropdownId = 0;
function onMouseOverMyStatus(event) {
	event.stopPropagation();
	var n = parseInt(jQuery(this).parent().parent().attr('id').substr(1));
	if (n != dropdownId) {
		jQuery(this).parent().removeClass("close-block").addClass("open-block");
		jQuery(this).children(".arrow-down").css({'display':'block'});
	}
}
function onMouseOutMyStatus(event) {
	event.stopPropagation();
	var n = parseInt(jQuery(this).parent().parent().attr('id').substr(1));
	if (n != dropdownId) {
		jQuery(this).parent().removeClass("open-block").addClass("close-block");
		jQuery(this).children(".arrow-down").css({'display':'none'});
	}
}
function onMouseClickMyStatus(event) {
	event.stopPropagation();
	var n = parseInt(jQuery(this).parent().parent().attr('id').substr(1));
	if (dropdownId == n) {
		jQuery(this).parent().removeClass("open-block").addClass("close-block").css({'height':20});
		dropdownId = 0;
	} else {
		if (dropdownId > 0) {			
			jQuery("#p"+dropdownId+" .dropdown").removeClass("open-block").addClass("close-block").css({'height':20});
			jQuery("#p"+dropdownId+" .dropdown .arrow-down").css({'display':'none'});
		}
		if (jQuery(this).parent().hasClass('full-menu')) {
			jQuery(this).parent().removeClass("close-block").addClass("open-block").animate({'height':170}, 300);
		} else {
			jQuery(this).parent().removeClass("close-block").addClass("open-block").animate({'height':145}, 300);
		}
		dropdownId = n;
	}
}
var arrStatus = new Array();
arrStatus['in-progress'] = 0;
arrStatus['closed'] = 1;
arrStatus['dormant'] = 2;
arrStatus['sent'] = 3;
arrStatus['viewed'] = 4;
function onMouseClickStatusOption(event) {
	event.stopPropagation();
	var n = parseInt(jQuery(this).parent().parent().attr('id').substr(1));
	var thestatus = jQuery(this).children("span").text();
	jQuery(this).parent().children(".mystatus").children(".arrow-down").css({'display':'none'});
	jQuery(this).parent().children(".mystatus").children("span").text(thestatus);
	thestatus = thestatus.toLowerCase().replace("!", "").replace(" ", "-");
	jQuery(this).parent().children(".mystatus").children(".status-indicator").attr('src', 'images/status-'+thestatus+".png");
	jQuery(this).parent().removeClass("open-block").addClass("close-block").css({'height':20});
	dropdownId = 0;

	var info = {};
	info.id = n;
	info.mystatus = arrStatus[thestatus];
	info.r = Math.random();
	jQuery.post("submitPresentationStatus.php", info, function(data) {
		var r = data + "";
		if (r == "success") {
		} else {
			alertMsg("server-error");
		}
	});
}
function changeStatusSent() {
	jQuery("#p"+sendPresentationId+" .status-sent").trigger('click');
	closePopAction();
	return false;
}
function onMouseOverItem(event) {
	if (!jQuery(this).hasClass('open-block')) {
		jQuery(this).removeClass('block-normal').addClass('block-over');
	}
}
function onMouseOutItem(event) {
	if (!jQuery(this).hasClass('open-block')) {
		jQuery(this).removeClass('block-over').addClass('block-normal');
	}
}
function initIt() {
	jQuery(".item-block").css({'cursor':'pointer'});
	jQuery(".item-block").bind('mouseover', onMouseOverItem);
	jQuery(".item-block").bind('mouseout', onMouseOutItem);
	jQuery(".item-block").bind('click', onMouseClickItem);
	jQuery(".mystatus").bind('mouseover', onMouseOverMyStatus);
	jQuery(".mystatus").bind('mouseout', onMouseOutMyStatus);
	jQuery(".mystatus").bind('click', onMouseClickMyStatus);
	jQuery(".status-option").bind('click', onMouseClickStatusOption);

	if (pno != 0) {
		jQuery("#p"+pno).removeClass("close-block").addClass("open-block");
		jQuery("#p"+pno+" .buttons").animate({'height':60}, 300);
		jQuery("#p"+pno+" .name img").attr('src', 'images/arrow-up.png');
		lastNo = pno;
	}
}
</script>
<script language="javascript" type="text/javascript" src="js/ePitch.js"></script>
</head>
<body class="body-with-footer">
<div id="mainContainer" class="main-no-bottom">
	<div class="top">
		<div class="top-center">
			<a href="dashboard.php"><img class="icon-epitch" src="images/epitch.png" /></a>
			<div class="presentation-name"></div>
			<div class="top-center-right">
				<div class="mycontrol">
					<a href="#" class="myaccount" onClick="return onMouseClickMyControl();">
						<p class="team-logo"></p>
						<p class="myinfo"><?php echo $_SESSION['rep']; ?><span class="myteam"><?php echo $_team; ?></span></p>
						<img class="arrow-down" src="images/arrow-down.png" />
					</a>
					<a href="dashboard.php" class="control-btn">Dashboard</a>
					<a href="http://c1ms.com/ePitch/ePitchLiteManual2.0.pdf" target="_blank" class="control-btn">ePitch Manual</a>
					<?php if ($repId == 1) { ?>
					<a href="http://c1ms.com/ePitch/ePitchManualAdmin.pdf" target="_blank" class="control-btn">ePitch Manual - Admin</a>
					<?php } ?>
					<a href="index.php?a=logout" class="control-btn">Log Out</a>
				</div>
			</div>
		</div>
	</div>
	<div class="mainContent">
		<div class="page-title"><img src="images/icon-dashboard.png" />Your custom presentations</div>
		<div class="content-block title-block">
			<a href="my-presentations.php?<?php echo getSortDirection('client'); ?>"><span class="name">Client<img src="images/arrow-sort-<?php echo getArrow('client'); ?>.png" /></span></a>
			<a href="my-presentations.php?<?php echo getSortDirection('hits'); ?>"><span class="hits">Hits<img src="images/arrow-sort-<?php echo getArrow('hits'); ?>.png" /></span></a>
			<a href="my-presentations.php?<?php echo getSortDirection('lasttime'); ?>"><span class="last-hit">Last Hit<img src="images/arrow-sort-<?php echo getArrow('lasttime'); ?>.png" /></span></a>
			<a href="my-presentations.php?<?php echo getSortDirection('lastSavedTime'); ?>"><span class="last-saved">Last Saved<img src="images/arrow-sort-<?php echo getArrow('lastSavedTime'); ?>.png" /></span></a>
			<a href="my-presentations.php?<?php echo getSortDirection('Value'); ?>"><span class="value">Value<img src="images/arrow-sort-<?php echo getArrow('Value'); ?>.png" /></span></a>
			<?php  if ($repId > 1) { ?>
			<a href="my-presentations.php?<?php echo getSortDirection('Status'); ?>"><span class="status">Status<img src="images/arrow-sort-<?php echo getArrow('Status'); ?>.png" /></span></a>
			<?php } ?>
			<div class="line-break-grey"></div>
		</div>
		<?php if (count($arrMine) > 0) {
			for ($i=0; $i<count($arrMine); $i++) {
				$id = $arrMine[$i]['id'];
				$mystatus = $arrStatus[$arrMine[$i]['Status']];
				$hasPurl = false;
				if (isset($arrMine[$i]['purl'])) {
					$hasPurl = true;
				} else {
					$arrMine[$i]['client'] = "Note: " . rawurldecode($arrMine[$i]['clientNote']);
				}					
				?>
		<div class="content-block item-block" id="p<?php echo $id; ?>">
			<span class="name"><img src="images/arrow-down.png" /><?php echo $arrMine[$i]['client']; ?></span>
			<span class="hits"><?php echo $arrMine[$i]['hits']; ?></span>
			<span class="last-hit"><?php echo getListTime($arrMine[$i]['lasttime']); ?></span>
			<span class="last-saved"><?php echo getListTime($arrMine[$i]['lastSavedTime']); ?></span>
			<span class="value"><?php echo getMoneyStringInt($arrMine[$i]['Value']); ?></span>
			<?php  if ($repId > 1) { ?>
			<div class="status dropdown <?php if ($arrMine[$i]['hits']>0) { ?>full-menu<?php } else {?>part-menu<?php } ?>" id="d<?php echo $id; ?>" style="z-index:<?php echo 1000-$i; ?>;">
				<a href="#" class="mystatus"><img class="status-indicator" src="images/status-<?php echo str_replace(" ", "-", str_replace("!", "", strtolower($mystatus))); ?>.png" /><span><?php echo $mystatus; ?></span><img class="arrow-down" src="images/arrow-down.png" /></a>
				<a href="#" class="status-option" data-id="in-progress"><img src="images/status-in-progress.png" /><span>In Progress</span></a>
				<a href="#" class="status-option status-sent" data-id="sent"><img src="images/status-sent.png" /><span>Sent</span></a>
				<?php if ($arrMine[$i]['hits']>0) { ?>
				<a href="#" class="status-option" data-id="viewed"><img src="images/status-viewed.png" /><span>Viewed!</span></a>
				<?php } ?>
				<a href="#" class="status-option" data-id="closed"><img src="images/status-closed.png" /><span>Closed</span></a>
				<a href="#" class="status-option" data-id="dormant"><img src="images/status-dormant.png" /><span>Dormant</span></a>
			</div>
			<?php } ?>
			<div class="buttons">
				<?php  if ($repId > 1) { ?>
				<a href="presentation.php?id=<?php echo $arrMine[$i]['id']; ?>" ><img src="images/icon-edit-presentation.png" />Edit Info</a>
				<?php } ?>
				<?php if ($hasPurl) { ?>
				<a href="tree.php?id=<?php echo $arrMine[$i]['id']; ?>" ><img src="images/icon-edit-presentation.png" />Edit Content</a>
				<a href="../../?preview.<?php echo $arrMine[$i]['purl']; ?>" target="_new" ><img src="images/icon-preview.png" />Preview</a>
				<!--<a href="#" ><img src="images/icon-generate-pdf.png" />Generate PDF</a>-->
				<?php  if ($repId > 1) { ?>
				<a href="#" onClick='return onMouseClickSend(<?php echo $arrMine[$i]['id'] . ", \"" . $arrMine[$i]['purl'] . "\", \"" . $arrMine[$i]['email'] . "\""; ?>);' ><img src="images/icon-send-presentation.png" />Email</a>
				<a href="analytics.php?id=<?php echo $arrMine[$i]['id']; ?>" target="_blank" <?php if ($arrMine[$i]['hits']==0) {?>class="analytics-disabled"<?php } ?> ><img src="images/icon-view-analytics<?php if ($arrMine[$i]['hits']==0) {?>-white<?php } ?>.png" />View Analytics</a>
				<a href="presentation.php?rid=<?php echo $arrMine[$i]['id']; ?>" ><img src="images/icon-replicate.png" />Replicate</a>
				<a href="#" onClick="return onMouseClickDelete(<?php echo $id; ?>);" class="btn-img-only btn-delete"><img src="images/icon-delete-presentation.png" /></a>
				<?php } ?>
				<?php } ?>
			</div>
		</div>
		<?php }} ?>
	</div>

	<div class="footer">
		<p class="footer-copy">&copy; Copyright 2015 &bull; Channel 1 Media Solutions &bull; All Rights Reserved&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &bull; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.channel1media.com" target="_blank">www.channel1media.com</a></p>
		<p class="footer-logo"><a href="http://www.channel1media.com" target="_blank"><img src="images/logo-c.png" /></a></p>
	</div>
	<div id="pop-action" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message">Did you send the presentation?</span>
			<a class="btn-cancel" href="#" onClick="return closePopAction();">No</a>
			<a class="btn-ok" href="#" onClick="return changeStatusSent();">Yes</a>
		</div>
	</div>
	<div id="pop-up" class="pop-up">
		<div class="pop-up-box">
			<span class="pop-up-message"></span><br />
			<a class="btn-cancel" href="#" onClick="return cancelConfirm();">Cancel</a>
			<a class="btn-yes" href="#" onClick="return yesConfirm();">Yes</a>
			<a class="btn-ok" href="#" onClick="return closeAlert();">OK</a>
		</div>
	</div>
	<div id="flash-msg" class="flash-msg">
		<img src="images/icon-check.png" /><span class="pop-up-message"></span>
	</div>
</div>
</body>