<?php require_once('../../Connections/channel1media.php'); ?>
<?php
session_start();
$r = "";
$thetime = date("YmdHis");
if (isset($_SESSION['repId'])) {
	$repId = $_SESSION['repId'];
	mysql_select_db($database_channel1media, $channel1media);
	$id = $_POST['id'];
	$rid = $_POST['rid'];
	$presentationValue = trim($_POST['presentationValue']);
	if (isset($_POST['firstname']) || isset($_POST['lastname']) || isset($_POST['email'])) {
		$firstname = rawurlencode(trim(rawurldecode($_POST['firstname'])));
		$lastname = rawurlencode(trim(rawurldecode($_POST['lastname'])));
		$email = rawurlencode(trim(rawurldecode($_POST['email'])));
		if ($id == 0) {
			$purl = getPurl(trim(rawurldecode($_POST['firstname'])) . " " . trim(rawurldecode($_POST['lastname'])));
			$insertSQL = sprintf("INSERT INTO " . $_dbname . "_presentations (`repId`, `replicateFrom`, `purl`, `Value`, `firstname`, `lastname`, `email`, `lastSavedTime`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
				   GetSQLValueString($repId, "int"),
				   GetSQLValueString($rid, "int"),
				   GetSQLValueString($purl, "text"),
				   GetSQLValueString($presentationValue, "text"),
				   GetSQLValueString($firstname, "text"),
				   GetSQLValueString($lastname, "text"),
				   GetSQLValueString($email, "text"),
				   GetSQLValueString($thetime, "text"));
			$insertHits = mysql_query($insertSQL, $channel1media) or die(mysql_error());
			$pid = $id = mysql_insert_id();

			if ($rid > 1) {
				$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$rid";
				$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
				$row_eb = mysql_fetch_assoc($eb);

				$updateSQL = "UPDATE " . $_dbname . "_presentations SET `section`='" .$row_eb['section']  . "', `sectionOff`='" .$row_eb['sectionOff']  . "' WHERE id=$id";
				$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
			}
		} else {
			$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=$id";
			$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
			$row_eb = mysql_fetch_assoc($eb);
			$totalRows_eb = mysql_num_rows($eb);
			$newBookmark = true;
			if ($totalRows_eb > 0) {
				if (isset($row_eb['purl'])) {
					$newBookmark = false;
					$updateSQL = "UPDATE " . $_dbname . "_presentations SET `Value`='$presentationValue', `firstname`='$firstname', `lastname`='$lastname', `email`='$email' WHERE id=$id";
					$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
				}
			}
			if ($newBookmark) {
				$purl = getPurl(trim(rawurldecode($_POST['firstname'])) . " " . trim(rawurldecode($_POST['lastname'])));
				$query_eb = "SELECT * FROM " . $_dbname . "_bookmark WHERE `presentationId`=$id";
				$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
				$row_eb = mysql_fetch_assoc($eb);
				$totalRows_eb = mysql_num_rows($eb);
				if ($totalRows_eb > 0) {
					$arrBookmark = explode(",", $row_eb['bookmarks']);
				}

				$query_eb = "SELECT * FROM " . $_dbname . "_sections WHERE `active`=1 ORDER BY `parentId`, `id` ASC";
				$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
				$row_eb = mysql_fetch_assoc($eb);
				$arrSection = array();
				$arrSectionParent = array();
				do {
					$arrSection[] = $row_eb['id'];
					$arrSectionParent[$row_eb['id']] = $row_eb['parentId'];
					$arrSectionOff[$row_eb['id']] = true;
				} while ($row_eb = mysql_fetch_assoc($eb));

				$query_eb = "SELECT * FROM " . $_dbname . "_presentations WHERE `id`=1";
				$eb = mysql_query($query_eb, $channel1media) or die(mysql_error());
				$row_eb = mysql_fetch_assoc($eb);
				if (isset($row_eb['section'])) {
					$section = $row_eb['section'];
					$sectionOff = $row_eb['sectionOff'];
				} else {
					$section = implode(",", $arrSection);
					$sectionOff = "";
				}
				if (count($arrBookmark)>0) {
					for ($i=0; $i<count($arrBookmark); $i++) {
						$bid = $arrBookmark[$i];
						$arrSectionOff[$bid] = false;
						$bid = $arrSectionParent[$bid];
						if ($bid != 0) {
							$arrSectionOff[$bid] = false;								
							$bid = $arrSectionParent[$bid];
							if ($bid != 0) {
								$arrSectionOff[$bid] = false;								
								$bid = $arrSectionParent[$bid];
								if ($bid != 0) {
									$arrSectionOff[$bid] = false;
								}
							}
						}
					}
					$sectionOff = "";
					for ($i=0; $i<count($arrSection); $i++) {
						$sid = $arrSection[$i];
						if ($arrSectionOff[$sid]) {
							if ($sectionOff != "") {
								$sectionOff .= ",";
							}
							$sectionOff .= $sid;
						}
					}
				}
				$updateSQL = "UPDATE " . $_dbname . "_presentations SET `Value`='$presentationValue', `firstname`='$firstname', `lastname`='$lastname', `email`='$email', `purl`='$purl', `section`='$section', `sectionOff`='$sectionOff' WHERE id=$id";
				//echo $updateSQL;
				$updateHits = mysql_query($updateSQL, $channel1media) or die(mysql_error());
			}
		}
		$r = "success:$id";
	}
}
echo $r;