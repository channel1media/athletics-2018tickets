<!DOCTYPE HTML>
<html>
<head>

<meta name="robots" content="noindex">
<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/x-icon" href="images/favicon.ico" />

<title>Oakland Athletics</title>

	<!--[if lte IE 9]>
	<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
	<![endif]-->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="js/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!--[if lte IE 8]><script src="js/selectivizr.min.js"></script><![endif]-->

	<link rel="stylesheet" href="shadowbox/shadowbox.css">
	<link rel="stylesheet" type="text/css" href="css/hamburgers.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="css/athletics.css">

	<script type="text/javascript">
		var ie8 = false;
	</script>
	<!--[if lt IE 9]>
	<script type="text/javascript">
		ie8 = true;
	<![endif]-->
	</script>
	
</head>

<body class="show-loading">

<?php include('epitchlite.php'); ?>

<div id="shade"></div>

<!-- Loader -->
	<div id="loading-container">
		<div class="loading-bar"></div>
		<div class="inner-loader ca">
			<img src="images/logo.png"/>
		</div>
	</div>
<!-- Loader -->

<!-- Video -->
	<div class="c1ms-clearfix video-controls-box">
		<div class="skip-video-button">
			<div><p>Skip Video</p></div>
		</div>
	</div>
<!-- Video -->

<div id="container">

	<!-- rep popup -->
		<!-- <div class="rep-popup">
			<img src="images/reps/<?php echo (str_replace(" ", "-", $rep)); ?>.jpg">
		</div> -->
	<!-- rep popup -->

	<div id="header">
		<div class="topRailOutter">
			<div class="topRail">
				<div class="navLogo" data-id="#pg1" data-track="Home"><img src="images/logo.png"/></div>
				<div class="rooted-in-oakland-mobile"></div>
				<div class="hamburger hamburger--squeeze" tabindex="0" aria-label="Menu" role="button">
					<!-- <div class="hamburger-label">Menu</div>-->					
					<div class="hamburger-box">
						<div class="hamburger-inner"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- rep info -->
		<div class="rep-bar">
			<div class="rep">
				<div class="rep-name">Your Season Ticket Rep is <span class="rep-hover"><?php echo $rep; ?></span></div>
				<span class="whitedot">&middot;</span>
				<div class="rep-email"><a href="mailto:<?php echo $repEmail; ?>""><?php echo $repEmail; ?></a></div>
				<span class="whitedot">&middot;</span>
				<div class="rep-phone"><?php echo $repPhone; ?></div>
				<div id="btnSound"><img src="images/sound-on.png" /></div>
				<a href="share.php?type=DotCom" target="_blank" class="dot-com"><img src="images/dotcom.png"></a>
			</div>
		</div>
		<!-- rep info -->

		<!-- mid bar -->
		<div class="mid-bar">
			<div class="rooted-in-oakland">
				<div class="navLogo" data-id="#pg1" data-track="Home"><img src="images/logo.png"></div>
				<div class="title">Rooted In Oakland</div>
				<div class="hamburger hamburger--squeeze" tabindex="0" aria-label="Menu" role="button">
					<!-- <div class="hamburger-label">Menu</div>-->					
					<div class="hamburger-box">
						<div class="hamburger-inner"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- mid bar -->

		<!-- navigation -->
			<div class="nav">
				
				<ul class="nav-holder">

				<?php 
				for ($i=0; $i<count($arrSection); $i++) { 
					$s0 = $arrSection[$i]; 
					$sectionName = rawurldecode($arrSectionName[$s0]); 
					if ($arrSectionParent[$s0] == 0 && $s0 != 1) {
					?>
					<li class="nBtn" data-id="#pg<?php echo $s0; ?>" data-track="<?php echo $s0; ?>"><p><?php echo $sectionName; ?></p></li>
					<?php } ?>
				 <?php } ?>

				</ul>

			</div>
		<!-- navigation -->
		
	</div>

	<?php 
		for ($i=0; $i<count($arrSection); $i++) { 
			$s0 = $arrSection[$i]; 
			if ($s0 != 100  && $arrSectionParent[$s0] == 0){
			  include 'pages/pg' . $s0 . '.php'; 
			}
		} 
    ?>

	<?php if(!isSectionOff(100)) { ?> 
		<?php include 'pages/pg100.php'; ?>
	<?php } ?>

	<!-- footer -->
		<div id="footer">
			<div class="footer-holder">
				<div class="rep">
				<?php if($isRealPurl): ?>
				<div class="rep-name">Your Season Ticket Rep is <?php echo $rep; ?></div>
				<span class="whitedot">&middot;</span>
				<div class="rep-email"><a href="mailto:<?php echo $repEmail; ?>""><?php echo $repEmail; ?></a></div>
				<span class="whitedot">&middot;</span>
				<div class="rep-phone"><?php echo $repPhone; ?></div>
				<?php endif; ?>
				</div>
				<div class="mlb">Major League Baseball trademarks and copyrights are used with permission of MLB Advanced Media, L.P. All rights reserved <a href="share.php?type=Terms" target="_blank">MLB.com Terms of Use</a> | <a href="share.php?type=Privacy" target="_blank">MLB.com Privacy Policy</a></div>
				<a href="share.php?type=DotCom" target="_blank" class="dot-com"><img src="images/dotcom.png"></a>
			</div>
		</div>
	<!-- footer -->

</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://player.vimeo.com/api/player.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/okvideo.min.js"></script>
<script type="text/javascript" src="js/scrollreveal.js"></script>
<script type="text/javascript" src="js/jquery.queryloader2.min.js"></script>
<script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
<!--[if gte IE 9]>
<script type="text/javascript" src="js/hamburgers.js"></script>
<![endif]-->
<script type="text/javascript" src="shadowbox/shadowbox.js"></script>
<script type="text/javascript">Shadowbox.init({language: 'en',players:  ['img', 'html', 'iframe', 'qt', 'wmp', 'swf', 'flv']});</script>
<script type="text/javascript" src="js/athletics.js"></script>

</body>
</html>   