<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG2-->
<div id="pg2" class="pg" data-section="pg2">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Diamond Level</p></div>
	</div>

	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Located directly behind home plate</li>
				<li>Complimentary Wi-Fi access</li>
				<li>Includes private parking in our Diamond Level lot</li>
				<li>All you can eat food from exclusive Diamond Level menu, with in-seat service</li>
				<li>Two complimentary alcoholic beverages for adults 21 and over</li>
				<li>Access your seats by walking down the visiting team's tunnel</li>
				<li>Exclusive field visits, player meet and greets, and autograph opportunities</li>
				<li>Available in Full and Half season plans</li>
				<li>Postseason rights</li>
				<li>Discounted cost: $7,995 per half season seat ($195 per ticket)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=DiamondLevel" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG2-->