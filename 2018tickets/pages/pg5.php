<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG5-->
<div id="pg5" class="pg" data-section="pg5">
      
	<div class="title-holder">
		<div class="pgTitle"><p>Weekend Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Lower Box or Field Infield seats for 44 weekend and holiday games</li>
				<li>Featured games include: <span>Opening Night, Giants (1), Yankees (3), Red Sox (3), Cubs (3) and Fireworks (6)<span></li>
				<li>Same seat for every game, including the Postseason</li>
				<li>Most flexible ticket exchange program - exchange tickets online</li>
				<li>Half Price Preferred Parking option</li>
				<li>Highest level of Postseason Rights, including the lowest per ticket price</li>
				<li>Discounted cost: $1,408 per seat (just $32 per ticket)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=WeekendPlan" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG5-->