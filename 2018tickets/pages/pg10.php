<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG10-->
<div id="pg10" class="pg" data-section="pg10">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Mini Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Accommodate 6 or 12 guests in a full size private box</li>
				<li>Includes a set food package (sliders, hot dogs, farmers' market salad, bottomless popcorn, water, and the first round of beer)</li>
				<li>Available for most games</li>
				<li>Only $660-$810 per 6 guests - food included!</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=MiniSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG10-->