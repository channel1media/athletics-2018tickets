<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG18-->
<div id="pg18" class="pg" data-section="pg18">
       
	<div class="title-holder">
		<div class="pgTitle"><p>President's Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>30 tickets in an Infield Suite above the first base bag</li>
				<li>Extensive high end food package (herb marinated grilled chicken breast kebabs, caprese sandwich, steakhouse wedge salad, Saag's sausage sampler, sliders, assorted desserts, beer, wine, soda, water and plenty more).</li>
				<li>30 Programs</li>
				<li>30 Gate Giveaway Items (valid on gate giveaway days only)</li>
				<li>10 VIP parking passes</li>
				<li>Total Cost: $5,500-$12,000</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=PresidentsSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG18-->