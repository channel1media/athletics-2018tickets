<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG4-->
<div id="pg4" class="pg" data-section="pg4">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Full Season Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>MVP Infield seats for the season - best seating in the Coliseum</li>
				<li>Same seat for every game, including the Postseason</li>
				<li>Most flexible ticket exchange program - exchange tickets online</li>
				<li>Half Price VIP Parking option</li>
				<li>Highest level of Postseason Rights, including the lowest per ticket price</li>
				<li>Discounted cost: $3,608 per seat (just $44 per ticket)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=FullSeasonPlan" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG4-->