<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG21-->
<div id="pg21" class="pg" data-section="pg21">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Westside Patio</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">
			
			<ul>
				<li>Accommodates 70-200 guests</li>
				<li>Includes a pregame BBQ Meal</li>
				<li>Pricing and minimums vary with gamedate and seating level selection</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=WestsidePatio" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG21-->