<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG14-->
<div id="pg14" class="pg" data-section="pg14">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Club Infield Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="top-text">Club Infield Suites are the most exclusive suites in the Coliseum, and are available for 20 or 14 guests.</div>
			<br />
			<div class="list-text">20-Person Club Infield Suite</div>
			<br />
			<ul>
				<li>Best location in the Coliseum</li>
				<li>4 VIP parking passes</li>
				<li>Total Cost: $1,800-$3,000</li>
			</ul>
			<br />
			<div class="list-text">14-Person Club Infield Suite</div>
			<br />
			<ul>
				<li>Best location in the Coliseum</li>
				<li>3 VIP parking passes</li>
				<li>Total Cost: $1,610-$2,380</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=ClubInfieldSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG14-->