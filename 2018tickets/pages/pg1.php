<!--PG1-->
<div id="pg1" class="pg" data-section="pg1">
    
    <div class="players-holder">
        <div id="players1" class="players"></div>
    </div>

    <div class="pgCont">

        <div class="main-holder">
        
            <div class="welcomeBox">
                <div class="welcome-text">Welcome</div>
                <div class="welcome-name"><?php echo $firstname; ?> <?php echo $lastname; ?></div>
            </div>

            <div class="homeBox">
                
                <div class="rep"><p>Your Season Ticket Rep Is <span><?php echo $rep; ?></span></p></div>
                
                <div class="EnP-holder">
                     <div class="rep-email"><img src="images/email.png" class="email-icon"><a href="mailto:<?php echo $repEmail; ?>""><?php echo $repEmail; ?></a></div>
                    <div class="rep-phone"><img src="images/phone.png" class="phone-icon"><?php echo $repPhone; ?></div>
                </div>
                   
            </div>

            <div class="purchase">To make a purchase, please contact your Athletics Representative.</div>

        </div>
        <div class="clearfix"></div>

    </div> 
    
</div>
<!--PG1-->