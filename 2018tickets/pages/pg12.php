<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG12-->
<div id="pg12" class="pg" data-section="pg12">
       
	<div class="title-holder">
		<div class="pgTitle"><p>6-9 Game Suite Mini Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Pick ANY games - including games against the Giants, Yankees, and Red Sox</li>
				<li>3 VIP parking passes for each game</li>
				<li>Playoff priority</li>
				<li>Discounted cost: only $1,260 per suite (reg. 1,800 -$2,250 for big games)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=GameSuiteMiniPlanSixToNine" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG12-->