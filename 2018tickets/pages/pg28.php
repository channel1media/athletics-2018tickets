<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG28-->
<div id="pg28" class="pg" data-section="pg28">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Group Outings</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="list-text">Book your A's Group Outing in 3 easy steps:</div>

			<ol>
				<li>Estimate the size of your group.</li>
				<li>Reserve your seats with a 10% deposit.</li>
				<li>Pay off your reservation three weeks before your game</li>
			</ol>

			<div class="bottom-text">Group Leaders qualify for a great rewards program, including an <span>Exclusive Group Leader Gift</span>.</div>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=GroupOutings" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG28-->