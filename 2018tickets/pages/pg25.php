<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG25-->
<div id="pg25" class="pg" data-section="pg25">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Jumbo Value</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="top-text"><span>Hungry for more?</span><br /><br />Add $5, $10, or $20 in jumbo value onto the barcode of your group ticket for use at various concession stands for food, beverages, and alcohol. You can also use jumbo value at most merchandise stands.</div>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=JumboValue" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG25-->