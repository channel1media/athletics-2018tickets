<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG22-->
<div id="pg22" class="pg" data-section="pg22">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Eastside Club</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">
			
			<ul>
				<li>Accommodates 100-1000 guests</li>
				<li>Includes pregame meal, served in a private area of our spacious Eastside Club</li>
				<li>Pricing and minimums vary with gamedate and seating level selection</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=EastsideClub" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG22-->