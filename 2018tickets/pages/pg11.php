<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG11-->
<div id="pg11" class="pg" data-section="pg11">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Left Field Loft</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Accommodates 50 or 100 guests</li>
				<li>Includes a set food package (sliders, hot dogs, farmers' market salad, peanuts, bottomless popcorn, assorted cookies, soda, and water)</li>
				<li>10 VIP Parking passes per 50 guests</li>
				<li>Total Cost: $2,500-$4,000 per 50 guests - food included!</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=LeftFieldLoft" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG11-->