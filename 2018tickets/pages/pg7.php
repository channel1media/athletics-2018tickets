<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG7-->
<div id="pg7" class="pg" data-section="pg7">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Pick 'Em Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Lower Box or Field Infield seats for ANY 24 games of your choice (no restrictions)</li>
				<li>Flexible ticket exchange program - exchange tickets online</li>
				<li>Half Price Parking option</li>
				<li>Postseason Rights</li>
				<li>Discounted cost: $768-$999 per seat</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=PickEmPlan" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG7-->