<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG13-->
<div id="pg13" class="pg" data-section="pg13">
       
	<div class="title-holder">
		<div class="pgTitle"><p>10-19 Game Suite Mini Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Pick ANY games - including games against the Giants, Yankees, and Red Sox</li>
				<li>3 VIP parking passes for each game</li>
				<li>Playoff priority</li>
				<li>Discounted cost: only $990 per suite (reg. 1,800 -$2,250 for big games)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=GameSuiteMiniPlanTenToNineteen" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG13-->