<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG9-->
<div id="pg9" class="pg" data-section="pg9">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Group Jersey Days</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="top-text">Purchase a group of 25+ in the Field Level on select games and everyone in your group will receive a 1989 Dennis Eckersley Replica Jersey. <span>That's only $30 for a Field Level game ticket AND a jersey</span>.</div>
			<br />
			<div class="list-text">Available Dates:</div>
			<br />
			<ul>
				<li>Thursday, May 19 vs. Yankees</li>
				<li>Saturday, June 18 vs. LA Angels</li>
				<li>Flexible ticket exchange program - exchange tickets online</li>
				<li>Sunday, July 24 vs. Rays</li>
				<li>Friday, August 12 vs. Mariners</li>
				<li>Friday, September 23 vs. Rangers (Fireworks Game!)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=GroupJerseyDays" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG9-->