<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG100-->
<div id="pg100" class="pg" data-section="pg100">
       
	<div class="title-holder">
		<div class="pgTitle"><p><?php echo ucwords(rawurldecode($arrSectionName[100])); ?></p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="summary-container">

				<?php if ($summaryPhoto=="") { ?>
				<!-- <div class="summary-image"><img src="upload/custom-default.png"></div> -->
				<?php } else { ?>
				<div class="summary-image"><img src="upload/<?php echo rawurldecode($summaryPhoto); ?>"></div>
				<?php } ?>	
			
				<div class="summary-content">
								
					<div class="summary-text">
					<?php echo rawurldecode($summaryCopy); ?>
					</div>

					<?php if($summaryFile != "") { ?>
						<br /><br />
						<div class="summary-download"><a href="upload/<?php echo rawurldecode($summaryFile); ?>" download>Download File</a></div>
					<?php } else {} ?>

				</div>

			</div>
			
		</div>

	</div>

</div>
<!--PG100-->