<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG16-->
<div id="pg16" class="pg" data-section="pg16">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Eastside Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="top-text">Eastside Suites are the most modern and spacious suites in the Coliseum, and are available for 24 or 18 guests.</div>
			<br />
			<div class="list-text">24-Person Eastside Suite</div>
			<br />
			<ul>
				<li>Most modern and spacious suites in the Coliseum</li>
				<li>Features 5 TVs and a private restroom</li>
				<li>4 VIP parking passes</li>
				<li>Total Cost: $1,920-$3,000</li>
			</ul>
			<br />
			<div class="list-text">18-Person Eastside Suite</div>
			<br />
			<ul>
				<li>Most modern and spacious suites in the Coliseum</li>
				<li>Features 5 TVs and a private restroom</li>
				<li>3 VIP parking passes</li>
				<li>Total Cost: $1,440-$2,250</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=EastsideSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG16-->