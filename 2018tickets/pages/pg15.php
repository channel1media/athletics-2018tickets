<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG15-->
<div id="pg15" class="pg" data-section="pg15">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Westside Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<div class="top-text">Westside Suites are available for 18 or 12 guests.</div>
			<br />
			<div class="list-text">18-Person Westside Suite</div>
			<br />
			<ul>
				<li>Features 2 TVs</li>
				<li>3 VIP parking passes</li>
				<li>Total Cost: $1,440-$2,250</li>
			</ul>
			<br />
			<div class="list-text">12-Person Westside Suite</div>
			<br />
			<ul>
				<li>Features 2 TVs</li>
				<li>3 VIP parking passes</li>
				<li>Total Cost: $960-$1,500</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=WestsideSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG15-->