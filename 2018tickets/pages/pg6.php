<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG6-->
<div id="pg6" class="pg" data-section="pg6">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Weeknight Plan</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Lower Box or Field Infield seats for 26 weeknight games</li>
				<li>Featured games include: <span>Giants (1), Yankees (1), and Angels (3)</span></li>
				<li>Same seat for every game</li>
				<li>Flexible ticket exchange program - exchange tickets online</li>
				<li>Half Price Parking option</li>
				<li>Postseason Rights</li>
				<li>Discounted cost: $858 per seat (just $33 per ticket)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=WeeknightPlan" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG6-->