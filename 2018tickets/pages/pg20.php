<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG20-->
<div id="pg20" class="pg" data-section="pg20">
       
	<div class="title-holder">
		<div class="pgTitle"><p>BBQ Terrace</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">
			
			<div class="top-text">Enjoy the Weber Sauces &amp; Seasonings BBQ Terrace.</div>
			<br />
			<ul>
				<li>Accommodates 50-70 guests</li>
				<li>Private seating area</li>
				<li>Includes a pregame BBQ Meal</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=BBQTerrace" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG20-->