<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG23-->
<div id="pg23" class="pg" data-section="pg23">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Westside Bay</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">
			
			<ul>
				<li>Accommodates 25-40 guests</li>
				<li>Includes choice of pregame meal, served in a private area overlooking the baseball diamond.</li>
				<li>Pricing and minimums vary with gamedate and seating level selection</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=WestsideBay" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG23-->