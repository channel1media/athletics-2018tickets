<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG3-->
<div id="pg3" class="pg" data-section="pg3">
       
	
	<div class="title-holder">
		<div class="pgTitle"><p>Field Box</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Located next to each dugout, on the field of play</li>
				<li>Includes parking in our exclusive Field Box lot</li>
				<li>In-seat beverage service (includes two complimentary beverage tickets)</li>
				<li>Exclusive field visits, player meet and greets, and autograph opportunities</li>
				<li>Available in Full and Half season plans</li>
				<li>Postseason rights</li>
				<li>Discounted cost: $3,690 per half season seat ($90 per ticket)</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=FieldBox" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG3-->