<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG17-->
<div id="pg17" class="pg" data-section="pg17">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Owner's Suites</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>36 tickets in an Infield Suite above the first base bag</li>
				<li>Extensive high end food package (prime rib, grilled chicken chipotle wraps, turkey Cobb salad, assorted desserts, beer, wine, soda, water and plenty more).</li>
				<li>36 hats and 36 programs</li>
				<li>36 Gate Giveaway Items (valid on gate giveaway days only)</li>
				<li>12 VIP parking passes</li>
				<li>Total Cost: $7,000-$13,500</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=OwnersSuites" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG17-->