<!-- Divider -->
	<div class="divider"></div>
<!-- Divider -->

<!--PG8-->
<div id="pg8" class="pg" data-section="pg8">
       
	<div class="title-holder">
		<div class="pgTitle"><p>Big Game Pack</p></div>
	</div>
	
	<div class="pgCont">

		<div class="content-box">

			<ul>
				<li>Lower Box or Field Reserved seats for the biggest games of the year!</li>
				<li>Featured games: <span>Opening Night</span>, and ALL games vs. the <span>Giants, Yankees, Red Sox, Cubs, and Fireworks games</span></li>
				<li>Flexible ticket exchange program - exchange tickets online</li>
				<li>Half Price Parking option</li>
				<li>Postseason Rights</li>
				<li>Discounted cost: $1,173-$1,233 per seat</li>
			</ul>
			
			<div class="moreInfo-holder">
				<a href="share.php?type=BigGamePack" target="_blank"><div class="moreInfo"><p>More Info</p></div></a>
				<div class="clearfix"></div>
			</div>
		</div>

	</div>

</div>
<!--PG8-->