<script type="text/javascript">
var presentationId = 1;
var sessionDbId = 0;
<?php if (isset($presentationId)) { ?>
	presentationId = <?php echo $presentationId; ?>;
	repId = <?php echo $repId; ?>;
<?php } if (isset($sessionDbId)) { ?>
	sessionDbId = <?php echo $sessionDbId; ?>;
<?php } ?>
var isTesting = false;
<?php if ($isTesting) { ?>
	isTesting = true;
<?php } ?>
var arrSection = new Array();
var arrSectionParent = new Array();
var arrSectionName = new Array();
var arrSectionHasChild = new Array();
var s0;

<?php for ($i=0; $i<count($arrSection); $i++) { ?>
	s0 = <?php echo $arrSection[$i]; ?>;
	arrSection.push(s0);
	arrSectionParent[s0] = <?php echo $arrSectionParent[$arrSection[$i]]; ?>;
	<?php if ($arrSectionName[$arrSection[$i]] == "Summary") { $arrSectionName[$arrSection[$i]] = $summaryTitle; } ?>
	arrSectionName[s0] = '<?php echo $arrSectionName[$arrSection[$i]]; ?>';
<?php } ?>

var summaryOn = true;
<?php if (!isSectionOff(100) || $summaryTitle =="") { ?>
	summaryOn = false;
<?php } ?>

var isRealPurl = false;
<?php if($isRealPurl) { ?>
isRealPurl = true;
<?php } ?>
</script>