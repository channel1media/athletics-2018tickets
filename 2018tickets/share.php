<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once "report/includes/classes/Database.php";
include_once "report/includes/classes/Browser.php";
include_once "report/includes/models/Tracking.php";
include_once "report/includes/models/Accounts.php";

if(isset($_GET['type'])) {
    $type = isset($_GET['type']) ? $_GET['type'] : "";
    $account_id = isset($_GET['account_id']) ? $_GET['account_id'] : "";
    $urls = array(

       "Invoice" => "#",
       "Renew" => "https://mytickets.tickets.com/buy/MyTicketsServlet?orgid=36207&agency=ATHM_MYTIXX&partnerId=2316K18U1OB-Z423",

       "Terms" => "http://mlb.mlb.com/oak/help/oak_help_about_terms.jsp?partnerId=11B1LWW6181-H415",
       "Privacy" => "http://mlb.mlb.com/oak/help/oak_help_about_privacy.jsp?partnerId=31W16181DPE-Y417",
       "DotCom" => "https://www.mlb.com/athletics/?&partnerId=3A11618GJ1-41C3Q",

       "SeasonTickets" => "http://m.mlb.com/athletics/tickets/season/ticket-services?partnerId=31HPT6181-421BL",

       "RickyHenderson" => "https://www.mlb.com/athletics/video/hendersons-record-setting-sb/c-11879049",
       "OaklandSweep" => "https://www.mlb.com/athletics/video/weiss-relives-1989-world-series/c-37198649",
       "DallasBraden" => "https://www.mlb.com/athletics/video/c-7936873?q=dallas+braden+perfect",
       "WorldSeries1972" => "https://www.mlb.com/athletics/video/as-win-1972-world-series/c-1815005583?tid=67793664",
       "ScottHatteberg" => "https://www.mlb.com/athletics/video/as-win-20-straight/c-1826362183?tid=67794254"

    );

    $pdo = Database::getInstance()->getConnection();
    $tracking = new Tracking($pdo);
    $account = new Accounts($pdo);

    $tracking->updateTrackingParams($type);

    if($type == "Renew" && $account_id != "")
        $account->saveRenewDate($account_id);

    header("Location: ".$urls[$type]);
    exit;
}

?>